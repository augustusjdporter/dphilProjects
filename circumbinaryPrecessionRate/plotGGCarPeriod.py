import numpy as np
import matplotlib
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
from precessionPeriodFunctions import circumbinaryOrbitPrecessionPeriodDB11, circumBinaryOrbitPrecessionD15

massPrimary = 26.
massSecondary = 12.
separationBinary = 0.64
eccentricityBinary = 0.28
eccentricityCircumbinary = 0.3
binaryPeriod = 31.
inclination = 0.
ascendingNode = 0.

x = np.linspace(1.0, 10., num=100)

yDB11 = circumbinaryOrbitPrecessionPeriodDB11(x, inclination, ascendingNode, eccentricityCircumbinary, 1., massPrimary/(massPrimary + massSecondary), eccentricityBinary, binaryPeriod)
yD15 = circumBinaryOrbitPrecessionD15(massPrimary, massSecondary, 1., x, eccentricityBinary)
yD15 = yD15 * binaryPeriod
fig, ax1 = plt.subplots()

minLocationOfCO = 2.9
maxLocationOfCO = 5.5

ax1.plot(x, yDB11, label="DB11")
ax1.plot(x, yD15, label="D15")
ax1.axvline(x=1, linestyle='--', color='red', alpha=0.5, label="binary separation")
ax1.set_yscale('log')
ax2 = ax1.twinx()

ax2.set_yscale('log')


ax1.set_xlabel("a (a$_{bin}$)")
ax1.set_ylabel("P (days)")
ax2.set_ylabel("P (P$_{bin}$)")


ax1.axvline(x=minLocationOfCO, linestyle='--', color='gray', alpha=0.5, label="min/max location of CO")
ax1.axvline(x=maxLocationOfCO, linestyle='--', color='gray', alpha=0.5)

ax1.axvline(x=1, linestyle='--', color='red', alpha=0.5, label="binary separation")


ax1.axhline(y=470, linestyle='--', color='blue', alpha=0.5, label="470 day GJW period")
ax1.legend()
ax3 = ax1.twiny().twinx()
ax3.set_xlabel("a (AU)")
plt.show()