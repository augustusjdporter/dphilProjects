#include "functions.h"
#include <iostream>
#include <fstream>

using namespace std;

int main()
{
	ofstream file;
	file.open("GGCarPeriod.txt");
	const double rMin = 1.;
	const double rMax = 10.;
	for (int i =0; i < 100; i++)
	{
		double r = rMin + (rMax - rMin) * double(i)/100;
		double GGCarPeriod = circumbinaryOrbitPrecessionPeriod(r, -0., -0.0, 0.3, 1., 26./38., 0.28, 31.);
		//cout << r << "\t" << GGCarPeriod << endl;
		file << r << "\t" << GGCarPeriod << endl;
	};
	file.close();
	return 0;
};