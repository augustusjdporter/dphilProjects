import numpy as np
import cmath

# returns period in whatever units the binaryPeriod parameter is
def circumbinaryOrbitPrecessionPeriodDB11(semiMajorAxisOrbit, inclination, longAscendingNode, eccentricityCircumbinary, semiMajorAxisBinary, massFraction, eccentricityBinary, binaryPeriod):
    # Work out the precessional period of the circumbinary orbits
    # https://en.wikipedia.org/wiki/Elliptic_integral#Incomplete_elliptic_integral_of_the_first_kind
    h = np.cos(inclination)**2. - eccentricityBinary**2.*np.sin(inclination)**2.*(5.*np.sin(longAscendingNode)**2. -1.) # should be constant of motion

    k_squared = (5.*eccentricityBinary**2/(1 - eccentricityBinary**2))*((1- h)/(h + 4*eccentricityBinary**2))

    F_k_squared = 0
    if k_squared < 1:
        # second term may need to be sqrt(1 - pow(k_squared, 2))
        dummy = complex(1., np.sqrt(1. - k_squared**2.))
        ellipticIntegral = (np.pi / 2.) / cmath.phase(dummy)
        F_k_squared = ellipticIntegral
    else:
        # second term may need to be sqrt(1 - pow(k_minus2, 2))
        k_minus2 = 1. / k_squared
        dummy = complex(1., np.sqrt(1. - k_minus2**2.))
        ellipticIntegral = (np.pi/2.)/cmath.phase(dummy)
        F_k_squared = 1./np.sqrt(k_squared) * ellipticIntegral

    # periodInBinaryPeriodUnits = 8./(3.*np.pi)*(1./(massFraction*(1. - massFraction)))*((semiMajorAxisOrbit / semiMajorAxisBinary)**7./2.)*((F_k_squared*(1.-eccentricityCircumbinary**2.)**2.)/np.sqrt((1. - (eccentricityBinary**2.))*(h + 4.*(eccentricityBinary**2))))
    periodInBinaryPeriodUnits = 8./(3.*np.pi)*1./(massFraction*(1. - massFraction))*pow(semiMajorAxisOrbit/ semiMajorAxisBinary, 7./2.)*F_k_squared*pow(1-pow(eccentricityCircumbinary, 2), 2)/np.sqrt((1 - pow(eccentricityBinary, 2))*(h + 4*pow(eccentricityBinary, 2)))
    # print(periodInBinaryPeriodUnits)
    return periodInBinaryPeriodUnits*binaryPeriod


# returns the circumbinary precession period in binary period units
def circumBinaryOrbitPrecessionD15(massPrimary, massSecondary, semiMajorAxisBinary, semiMajorAxisCircumbinary, eccentricityBinary):
    rateOfPrecession =  (3./4.) * (massPrimary*massSecondary)/((massPrimary + massSecondary)**2) * (semiMajorAxisBinary/semiMajorAxisCircumbinary)**2 * (1+(eccentricityBinary**2)/2)**2
    time = 2*np.pi / rateOfPrecession
    return time