import matplotlib.pyplot as plt
import numpy as np


k2 = 1.
def precession_tidal(bin_mass, bin_sep, r_star):
    return 4.7 * 10 **(-6) * (k2 / 0.13) * (bin_mass / 0.89)**0.5 * (bin_sep / 0.22) ** (-6.5) * (r_star / 2.03) ** 5

def precession_rotational(bin_mass, bin_sep, r_star):
    return 1.4 * 10 **(-6) * (k2 / 0.13) * (bin_mass / 0.89)**0.5 * (bin_sep / 0.22) ** (-6.5) * (r_star / 2.03) ** 5

def precession_disk_grav(bin_mass, bin_sep, p, surface_density_disk, phi):
    return 2.6 * 10**(-3) * (0.89 / bin_mass) ** 0.5 * (bin_sep / 0.22) **(0.5 - p) * (surface_density_disk / 3000.) * (phi / 0.46)

def precession_gen_rel(bin_mass, bin_sep):
    return 6.9 * 10 ** (-6) * (bin_mass / 0.89)**1.5 * (0.22 / bin_sep) **2.5


stellar_radii = [31., 2.]
bin_mass = 37.5
bin_sep = 0.65

precession_sum = 0.
for stellar_radius in stellar_radii:
    print("tidal", precession_tidal(bin_mass, bin_sep, stellar_radius))
    precession_sum += precession_tidal(bin_mass, bin_sep, stellar_radius)
    print("rotational", precession_rotational(bin_mass, bin_sep, stellar_radius))
    precession_sum += precession_rotational(bin_mass, bin_sep, stellar_radius)

print("precession tidal", precession_sum, "rad.yr-1")

p = 1.5
surface_density_disk = 0.235
phi = 0.5

Precession_disk_grav = precession_disk_grav(bin_mass, bin_sep, p, surface_density_disk, phi)
print("Precession_disk_grav", Precession_disk_grav, "rad.yr-1")

Precession_rel = precession_gen_rel(bin_mass, bin_sep)
print("Precession_rel", Precession_rel, "rad.yr-1")

disk_densities = np.arange(surface_density_disk, 3000, surface_density_disk)

fig, (ax1, ax2, ax3) = plt.subplots(1, 3)

ax1.plot(disk_densities, precession_disk_grav(bin_mass, bin_sep, p, disk_densities, phi), label="Disk Gravity")
ax1.plot(disk_densities, np.ones_like(disk_densities) * precession_sum, label="Tidal and Rotational")
ax1.plot(disk_densities, np.ones_like(disk_densities) * Precession_rel, label="General Relativity")
ax1.set_yscale('log')
ax1.set_xscale('log')
ax1.legend()
ax1.set_xlabel("Disk surface density (g.cm-2)")
ax1.set_ylabel("Precession frequency")


surface_density_disk = 10

p_s = np.linspace(0., 3, 100)

ax2.plot(p_s, precession_disk_grav(bin_mass, bin_sep, p_s, surface_density_disk, phi), label="Disk Gravity")
ax2.plot(p_s, np.ones_like(p_s) * precession_sum, label="Tidal and Rotational")
ax2.plot(p_s, np.ones_like(p_s) * Precession_rel, label="General Relativity")
ax2.set_yscale('log')
ax2.set_xscale('log')
ax2.legend()
ax2.set_xlabel("Disk density profile")
ax2.set_ylabel("Precession frequency")

phis = np.linspace(0.3, 0.8, 100)

ax3.plot(phis, precession_disk_grav(bin_mass, bin_sep, p, surface_density_disk, phis), label="Disk Gravity")
ax3.plot(phis, np.ones_like(phis) * precession_sum, label="Tidal and Rotational")
ax3.plot(phis, np.ones_like(phis) * Precession_rel, label="General Relativity")
ax3.set_yscale('log')
ax3.set_xscale('log')
ax3.set_xlabel("'Phi'")
ax3.set_ylabel("Precession frequency")
ax3.legend()

plt.show()





