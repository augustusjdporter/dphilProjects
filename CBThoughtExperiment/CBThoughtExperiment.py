import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy import constants
G = 6.674 * 10 ** (-11)
c = 299792458.
M_sun = 1.989 * 10 ** 30
AU = 149597871000.
L_sun = 3.828 * 10 ** 26
sigma = 5.67 * 10 ** (-8)
contours = [0.3, 0.5, 1., 2., 4.3, 5, 10]

k_boltzmann = 8.6173303 * 10**(-5) # eV per K
wiens_displacement_constant = 2.8977729 * 10**(-3)

def temperature_from_energy(energy):
    return energy / constants.Boltzmann


def redshift_by_mass_and_separation(observing_wavelength, mass_binary, separation_binary, inclination):
    print(np.sin(inclination))
    return observing_wavelength / c * (G * mass_binary / (2. * separation_binary))**0.5 * np.sin(inclination)


def luminosity_from_mass(mass):
    luminosity = np.zeros_like(mass)
    luminosity[np.where(mass <= 0.43*M_sun)] = 0.23 * L_sun * (mass[np.where(mass <= 0.43*M_sun)] / M_sun) ** 2.3

    luminosity[np.logical_and(0.43*M_sun < mass, mass <= 2.*M_sun)] = L_sun * (mass[np.logical_and(0.43*M_sun < mass, mass <= 2.*M_sun)] / M_sun) ** 4

    luminosity[np.logical_and(2.*M_sun < mass, mass <= 20. * M_sun)] = 1.5 * L_sun * (mass[np.logical_and(2.*M_sun < mass, mass <= 20. * M_sun)] / M_sun) ** 3.5

    luminosity[20. * M_sun < mass] = 3200. * L_sun * (mass[20. * M_sun < mass] / M_sun)

    return luminosity


def temperature_of_disk(binary_mass, distance):
    luminosity = luminosity_from_mass(binary_mass)
    return (luminosity / (4. * np.pi * distance ** 2 * sigma)) ** 0.25


class emission_line:
    def __init__(self, element, central_wavelength, excitation_energy, max_temperature, min_temperature = None):
        self.element = element
        self.central_wavelength = central_wavelength
        self.excitation_energy = excitation_energy
        self.max_temperature = max_temperature
        if min_temperature is not None:
            self.min_temperature = min_temperature
        else:
            self.min_temperature = self.calculate_min_temperature()
            print(element, central_wavelength, self.min_temperature)

    def calculate_min_temperature(self):
        # energy = constants.h * constants.speed_of_light / (central_wavelength * 10 ** (-10))
        # return temperature_from_energy(energy)
        return wiens_displacement_constant / (self.central_wavelength * 10 ** (-10))

        # E = hc / lambda
        # lambda = b / T
        # min_temperature = wiens_displacement_constant * self.excitation_energy * constants.electron_volt / (constants.h * constants.speed_of_light)
        # return min_temperature



species = []
species.append(emission_line("H Alpha", 6563, 12.04, 10000.))
species.append(emission_line("He", 5875.6, 22.97, 7000.))
species.append(emission_line("He", 6678.1, 22.97, 7000.))
species.append(emission_line("He", 7065.2, 22.62, 7000.))
species.append(emission_line("Si II", 6347.1, 10.03, 7000.))
species.append(emission_line("Fe II", 6369.5, 4.82, 7000.))
# energy in eV


# log scale
mass = np.logspace(0., 2, 100) * M_sun
sep = np.logspace(-1, 2, 100) * AU

inclinations = np.linspace(0., np.pi, 180)
inclinations = [np.pi / 2]
# linear scale
# mass = np.linspace(1., 100., 400) * M_sun
# sep = np.linspace(0.1, 100., 400) * AU


masses, separation = np.meshgrid(mass, sep)

for specie in species:
    central_wavelength = specie.central_wavelength
    min_temperature = specie.min_temperature
    print(specie.element, central_wavelength)
    print("min_temp", min_temperature)
    plt.clf()
    # fig, (ax1, ax2) = plt.subplots(1, 2)
    fig = plt.figure()
    ax1 = fig.add_subplot(1, 2, 1)
    ax1.set_xscale("log")
    ax1.set_yscale("log")


    redshifts = redshift_by_mass_and_separation(central_wavelength, masses, separation, np.pi / 2.)

    plt.pcolormesh(masses / M_sun, separation / AU, redshifts, norm=matplotlib.colors.LogNorm())
    try:
        cbar = plt.colorbar()
    except:
        continue
    cbar.ax.set_ylabel('Shift (Angstroms)', rotation=270)
    ax1.set_xlabel("Mass of binary (Solar Masses)")
    ax1.set_ylabel("Separation of binary (Astronomical Units)")

    CS = ax1.contour(masses / M_sun, separation / AU, redshifts, contours, colors='k')

    ax1.clabel(CS, fontsize=9, inline=1)

    ax1.scatter([37.5], [0.65])
    temperatures = temperature_of_disk(masses, 2 * separation)
    ax2 = fig.add_subplot(1, 2, 2)
    ax2.set_xscale("log")
    ax2.set_yscale("log")

    plt.pcolormesh(masses / M_sun, separation / AU, temperatures, norm=matplotlib.colors.LogNorm())
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('Temperature (Kelvin)', rotation=270)

    # min_temp_helium = temperature_from_energy(species[specie])
    print(min_temperature)
    max_temp = specie.max_temperature
    print([min_temperature, max_temp])
    CS = ax1.contour(masses / M_sun, separation / AU, temperatures, [min_temperature, max_temp], colors='k')
    ax1.clabel(CS, fontsize=9, inline=1)

    CS = ax2.contour(masses / M_sun, separation / AU, temperatures, [min_temperature, max_temp], colors='k')
    ax2.clabel(CS, fontsize=9, inline=1)

    ax2.set_xlabel("Mass of binary (Solar Masses)")
    ax2.set_ylabel("Separation of binary (Astronomical Units)")
    fig.suptitle("{} {}".format(specie.element, specie.central_wavelength))
    ax2.scatter([37.5], [0.65])
    ax1.set_title("Redshift")
    ax2.set_title("Temperature of inner stable orbit (2 binary separations)")
    plt.gcf().set_size_inches(20., 7.)
    plt.show()
    # fig.savefig("images/{}.png".format("{}{}.pdf".format(specie.element, specie.central_wavelength)))




