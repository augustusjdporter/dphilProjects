#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <complex>
#include <zconf.h>


#ifdef __linux__ 
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#endif

#include "functions.h"
#include "dataPoint.h"

using namespace std;
const double G(6.67408*pow(10, -11));
const double AU(1.4960*pow(10, 11));
const double solarMass(1.989*pow(10, 30));

const string fileLocation("/home/portera/privateWork/circumbinaryPrecessionPovray/");


int main()
{
    std::cout << "Program to print coordinates of heavenly bodies to file and call Povray to render them. " << std::endl;

    //First read in the libration data from files
    vector<dataPoint> innerLibrationData;
    vector<dataPoint> upperLibrationData;
    vector<dataPoint> lowerLibrationData;
    vector<dataPoint> outerLibrationData;

    if(!(readLibrationData(innerLibrationData, fileLocation + "InnerLibration.txt") && readLibrationData(upperLibrationData, fileLocation + "UpperLibration.txt") && readLibrationData(lowerLibrationData, fileLocation + "LowerLibration.txt") && readLibrationData(outerLibrationData, fileLocation + "OuterLibration.txt")))
    {
        cout << "failure in reading a data file. Make sure all files exist." << endl;
        return 0;
    };

    const double eccentricityBinary(0.62);
    const double semiMajorAxisBinary(1*AU);
    const double semiMajorAxisOrbit(2*AU);
    const double massAtFocus(1*solarMass);
    const double massRatio(2.2);

    //M = m1 + m2
    //q = m1/m2
    //=m1/M-m1
    //qM - qm1 = m1
    //qM = (1+q)m1
    //m1 = qM/(1+q)
    const double massPrimary(massRatio*massAtFocus/(1+massRatio));
    const double massSecondary(massAtFocus - massPrimary);

    double a1(massSecondary*semiMajorAxisBinary/(massPrimary + massSecondary));
    double a2(-massPrimary*semiMajorAxisBinary/(massPrimary + massSecondary));//minus, so star starts in minus x axis, and velocity takes into account

    double a1oAU(a1/AU);
    double a2oAU(a2/AU);

    double trueAnomaly(0);
    double inclinationBinary(0);
    double longAscendingNodeBinary(0);
    double argPeriapsisBinary(0);
    double argPeriapsisCircumbinary(0);
    double eccentricityCircumbinary(0);


    double binaryPeriod(sqrt(4*pow(M_PI, 2)*pow(semiMajorAxisBinary, 3)/(G*massAtFocus)));
    double circumbinaryPeriod(sqrt(4*pow(M_PI, 2)*pow(semiMajorAxisOrbit, 3)/(G*massAtFocus)));



    //get period of each libration
    double innerLibrationPeriod = circumbinaryOrbitPrecessionPeriod(semiMajorAxisOrbit, innerLibrationData.at(0).inclination(), innerLibrationData.at(0).longAscendingNode(), eccentricityCircumbinary, semiMajorAxisBinary, massPrimary/massAtFocus, eccentricityBinary, binaryPeriod);
    double upperLibrationPeriod = circumbinaryOrbitPrecessionPeriod(semiMajorAxisOrbit, upperLibrationData.at(0).inclination(), upperLibrationData.at(0).longAscendingNode(), eccentricityCircumbinary, semiMajorAxisBinary, massPrimary/massAtFocus, eccentricityBinary, binaryPeriod);
    double lowerLibrationPeriod = circumbinaryOrbitPrecessionPeriod(semiMajorAxisOrbit, lowerLibrationData.at(0).inclination(), lowerLibrationData.at(0).longAscendingNode(), eccentricityCircumbinary, semiMajorAxisBinary, massPrimary/massAtFocus, eccentricityBinary, binaryPeriod);
    double outerLibrationPeriod = circumbinaryOrbitPrecessionPeriod(semiMajorAxisOrbit, outerLibrationData.at(0).inclination(), outerLibrationData.at(0).longAscendingNode(), eccentricityCircumbinary, semiMajorAxisBinary, massPrimary/massAtFocus, eccentricityBinary, binaryPeriod);


    //iterate enough times so that the slowest precession makes a full cycle
    //iterate in terms of the binary period. Maybe 10 times per binary cycle?

    double timestep(binaryPeriod/16);
    double time(0);
    //get longest libration period
    double longestLibrationPeriod(0);

    longestLibrationPeriod = innerLibrationPeriod;

    if (longestLibrationPeriod < upperLibrationPeriod)
        longestLibrationPeriod = upperLibrationPeriod;

    if (longestLibrationPeriod < lowerLibrationPeriod)
        longestLibrationPeriod = lowerLibrationPeriod;

    if (longestLibrationPeriod < outerLibrationPeriod)
        longestLibrationPeriod = outerLibrationPeriod;

    cout << longestLibrationPeriod/binaryPeriod << endl;
    const int numberOfSteps(longestLibrationPeriod/timestep);
    cout << numberOfSteps << endl;
    for (int i = 0; i < numberOfSteps; i++)
    {
        bool renderUpperAlone(true);
        bool renderLowerAlone(true);
        bool renderInnerAlone(true);
        bool renderOuterAlone(true);

        //Need to work out which point of the libration period we are on
        double innerLibrationPosition(time/innerLibrationPeriod);
        while (innerLibrationPosition > 1.0)
        {
            innerLibrationPosition = innerLibrationPosition - 1.0;
            renderInnerAlone = false;
        }

        double upperLibrationPosition(time/upperLibrationPeriod);
        while (upperLibrationPosition > 1.0)
        {
            upperLibrationPosition = upperLibrationPosition - 1.0;
            renderUpperAlone = false;
        }

        double lowerLibrationPosition(time/lowerLibrationPeriod);
        while (lowerLibrationPosition > 1.0)
        {
            lowerLibrationPosition = lowerLibrationPosition - 1.0;
            renderLowerAlone = false;
        }

        double outerLibrationPosition(time/outerLibrationPeriod);
        while (outerLibrationPosition > 1.0)
        {
            outerLibrationPosition = outerLibrationPosition - 1.0;
            renderOuterAlone = false;
        }

        int innerLibrationVectorIndex(innerLibrationPosition*innerLibrationData.size());
        int upperLibrationVectorIndex(upperLibrationPosition*upperLibrationData.size());
        int lowerLibrationVectorIndex(lowerLibrationPosition*lowerLibrationData.size());
        int outerLibrationVectorIndex(outerLibrationPosition*outerLibrationData.size());

        cout << innerLibrationVectorIndex << "\t" << upperLibrationVectorIndex << "\t" << lowerLibrationVectorIndex << "\t" << outerLibrationVectorIndex << "\t" << endl;
        vector<double> primaryStarInitialPosition(cartesianPosition3D(a1, eccentricityBinary, 0/*trueAnomaly*/, inclinationBinary, argPeriapsisBinary, longAscendingNodeBinary));
        vector<double> secondaryStarInitialPosition(cartesianPosition3D(a2, eccentricityBinary, 0/*trueAnomaly*/, inclinationBinary, argPeriapsisBinary, longAscendingNodeBinary));

        vector<double> innerLibrationOrbitInitialPosition(cartesianPosition3D(semiMajorAxisOrbit, eccentricityCircumbinary, 0/*trueAnomaly*/, innerLibrationData.at(innerLibrationVectorIndex).inclination(), argPeriapsisCircumbinary, innerLibrationData.at(innerLibrationVectorIndex).longAscendingNode()));
        vector<double> upperLibrationOrbitInitialPosition(cartesianPosition3D(semiMajorAxisOrbit, eccentricityCircumbinary, 0/*trueAnomaly*/, upperLibrationData.at(upperLibrationVectorIndex).inclination(), argPeriapsisCircumbinary, upperLibrationData.at(upperLibrationVectorIndex).longAscendingNode()));
        vector<double> lowerLibrationOrbitInitialPosition(cartesianPosition3D(semiMajorAxisOrbit, eccentricityCircumbinary, 0/*trueAnomaly*/, lowerLibrationData.at(lowerLibrationVectorIndex).inclination(), argPeriapsisCircumbinary, lowerLibrationData.at(lowerLibrationVectorIndex).longAscendingNode()));
        vector<double> outerLibrationOrbitInitialPosition(cartesianPosition3D(semiMajorAxisOrbit, eccentricityCircumbinary, 0/*trueAnomaly*/, outerLibrationData.at(outerLibrationVectorIndex).inclination(), argPeriapsisCircumbinary, outerLibrationData.at(outerLibrationVectorIndex).longAscendingNode()));

        ofstream orbitFile;
        orbitFile.open(fileLocation + "orbits.txt");
        orbitFile << "\"primary\", " << primaryStarInitialPosition.at(0)/AU << ", " << primaryStarInitialPosition.at(1)/AU << ", " << primaryStarInitialPosition.at(2)/AU << ", " << eccentricityBinary << ", " << inclinationBinary*180/M_PI << ", " << argPeriapsisBinary*180/M_PI << ", " << longAscendingNodeBinary*180/M_PI << ", " << endl;
        orbitFile << "\"secondary\", " << secondaryStarInitialPosition.at(0)/AU << ", " << secondaryStarInitialPosition.at(1)/AU << ", " << secondaryStarInitialPosition.at(2)/AU << ", " << eccentricityBinary << ", " << inclinationBinary*180/M_PI << ", " << argPeriapsisBinary*180/M_PI << ", " << longAscendingNodeBinary*180/M_PI << ", " << endl;
        orbitFile << "\"innerLibration\", " << innerLibrationOrbitInitialPosition.at(0)/AU << ", " << innerLibrationOrbitInitialPosition.at(1)/AU << ", " << innerLibrationOrbitInitialPosition.at(2)/AU << ", " << eccentricityCircumbinary << ", " << innerLibrationData.at(innerLibrationVectorIndex).inclination()*180/M_PI << ", " << argPeriapsisCircumbinary*180/M_PI << ", " << innerLibrationData.at(innerLibrationVectorIndex).longAscendingNode()*180/M_PI << ", " << endl;
        orbitFile << "\"upperLibration\", " << upperLibrationOrbitInitialPosition.at(0)/AU << ", " << upperLibrationOrbitInitialPosition.at(1)/AU << ", " << upperLibrationOrbitInitialPosition.at(2)/AU << ", " << eccentricityCircumbinary << ", " << upperLibrationData.at(upperLibrationVectorIndex).inclination()*180/M_PI << ", " << argPeriapsisCircumbinary*180/M_PI << ", " << upperLibrationData.at(upperLibrationVectorIndex).longAscendingNode()*180/M_PI << ", " << endl;
        orbitFile << "\"lowerLibration\", " << lowerLibrationOrbitInitialPosition.at(0)/AU << ", " << lowerLibrationOrbitInitialPosition.at(1)/AU << ", " << lowerLibrationOrbitInitialPosition.at(2)/AU << ", " << eccentricityCircumbinary << ", " << lowerLibrationData.at(lowerLibrationVectorIndex).inclination()*180/M_PI << ", " << argPeriapsisCircumbinary*180/M_PI << ", " << lowerLibrationData.at(lowerLibrationVectorIndex).longAscendingNode()*180/M_PI << ", " << endl;
        orbitFile << "\"outerLibration\", " << outerLibrationOrbitInitialPosition.at(0)/AU << ", " << outerLibrationOrbitInitialPosition.at(1)/AU << ", " << outerLibrationOrbitInitialPosition.at(2)/AU << ", " << eccentricityCircumbinary << ", " << outerLibrationData.at(outerLibrationVectorIndex).inclination()*180/M_PI << ", " << argPeriapsisCircumbinary*180/M_PI << ", " << outerLibrationData.at(outerLibrationVectorIndex).longAscendingNode()*180/M_PI << ", " << endl;
        orbitFile.close();


        double binaryTrueAnomaly = findTrueAnomaly(binaryPeriod, eccentricityBinary, time);
        double circumbinaryTrueAnomaly = findTrueAnomaly(circumbinaryPeriod, eccentricityCircumbinary, time);

        ofstream file;

        file.open(fileLocation + "binaryPosition.txt");

        vector<double> primaryStarPosition(cartesianPosition3D(a1, eccentricityBinary, binaryTrueAnomaly, inclinationBinary, argPeriapsisBinary, longAscendingNodeBinary));
        vector<double> secondaryStarPosition(cartesianPosition3D(a2, eccentricityBinary, binaryTrueAnomaly, inclinationBinary, argPeriapsisBinary, longAscendingNodeBinary));

        vector<double> innerLibrationCartPosition(cartesianPosition3D(semiMajorAxisOrbit, eccentricityCircumbinary, circumbinaryTrueAnomaly, innerLibrationData.at(innerLibrationVectorIndex).inclination(), argPeriapsisCircumbinary, innerLibrationData.at(innerLibrationVectorIndex).longAscendingNode()));
        vector<double> upperLibrationCartPosition(cartesianPosition3D(semiMajorAxisOrbit, eccentricityCircumbinary, circumbinaryTrueAnomaly, upperLibrationData.at(upperLibrationVectorIndex).inclination(), argPeriapsisCircumbinary, upperLibrationData.at(upperLibrationVectorIndex).longAscendingNode()));
        vector<double> lowerLibrationCartPosition(cartesianPosition3D(semiMajorAxisOrbit, eccentricityCircumbinary, circumbinaryTrueAnomaly, lowerLibrationData.at(lowerLibrationVectorIndex).inclination(), argPeriapsisCircumbinary, lowerLibrationData.at(lowerLibrationVectorIndex).longAscendingNode()));
        vector<double> outerLibrationCartPosition(cartesianPosition3D(semiMajorAxisOrbit, eccentricityCircumbinary, circumbinaryTrueAnomaly, outerLibrationData.at(outerLibrationVectorIndex).inclination(), argPeriapsisCircumbinary, outerLibrationData.at(outerLibrationVectorIndex).longAscendingNode()));

        file << "\"primary\", " << primaryStarPosition.at(0)/AU << ", " << primaryStarPosition.at(1)/AU << ", " << primaryStarPosition.at(2)/AU << "," << endl;
        file << "\"secondary\", " << secondaryStarPosition.at(0)/AU << ", " << secondaryStarPosition.at(1)/AU << ", " << secondaryStarPosition.at(2)/AU << "," << endl;
        file << "\"inner\", " << innerLibrationCartPosition.at(0)/AU << ", " << innerLibrationCartPosition.at(1)/AU << ", " << innerLibrationCartPosition.at(2)/AU << "," << endl;
        file << "\"upper\", " << upperLibrationCartPosition.at(0)/AU << ", " << upperLibrationCartPosition.at(1)/AU << ", " << upperLibrationCartPosition.at(2)/AU << "," << endl;
        file << "\"lower\", " << lowerLibrationCartPosition.at(0)/AU << ", " << lowerLibrationCartPosition.at(1)/AU << ", " << lowerLibrationCartPosition.at(2)/AU << "," << endl;
        file << "\"outer\", " << outerLibrationCartPosition.at(0)/AU << ", " << outerLibrationCartPosition.at(1)/AU << ", " << outerLibrationCartPosition.at(2)/AU << "," << endl;
        file.close();

        stringstream plotNumber;
        plotNumber << i;

        //Many automated processing programs need pictures to be in alphabetical order, this ensures they are with up to 100,000 files
        string numberOfZerosToPrepend("");
        if (i >= 0 && i < 10)
        {
            numberOfZerosToPrepend = "0000";
        }
        else if (i >= 10 && i < 100)
        {
            numberOfZerosToPrepend = "000";
        }
        else if (i >= 100 && i < 1000)
        {
            numberOfZerosToPrepend = "00";
        }
        else if (i >= 1000 && i < 10000)
        {
            numberOfZerosToPrepend = "0";
        }
        else if (i >= 10000 && i < 100000)
        {
            numberOfZerosToPrepend = "";
        }

        #ifdef __APPLE__
        //string command("/home/portera/projects/PovrayCommandLineMacV2/Povray37UnofficialMacCmd -V +I/home/portera/privateWork/orbitVisualisationWithPovray/renderBinary.pov +L/home/portera/projects/PovrayCommandLineMacV2/include +J +Q11 +W1280 +H960 Output_File_Type=N +A +AM2 +R3 +THFS +o/Data/binary/binary" + plotNumber.str() + ".png");
        string command("/home/portera/projects/PovrayCommandLineMacV2/Povray37UnofficialMacCmd  " + fileLocation + "renderBinary.ini +I" + fileLocation + "renderPrecessionsAll.pov +o/Data/circumbinaryPrecessionAll/binary" + numberOfZerosToPrepend + plotNumber.str() + ".png -GD -GR -GS -GW -GA");
        cout << command << endl;
        system(command.c_str());

        command = string("/home/portera/projects/PovrayCommandLineMacV2/Povray37UnofficialMacCmd  " + fileLocation + "renderBinary.ini +I" + fileLocation + "renderPrecessionsAllSide.pov +o/Data/circumbinaryPrecessionRotateAll/binary" + numberOfZerosToPrepend + plotNumber.str() + ".png -GD -GR -GS -GW -GA");
        cout << command << endl;
        system(command.c_str());

        if(renderInnerAlone == true)
        {
            command = string("/home/portera/projects/PovrayCommandLineMacV2/Povray37UnofficialMacCmd  " + fileLocation + "renderBinary.ini +I" + fileLocation + "renderInnerPrecession.pov +o/Data/circumbinaryInnerPrecession/binary" + numberOfZerosToPrepend + plotNumber.str() + ".png -GD -GR -GS -GW -GA");
            cout << command << endl;
            system(command.c_str());

            command = string("/home/portera/projects/PovrayCommandLineMacV2/Povray37UnofficialMacCmd  " + fileLocation + "renderBinary.ini +I" + fileLocation + "renderInnerPrecessionSide.pov +o/Data/circumbinaryInnerPrecessionRotate/binary" + numberOfZerosToPrepend + plotNumber.str() + ".png -GD -GR -GS -GW -GA");
            cout << command << endl;
            system(command.c_str());
        }

        if (renderOuterAlone == true)
        {
            command = string("/home/portera/projects/PovrayCommandLineMacV2/Povray37UnofficialMacCmd  " + fileLocation + "renderBinary.ini +I" + fileLocation + "renderOuterPrecession.pov +o/Data/circumbinaryOuterPrecession/binary" + numberOfZerosToPrepend + plotNumber.str() + ".png -GD -GR -GS -GW -GA");
            cout << command << endl;
            system(command.c_str());

            command = string("/home/portera/projects/PovrayCommandLineMacV2/Povray37UnofficialMacCmd  " + fileLocation + "renderBinary.ini +I" + fileLocation + "renderOuterPrecessionSide.pov +o/Data/circumbinaryOuterPrecessionRotate/binary" + numberOfZerosToPrepend + plotNumber.str() + ".png -GD -GR -GS -GW -GA");
            cout << command << endl;
            system(command.c_str());
        }

        if (renderLowerAlone == true || renderUpperAlone == true)
        {
            command = string("/home/portera/projects/PovrayCommandLineMacV2/Povray37UnofficialMacCmd  " + fileLocation + "renderBinary.ini +I" + fileLocation + "renderUpperLowerPrecessions.pov +o/Data/circumbinaryUpperLowerPrecession/binary" + numberOfZerosToPrepend + plotNumber.str() + ".png -GD -GR -GS -GW -GA");
            cout << command << endl;
            system(command.c_str());

            command = string("/home/portera/projects/PovrayCommandLineMacV2/Povray37UnofficialMacCmd  " + fileLocation + "renderBinary.ini +I" + fileLocation + "renderUpperLowerPrecessionSide.pov +o/Data/circumbinaryUpperLowerPrecessionRotate/binary" + numberOfZerosToPrepend + plotNumber.str() + ".png -GD -GR -GS -GW -GA");
            cout << command << endl;
            system(command.c_str());
        }

        #elif __linux__
        string command("povray  renderBinaryUbuntu.ini +o/home/augustus/Data/binary/binary" + numberOfZerosToPrepend + plotNumber.str() + ".png");
        system(command.c_str());
        #endif


        time = time + timestep;
    };



    return 0;
}