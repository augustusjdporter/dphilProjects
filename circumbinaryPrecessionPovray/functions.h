//
// Created by Augustus Porter on 07/11/2016.
//
#ifndef ORBITVISUALISATIONWITHPOVRAY_FUNCTIONS_H
#define ORBITVISUALISATIONWITHPOVRAY_FUNCTIONS_H

#include <cmath>
#include <vector>
#include <iostream>
#include <fstream>
#include <complex>
#include "dataPoint.h"

using namespace std;

const vector <double> cartesianPosition3D(const double semiMajorAxis, const double ellipticity, const double trueAnomaly, const double inclination, const double argPeriapsis, const double longAscendingNode)
{
    double r = semiMajorAxis*(1 - pow(ellipticity, 2)) / (1 + ellipticity*cos(trueAnomaly));

    double xPos = r*(cos(longAscendingNode)*cos(trueAnomaly + argPeriapsis) - sin(longAscendingNode)*sin(trueAnomaly + argPeriapsis)*cos(inclination));

    double yPos = r*(sin(longAscendingNode)*cos(trueAnomaly + argPeriapsis) + cos(longAscendingNode)*sin(trueAnomaly + argPeriapsis)*cos(inclination));

    double zPos = r*(sin(trueAnomaly + argPeriapsis)*sin(inclination));

    vector <double> position;

    position.push_back(xPos);
    position.push_back(yPos);
    position.push_back(zPos);

    return position;
};

const double eccentricAnomalyNplus1(const double eccentricAnomalyN, const double eccentricity, const double meanAnomaly)
{
    return eccentricAnomalyN - (eccentricAnomalyN - eccentricity*sin(eccentricAnomalyN) - meanAnomaly)/(1 - eccentricity*cos(eccentricAnomalyN));
};

const double findTrueAnomaly(const double period, const double eccentricity, const double time)
{
    //https://en.wikipedia.org/wiki/Kepler%27s_laws_of_planetary_motion#Position_as_a_function_of_time
    double meanMotion(2*M_PI/period);
    double meanAnomaly(meanMotion*time);

    //need to iteratively solve Keplers equation M = E - e*sinE, where E is eccentric anomaly, and M is mean anomaly
    double eccentricAnomaly = eccentricAnomalyNplus1(meanAnomaly, eccentricity, meanAnomaly);
    while(true)
    {
        double tempEccentricAnomaly = eccentricAnomaly;
        eccentricAnomaly = eccentricAnomalyNplus1(eccentricAnomaly, eccentricity, meanAnomaly);

        if (abs(eccentricAnomaly - tempEccentricAnomaly) != 0 && tempEccentricAnomaly == 0)
            tempEccentricAnomaly = 0.0000000001;
        double percentageChange(abs(eccentricAnomaly - tempEccentricAnomaly)*100/tempEccentricAnomaly);

        //When further iterations dont cause much change, carry on
        if(abs(eccentricAnomaly - tempEccentricAnomaly) == 0 || percentageChange < 0.1)
        {
            break;
        }
    };

    //return the true anomaly
    return 2*atan(sqrt((1 + eccentricity)/(1 - eccentricity))*tan(eccentricAnomaly/2));
};

const double circumbinaryOrbitPrecessionPeriod(const double semiMajorAxisOrbit, const double inclination, const double longAscendingNode, const double eccentricityCircumbinary, const double semiMajorAxisBinary, const double massFraction, const double eccentricityBinary, const double binaryPeriod)
{
    //Work out the precessional period of the circumbinary orbits
    //https://en.wikipedia.org/wiki/Elliptic_integral#Incomplete_elliptic_integral_of_the_first_kind
    double h = pow(cos(inclination), 2) - pow(eccentricityBinary, 2)*pow(sin(inclination),2)*(5*pow(sin(longAscendingNode), 2) -1);//should be constant of motion
    double k_squared = 5*pow(eccentricityBinary, 2)/(1 - pow(eccentricityBinary, 2))*(1- h)/(h + 4*pow(eccentricityBinary, 2));
    double k_minus2 = 1/k_squared;
    double F_k_squared(0);
    if(k_squared < 1)
    {
        //second term may need to be sqrt(1 - pow(k_squared, 2))
        complex<double> dummy(1, sqrt(1 - pow(k_squared, 2)));
        double ellipticIntegral = (M_PI / 2) / arg(dummy);
        F_k_squared = ellipticIntegral;
    }
    else
    {
        //second term may need to be sqrt(1 - pow(k_minus2, 2))
        complex<double> dummy(1, sqrt(1 - pow(k_minus2, 2)));
        double ellipticIntegral = (M_PI/2)/arg(dummy);
        F_k_squared = pow(k_squared, -1 / 2) * ellipticIntegral;
    };

    double periodInBinaryPeriodUnits = 8./(3.*M_PI)*1./(massFraction*(1. - massFraction))*pow(semiMajorAxisOrbit/ semiMajorAxisBinary, 7./2.)*F_k_squared*pow(1.-pow(eccentricityCircumbinary, 2.), 2.)/sqrt((1. - pow(eccentricityBinary, 2.))*(h + 4.*pow(eccentricityBinary, 2.)));
    cout << F_k_squared << endl;
    return periodInBinaryPeriodUnits*binaryPeriod;
};

const bool readLibrationData(vector<dataPoint>& librationData, const string fileLocation)
{
    ifstream dataFile;
    dataFile.open(fileLocation);
    if (!dataFile)
    {
        cout << "Failed to open data file: " << fileLocation << endl;
        return false;
    }

    //finding only increments up to the next value every 7 or so frames.
    //Therefore extrapolate between x and y values for 7 more values

    double xLast(0);
    double yLast(0);
    while (!dataFile.eof())
    {
        double xDummy;
        double yDummy;

        dataFile >> xDummy >> yDummy;

        //interpolate between data points so that it looks smooth
        if (xLast == 0 && yLast == 0)
        {
            librationData.push_back(dataPoint(xDummy, yDummy));
        }
        else
        {
            double xDiff(xDummy - xLast);
            double yDiff(yDummy - yLast);
            for (int i = 1; i <= 14; i++)
            {
                librationData.push_back(dataPoint(xLast + xDiff / 14 * i, yLast + yDiff / 14 * i));
            }
        };

        xLast = xDummy;
        yLast = yDummy;
    };

    dataFile.close();

    return true;
};
#endif //ORBITVISUALISATIONWITHPOVRAY_FUNCTIONS_H
