#include "/home/portera/projects/PovrayCommandLineMacV2/include/colors.inc"
#include "/home/portera/projects/PovrayCommandLineMacV2/include/stones.inc"
#include "stars.inc"
#include "rad_def.inc"

camera
{
	//location <2, -3.5, 0> //looks at plane of binary if inclination = 30 degs
	location <0, 0, -4.7>
	look_at <0, 0, 0>
}
 light_source {<0, 0, -4.7>, White }
// 
//  light_source {<1, 0, -4.7>, White }
//global_settings {
//   radiosity {
//      Rad_Settings(Radiosity_OutdoorHQ,on,on)
//   }
//}
#default {finish{ambient 0}}
 
#declare star = union{sphere // transparent sphere containing media
 { 0,1 pigment { rgbt 1 } hollow
   interior
   { media
     { emission 60 
       density
       { spherical density_map
         { [0 rgb 0]
           [0.03 rgb <1,0,0>]
           [0.2 rgb <1,1,0>]
           [1 rgb 1]
         }
       }
     }
  }
 }
 //light_source {<0.1,0.1,0.1>, Grey }
}

#declare circumbinary = sphere
{
	<0,0,0>, 0.01
	texture
	{
		pigment
		{
			color Blue
		}
	}
}

#macro drawOrbit (periastronDistance, Eccentricity, focusX, focusY, focusZ, inclination, argPeriapsis, longAscendingNode, colorChoice, scaleChoice)

	object
	{
	torus
	{
		1, scaleChoice
		rotate -90*x
		texture
		{
			pigment
			{
				color colorChoice
			}
		}
	}
	
	//Scale ellipse so that is an ellipse, not a circle. x->ax, y->by
	scale x* periastronDistance/(1-Eccentricity) // a = periast/ (1 - e)
	scale y* periastronDistance/(1-Eccentricity)*sqrt(1 - pow(Eccentricity, 2)) //b = a*(1 - e^2)
	
	//Using the periastron distance (position when argPeriapsis==0) and eccentricity, need to place the ellipse so that its focus is at the centre of mass
	//a - focal = periastronDistance
	//f = a*e
	//a = peri/(1-e)
	//f = e*peri/(1-e)
	
	//Translate ellipse so (0,0) is at CoM focus, not center of ellipse
	translate -x*Eccentricity* periastronDistance/(1-Eccentricity)
	
	//Rotations are always around (0,0), so need to do them while CoM focus is at (0,0)
	rotate z*argPeriapsis
	rotate x*inclination
	rotate z*longAscendingNode
	
	translate <focusX, focusY, focusZ>
	}
#end
 

#fopen myOrbitFile "/home/portera/privateWork/circumbinaryPrecessionPovray/orbits.txt" read
#while (defined(myOrbitFile))
  	#read (myOrbitFile, name, xVar, yVar, zVar, ecc, inc, argPeri, longNode)
  	#switch (0)		
  		//secondary orbit needs to be defined from the "negative" side of the axis.
  		#case(strcmp(name, "secondary"))
  			drawOrbit(-sqrt(pow(xVar, 2) + pow(yVar, 2) + pow(zVar, 2)), ecc, 0, 0, 0, inc, argPeri, longNode, Gray, 0.008)
  			#break
  			
  		#case(strcmp(name, "primary"))
  			drawOrbit(sqrt(pow(xVar, 2) + pow(yVar, 2) + pow(zVar, 2)), ecc, 0, 0, 0, inc, argPeri, longNode, Gray, 0.015)
  			#break
  			
  		#case(strcmp(name, "outerLibration"))
  			drawOrbit(sqrt(pow(xVar, 2) + pow(yVar, 2) + pow(zVar, 2)), ecc, 0, 0, 0, inc, argPeri, longNode, Cyan, 0.004)
  			#break
	#end
#end

#fopen myFile "/home/portera/privateWork/circumbinaryPrecessionPovray/binaryPosition.txt" read
#while (defined(myFile))
  	#read (myFile, name, xVar, yVar, zVar)
  	#switch(0)
  		#case(strcmp(name, "secondary"))
  		#case(strcmp(name, "primary"))
  		#case(strcmp(name, "outer"))
  			object {star scale 0.02 translate x*xVar translate y*yVar translate z*zVar }
  			#break
  	#end
#end
  
//  text {
//    ttf "timrom.ttf" "Retrograde Orbits" 0.01, 0
//    pigment { Blue }
//    
//    scale x*0.2
//    scale y*0.2
//    scale z*0.2
//    
//    translate x*1.5
//    translate y*-1.7
//  }

