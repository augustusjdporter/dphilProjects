//
// Created by Augustus Porter on 07/11/2016.
//

#ifndef ORBITVISUALISATIONWITHPOVRAY_DATAPOINT_H
#define ORBITVISUALISATIONWITHPOVRAY_DATAPOINT_H
#include <iostream>

using namespace std;

class dataPoint
{
public:
    dataPoint(const double newX, const double newY)
    {
        x = newX;
        y = newY;
    };

    ~dataPoint(){};

    const double inclination()
    {
        return M_PI*sqrt(x*x + y*y);
    };

    const double longAscendingNode()
    {
        double longAscendingNode(atan(y/x));
        if (x >= 0 && y >= 0)
        {
            longAscendingNode = longAscendingNode;
        }
        else if (x < 0 && y > 0)
        {
            longAscendingNode = longAscendingNode + M_PI;
        }
        else if (x < 0 && y <= 0)
        {
            longAscendingNode = longAscendingNode + M_PI;
        }
        else if (x > 0 && y < 0)
        {
            longAscendingNode = longAscendingNode + 2*M_PI;
        }
        return longAscendingNode;
    };

private:
    dataPoint()
    {
        cout << "default constructor should be unused" << endl;
    };

    double x;
    double y;
};
#endif //ORBITVISUALISATIONWITHPOVRAY_DATAPOINT_H
