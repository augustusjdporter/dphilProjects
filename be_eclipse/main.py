import numpy as np
import matplotlib
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from astropy import constants


def does_it_eclipse(separation, inclination, r2):
    return separation * np.cos(inclination) < 1+r2


def p_transit(r2, a):
    return 1 - 2*np.arccos((1+r2)/a)/np.pi


if __name__ == "__main__":
    # x = np.linspace(0,1, 1000)
    # plt.plot(x, np.arccos(x))
    # plt.show()

    dist = np.tan(np.radians(0.64*10**(-3) / 3600)) * 650 * constants.si.iau2015.pc
    dist = dist / constants.si.iau2015.R_sun
    print(dist)
    frac_binary = 6/33
    print(frac_binary)
    r2 = np.linspace(0, 5, 3000)
    a = np.linspace(2, 50, 6000)

    a, r2 = np.meshgrid(a, r2)

    p_s = p_transit(r2, a)
    # , extent=[10,5000,0,1]
    plt.imshow(p_s, aspect="auto", extent=[np.min(a),np.max(a),np.max(r2),np.min(r2)])
    plt.contour(a, r2, p_s, levels=[frac_binary])

    gg_sep = 0.65*constants.si.iau2015.au / (32*constants.si.iau2015.R_sun)
    plt.axvline(x=gg_sep.value, color="red", linestyle="--")
    # plt.contour(a, r2, p_s, 6,
    #             colors='k',  # negative contours will be dashed by default
    #             )
    # plt.contourf(r2, a, p_s)
    plt.show()

