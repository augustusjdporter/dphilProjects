#include <iostream>
#include "body.h"
#include "system.h"
#include "Universe.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <utility>
#include <string.h>
#include <thread>

#include <sys/types.h>
#include <sys/stat.h>

using namespace std;


const vector <double> cartesianPosition3D(const double semiMajorAxis, const double ellipticity, const double trueAnomaly, const double inclination, const double argPeriapsis, const double longAscendingNode)
{
    double r = semiMajorAxis*(1 - pow(ellipticity, 2)) / (1 + ellipticity*cos(trueAnomaly));

    double xPos = r*(cos(longAscendingNode)*cos(trueAnomaly + argPeriapsis) - sin(longAscendingNode)*sin(trueAnomaly + argPeriapsis)*cos(inclination));

    double yPos = r*(sin(longAscendingNode)*cos(trueAnomaly + argPeriapsis) + cos(longAscendingNode)*sin(trueAnomaly + argPeriapsis)*cos(inclination));

    double zPos = r*(sin(trueAnomaly + argPeriapsis)*sin(inclination));

    vector <double> position;

    position.push_back(xPos);
    position.push_back(yPos);
    position.push_back(zPos);

    return position;
};

const vector <double> cartesianVelocity3D(const double semiMajorAxis, const double ellipticity, const double massAtFocus, const double trueAnomaly, const double inclination, const double argPeriapsis, const double longAscendingNode)
{
    double orbitalPeriod(0);

    orbitalPeriod = sqrt(4 * pow(M_PI, 2)*pow(semiMajorAxis, 3) / (G*massAtFocus));

    double velocityMagnitude(2*M_PI*semiMajorAxis/(orbitalPeriod*sqrt(1-pow(ellipticity, 2))));
    vector<double> velocity;

    double vx = ellipticity*(sin(longAscendingNode)*cos(argPeriapsis)*cos(inclination) + cos(longAscendingNode)*sin(argPeriapsis)) + sin(longAscendingNode)*cos(inclination)*cos(trueAnomaly + argPeriapsis) + cos(longAscendingNode)*sin(trueAnomaly + argPeriapsis);
    velocity.push_back(-velocityMagnitude*vx);

    double vy = ellipticity*(cos(longAscendingNode)*cos(argPeriapsis)*cos(inclination) - sin(longAscendingNode)*sin(argPeriapsis)) + cos(longAscendingNode)*cos(inclination)*cos(trueAnomaly + argPeriapsis) - sin(longAscendingNode)*sin(trueAnomaly + argPeriapsis);
    velocity.push_back(velocityMagnitude*vy);

    double vz = sin(inclination)*(cos(trueAnomaly + argPeriapsis) + ellipticity*cos(argPeriapsis));
    velocity.push_back(velocityMagnitude*vz);
    return velocity;
};

const double eccentricAnomalyNplus1(const double eccentricAnomalyN, const double eccentricity, const double meanAnomaly)
{
    return eccentricAnomalyN - (eccentricAnomalyN - eccentricity*sin(eccentricAnomalyN) - meanAnomaly)/(1 - eccentricity*cos(eccentricAnomalyN));
};

const double findTrueAnomaly(const double period, const double eccentricity, const double time)
{
    //https://en.wikipedia.org/wiki/Kepler%27s_laws_of_planetary_motion#Position_as_a_function_of_time
    double meanMotion(2*M_PI/period);
    double meanAnomaly(meanMotion*time);

    //need to iteratively solve Keplers equation M = E - e*sinE, where E is eccentric anomaly, and M is mean anomaly
    double eccentricAnomaly = eccentricAnomalyNplus1(meanAnomaly, eccentricity, meanAnomaly);
    while(true)
    {
        double tempEccentricAnomaly = eccentricAnomaly;
        eccentricAnomaly = eccentricAnomalyNplus1(eccentricAnomaly, eccentricity, meanAnomaly);

        if (abs(eccentricAnomaly - tempEccentricAnomaly) != 0 && tempEccentricAnomaly == 0)
            tempEccentricAnomaly = 0.0000000001;
        double percentageChange(abs(eccentricAnomaly - tempEccentricAnomaly)*100/tempEccentricAnomaly);

        //When further iterations dont cause much change, carry on
        if(abs(eccentricAnomaly - tempEccentricAnomaly) == 0 || percentageChange < 0.1)
        {
            break;
        }
    };

    //return the true anomaly
    return 2*atan(sqrt((1 + eccentricity)/(1 - eccentricity))*tan(eccentricAnomaly/2));
};

void printCoordinatesToFile(const string simName, const string& path, const string& file_name, const double& normalisation, const vector<Body>& binary, vector<Body>& testParticles)
{
    ofstream coordinate_file;
    coordinate_file.open(path + file_name);

    ofstream orbital_file;
    orbital_file.open(path + file_name + "orbitParams.txt");

    ofstream trajectories;
    trajectories.open("Coords/" +simName + "/trajectories.txt", std::fstream::in | std::fstream::out | std::fstream::app);

    for (int i = 0; i < 2; i++)
        coordinate_file << binary.at(i).name() << "\t" << binary.at(i).xPosition()/normalisation << "\t" << binary.at(i).yPosition()/normalisation << "\t" << binary.at(i).zPosition()/normalisation << "\t" << binary.at(i).xVelocity() << "\t" << binary.at(i).yVelocity() << "\t" << binary.at(i).zVelocity() << endl;//prints shape data with overloaded <<

    for (int i =0; i < testParticles.size(); i++)
    {
        coordinate_file << testParticles.at(i).name() << "\t" << testParticles.at(i).xPosition()/normalisation << "\t" << testParticles.at(i).yPosition()/normalisation << "\t" << testParticles.at(i).zPosition()/normalisation << "\t" << testParticles.at(i).xVelocity() << "\t" << testParticles.at(i).yVelocity() << "\t" << testParticles.at(i).zVelocity() << endl;//prints shape data with overloaded <<
        if(testParticles.at(i).isTrackingTrajectory() == true)
        {
            testParticles.at(i).addToTrajectory(&trajectories);
        }

        if (testParticles.at(i).eccentricity() > 1.0)
        {
            if (testParticles.at(i).isBound())
            {
                //Another particle became unbound
                string filename(path + "unboundParticles.txt");
                fstream unboundFile;
                unboundFile.open(filename.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
                if (!unboundFile)
                {
                    //file does not already exist
                    unboundFile.open(filename,  fstream::in | fstream::out | fstream::trunc);
                    unboundFile << testParticles.at(i).ID() << endl;
                    unboundFile.close();
                }
                else
                {
                    unboundFile << testParticles.at(i).ID() << endl;
                    unboundFile.close();
                }
                //add to is unbound file
                testParticles.at(i).setIsBound(false);
            }
        }
        else
        {
            orbital_file << testParticles.at(i).ID() << "\t" << testParticles.at(i).eccentricity() << "\t" << testParticles.at(i).inclination() <<"\t" << testParticles.at(i).ascendingNode() << endl;
        }
    }

    trajectories.close();
    coordinate_file.close();
    orbital_file.close();
};

int main()
{
    std::cout << "Hello, World!" << std::endl;
    std::cout << "Recreating Doolin and Blundell" << std::endl;
    string simulationName("first");

    for (double eccentricity = 0; eccentricity <= 0.6; eccentricity = eccentricity + 0.1)
    {
        for (double massFraction = 0.5; massFraction >= 0.1; massFraction = massFraction - 0.1)
        {
            //if (eccentricity == 0 && (massFraction == 0.5 || massFraction == 0.4)) continue;
            //Define parameters to be filled by the config reader
            int timestep(0);
            int numberOfSteps(0);
            int samplingRate(0);
            double normalisation(0);
            string runName = simulationName;
            stringstream simNameCombiner;
            simNameCombiner << "e" << eccentricity << "mF" << massFraction;
            runName = runName + simNameCombiner.str();
            Universe simulation_universe(runName);

            string path;
            int stepCount(1);
            int plotNumber(0);
            string simulationType;

            mkdir("/Data/circOrbits/Coords", 0700);
            path = "/Data/circOrbits/Coords/" + runName + "/";

            //Make directory structure

            mkdir(string("/Data/circOrbits/Coords").c_str(), 0700);

            mkdir((path).c_str(), 0700);

            mkdir((path + "Snapshots/").c_str(), 0700);

            mkdir((path + "Plots/").c_str(), 0700);

            mkdir((path + "trajectories/").c_str(), 0700);


            int j(0), k(0), count(0);
            //@@@@@@@@@

            const double totalMass(solar_mass);
            const double semiMajorAxis(AU);

            const double massSecondary = massFraction * totalMass;
            const double massPrimary = totalMass - massSecondary;

            const double primaryInitialXPosition(massSecondary * semiMajorAxis / totalMass);
            //m1r1 = m2r2
            //r1 +r2 = a
            //m1(a - r2) = m2r2
            //m1a = m1r2 + m2r2
            //r2 = m1a/(m1 + m2)
            const double secondaryInitialXPosition(massPrimary * semiMajorAxis / totalMass);

            //v_a = ((GM_s/a)(1-e)/(1+e))^0.5

            const double primaryInitialYVel(pow(G *((massSecondary / massPrimary) * massSecondary * massPrimary / (massPrimary + massSecondary)) /semiMajorAxis * (1 - eccentricity) / (1 + eccentricity), 0.5));
            const double secondaryInitialYVel(pow(G *((massPrimary / massSecondary) * massSecondary * massPrimary / (massPrimary + massSecondary)) /semiMajorAxis * (1 - eccentricity) / (1 + eccentricity), 0.5));

            vector<Body> binary;
            binary.push_back(Body("primary", massPrimary, primaryInitialXPosition, 0, 0, 0, primaryInitialYVel, 0, 0, true));
            binary.push_back(Body("secondary", massSecondary, -secondaryInitialXPosition, 0, 0, 0, -secondaryInitialYVel, 0, 0, true));


            vector<Body> testParticles;
            //loop over a
            double a(1.5);
            while (a <= 10)
            {
                double i(0);
                while (i <= M_PI)
                {
                    double ascendingNode(0);
                    while (ascendingNode < 2 * M_PI)
                    {
                        //double trueAnomaly(0);
                        //while (trueAnomaly < 2 * M_PI)
                            //P^2 = R^3 in solar units

                        double xPos(a * AU * cos(ascendingNode));
                        double yPos(a * AU * sin(ascendingNode));
                        double zPos(0);
                        double xVel(-pow(G * totalMass / (a * semiMajorAxis), 0.5) * cos(i) * sin(ascendingNode));
                        double yVel(pow(G * totalMass / (a * semiMajorAxis), 0.5) * cos(i) * cos(ascendingNode));
                        double zVel(pow(G * totalMass / (a * semiMajorAxis), 0.5) * sin(i));
                        testParticles.push_back(Body("testParticle", 0, xPos, yPos, zPos, xVel, yVel, zVel, 0, true));
                        //	trueAnomaly = trueAnomaly + M_PI / 2;
                        ascendingNode = ascendingNode + M_PI / 2;
                    }
                    i = i + M_PI / 20;
                }
                a = a + 0.5;
            }
                //loop over true anomaly
            //P(/years)^2 = (a/AU)^3
            double binaryPeriod(secondsInYear);


            normalisation = AU;
            timestep = 3600*12;
            samplingRate = 10 * binaryPeriod / timestep;
           // samplingRate = 100;
            numberOfSteps = pow(10, 4) * binaryPeriod / timestep;
            //@@@@@@@

            simulation_universe.makeTrajectoryFiles(path + "trajectories/",
                                                     "isTrackingTrajectories.txt",
                                                     "trajectories.txt");


            int refresh = samplingRate; //when refresh = samplingRate, we take a snapshot

            while (stepCount <= numberOfSteps)
            {
                //set position of binary
                double binaryTrueAnomaly = findTrueAnomaly(binaryPeriod, eccentricity, timestep*stepCount);
                vector<double> primaryStarPosition(cartesianPosition3D(primaryInitialXPosition, eccentricity, binaryTrueAnomaly, 0, 0, 0));
                vector<double> secondaryStarPosition(cartesianPosition3D(secondaryInitialXPosition, eccentricity, binaryTrueAnomaly, 0, 0, 0));
                binary.at(0).set_xPosition(primaryStarPosition.at(0));
                binary.at(0).set_yPosition(primaryStarPosition.at(1));
                binary.at(0).set_zPosition(primaryStarPosition.at(2));

                binary.at(1).set_xPosition(secondaryStarPosition.at(0));
                binary.at(1).set_yPosition(secondaryStarPosition.at(1));
                binary.at(1).set_zPosition(secondaryStarPosition.at(2));


                //update the velocities and accelerations of the particles
                for (int i = 0; i < testParticles.size(); i++)
                {
                    testParticles.at(i).accelerationCalc(binary);
                    testParticles.at(i).update_position_and_velocity(timestep);
                };


                if (refresh == samplingRate)
                {

                    std::string command =
                            "python plot-planetary-simulation.py ";
                    command += runName;
                    command += " ";

                    ostringstream convertIntToString;
                    convertIntToString << plotNumber;

                    command = command + convertIntToString.str() + " ";

                    convertIntToString.str(std::string());

                    convertIntToString << plotNumber;

                    command = command + convertIntToString.str();

                    stringstream combiner;
                    combiner << "Snapshots/It_" << convertIntToString.str() << ".txt";

                    string file_name;
                    combiner >> file_name;

                    printCoordinatesToFile(simulationName, path, file_name, normalisation, binary, testParticles);


                    //Call python plotting on command line. result is currently unused
                    auto run_python_command = [](const string command)
                    {
                        cout << command << endl;
                        //int result = system(command.c_str());
                    };

                    std::thread(run_python_command, command).join();

                    refresh = 0;
                    plotNumber++;

                }

                //enduni = time(0);
                //int stop_s = clock();
                //cout << "Time taken for step " << stepCount << " (seconds): " << enduni - beginninguni << endl;
                //cout << "ms: " << (stop_s - start_s) / double(CLOCKS_PER_SEC) * 1000 << endl;

                refresh++;
                stepCount++;
            }
            cout << "Simulation is finished! Thank you for using Moonglum!!" << endl;
        }
    }
    return 0;
};