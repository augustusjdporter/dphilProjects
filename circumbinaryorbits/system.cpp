#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <random>
#include <fstream>
#include <thread>

#include "system.h"

unsigned concurentThreadsSupported = std::thread::hardware_concurrency();

using namespace std;

System::System()
{
	m_name = "Ghost";
};

System::System(const string& name)
{
	m_name = name;
};

System::~System()
{
	SystemMap::iterator it;
	for (it = m_BoundSystems.begin(); it != m_BoundSystems.end(); ++it)
	{
		it->second = NULL;
	}

	/*for (shared_ptr<Body> bodyPtr : m_Bodies)
	{
		bodyPtr.reset();
	}*/

	m_BoundSystems.clear();
	m_name = "";
};

void System::addBody(shared_ptr<Body> newBody)
{
	m_Bodies.push_back(newBody);
};

/*void System::addBody(const Body newBody)
{
	m_Bodies.push_back(shared_ptr<Body>(new Body(newBody)));
};*/


void System::update_on_cpu(const double& timestep)
{	
	//cout << "calculating acc on cpu" << endl;
	vector<thread> threads;
	auto acceleration_func = [&](int start, int total)
	{
		for (int i = start; i < start + total; ++i)
		{

		};
	};
//cout << concurentThreadsSupported << endl;
//	for (int i = 0; i < concurentThreadsSupported; ++i)
//	{
//		if (i != concurentThreadsSupported-1)
//			threads.push_back(thread(acceleration_func, i * m_Bodies.size()/concurentThreadsSupported, m_Bodies.size()/concurentThreadsSupported));
//		else	//Chuck any of the remainder into the last thread (wont make a difference in performance, will usually be a max of 3 bodies)
//			threads.push_back(thread(acceleration_func, i * m_Bodies.size()/concurentThreadsSupported, m_Bodies.size()/concurentThreadsSupported + m_Bodies.size()%concurentThreadsSupported-1));
//	}
threads.push_back(thread(acceleration_func, 0, 2));
	for (auto& th : threads)
	th.join();

	threads.clear();
	
	auto update_pos_func = [&](int start, int total)
	{
		for (int i = start; i < start + total; ++i)
		{
			m_Bodies.at(i)->update_position_and_velocity(timestep);
		};
	};

/*	for (int i = 0; i < concurentThreadsSupported; ++i)
	{
		if (i != concurentThreadsSupported - 1)
			threads.push_back(thread(update_pos_func, i * m_Bodies.size() / concurentThreadsSupported, m_Bodies.size() / concurentThreadsSupported));
		else	//Chuck any of the remainder into the last thread (wont make a difference in performance, will usually be a max of 3 bodies)
			threads.push_back(thread(update_pos_func, i * m_Bodies.size() / concurentThreadsSupported, m_Bodies.size() / concurentThreadsSupported + m_Bodies.size() % concurentThreadsSupported-1));
	};*/
	threads.push_back(thread(update_pos_func, 0, 2));
	for (auto& th : threads)
		th.join();

	/*for (int i = 0; i < m_Bodies.size(); ++i)
	{
		m_Bodies.at(i)->accelerationCalc(&m_Bodies);
	};

	for (int i = 0; i < m_Bodies.size(); ++i)
	{
		m_Bodies.at(i)->update_position_and_velocity(timestep);;
	};*/
	return;
}

void System::printCoordinates(ofstream* coordinate_file, ofstream* trajectory_file, const double& normalisation)
{

	for (int i=0; i < m_Bodies.size(); i++)
	{
		*coordinate_file << m_Bodies.at(i)->name() << "\t" << m_Bodies.at(i)->xPosition()/normalisation << "\t" << m_Bodies.at(i)->yPosition()/normalisation << "\t" << m_Bodies.at(i)->zPosition()/normalisation << "\t" << m_Bodies.at(i)->xVelocity() << "\t" << m_Bodies.at(i)->yVelocity() << "\t" << m_Bodies.at(i)->zVelocity() << endl;//prints shape data with overloaded <<
		if(m_Bodies.at(i)->isTrackingTrajectory() == true)
		{
			m_Bodies.at(i)->addToTrajectory(trajectory_file);
		}
	}

	return;
};

void System::printOrbitalParams(ofstream* file, const string& path)
{

	for (int i=0; i < m_Bodies.size(); i++)
	{

		*file << m_Bodies.at(i)->ID() << "\t" << m_Bodies.at(i)->eccentricity() << "\t" << m_Bodies.at(i)->inclination() <<"\t" << m_Bodies.at(i)->ascendingNode() << endl;

		if (m_Bodies.at(i)->eccentricity() > 1.0)
		{
			if (m_Bodies.at(i)->isBound())
			{
				//Another particle became unbound
				string filename(path + "unboundParticles.txt");
				fstream unboundFile;
				unboundFile.open(filename.c_str(), std::fstream::in | std::fstream::out | std::fstream::app);
				if (!unboundFile)
				{
					//file does not already exist
					unboundFile.open(filename,  fstream::in | fstream::out | fstream::trunc);
					unboundFile << m_Bodies.at(i)->ID() << endl;
					unboundFile.close();
				}
				else
				{
					unboundFile << m_Bodies.at(i)->ID() << endl;
					unboundFile.close();
				}
				//add to is unbound file
				m_Bodies.at(i)->setIsBound(false);
			}
		}
	}
	return;
}

void System::addBoundSystem(System* newSystem)
{
	m_BoundSystems.insert(make_pair(newSystem->name(), newSystem));
	return;
};

const string& System::name() const 
{
	return m_name;
};

vector<shared_ptr<Body>>* System::Bodies()
{
	return &m_Bodies;
};
