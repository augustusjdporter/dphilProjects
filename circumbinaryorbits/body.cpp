#ifdef _WIN32
#define _USE_MATH_DEFINES
#include <cmath>
#endif

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <random>

#include <sys/types.h>
#include <sys/stat.h>
#ifdef __linux__ 
#include <unistd.h>
#endif

#include "body.h"

using namespace std;

static int bodyCount(0);

Body::Body()
{
	m_ID = bodyCount;
	bodyCount++;
	m_name = "Ghost";
	m_mass = 0;
	m_xPosition = m_yPosition = m_xVelocity = m_yVelocity = 0;

	m_xAcceleration = 0;
	m_yAcceleration = 0;
	m_zAcceleration = 0;
	
	m_radius = 0;
	m_relaxation = 0;
	
	m_trajectory = NULL;

	m_isBound = true;
};

Body::Body(const Body& bodyToCopy)
{
	m_ID = bodyToCopy.ID();

	m_name = bodyToCopy.name();
	m_mass = bodyToCopy.mass();

	m_xPosition = bodyToCopy.xPosition();
	m_yPosition = bodyToCopy.yPosition();
	m_zPosition = bodyToCopy.zPosition();

	m_xVelocity = bodyToCopy.xVelocity();
	m_yVelocity = bodyToCopy.yVelocity();
	m_zVelocity = bodyToCopy.zVelocity();

	m_isValid = bodyToCopy.isValid();

	m_radius = bodyToCopy.radius();
	
	m_relaxation = bodyToCopy.relaxation();

	m_logTrajectory = bodyToCopy.isTrackingTrajectory();

	m_xAcceleration = 0;
	m_yAcceleration = 0;
	m_zAcceleration = 0;
	
	m_trajectory = NULL;

	m_isBound = true;
};

Body::~Body()
{
	/*if (m_name != "Planetesimal")*/
	cout << "Destroying " << m_name << " " << m_ID << endl;
	if (m_trajectory != NULL)
	{
		if(m_trajectory->is_open() == true)
		{
			m_trajectory->close();
			delete m_trajectory;
			m_trajectory = NULL;
		}
	}
};

Body::Body(string tempName, 
	 double tempMass,
	 double tempxPosition, 
	 double tempyPosition, 
	 double tempzPosition, 
	 double tempxVelocity, 
	 double tempyVelocity, 
	 double tempzVelocity,
	 double tempRadius,
	 bool	tempLogTrajectory)
{
	m_ID = bodyCount;
	bodyCount++;
	m_trajectory = NULL;

	m_name = tempName;
	m_mass = tempMass;

	m_xPosition = tempxPosition;
	m_yPosition = tempyPosition;
	m_zPosition = tempzPosition;

	m_xVelocity = tempxVelocity;
	m_yVelocity = tempyVelocity;
	m_zVelocity = tempzVelocity;

	m_isValid = true;

	m_radius = tempRadius;

	m_logTrajectory = tempLogTrajectory;

	m_xAcceleration = 0;
	m_yAcceleration = 0;
	m_zAcceleration = 0;
	
	m_trajectory = NULL;

	//m_relaxation = 0.05*3.0857*pow(10, 12);
	m_relaxation = m_radius;
	if (m_name != "Planetesimal")
	cout << m_name << " created " << m_ID << endl;

	m_isBound = true;
}
Body::Body(int tempID, string tempName, double tempMass, double tempRadius, double tempRelaxation, double tempxPosition, double tempyPosition, double tempzPosition, double tempxVelocity, double tempyVelocity, double tempzVelocity, bool tempLogTrajectory)
{
	m_ID = tempID;
	m_trajectory = NULL;

	m_name = tempName;
	m_mass = tempMass;

	m_xPosition = tempxPosition;
	m_yPosition = tempyPosition;
	m_zPosition = tempzPosition;

	m_xVelocity = tempxVelocity;
	m_yVelocity = tempyVelocity;
	m_zVelocity = tempzVelocity;

	m_isValid = tempLogTrajectory;

	m_radius = tempRadius;

	m_logTrajectory = tempLogTrajectory;

	m_xAcceleration = 0;
	m_yAcceleration = 0;
	m_zAcceleration = 0;
	
	m_isValid = true;
	
	m_trajectory = NULL;

	//m_relaxation = 0.05*3.0857*pow(10, 12);
	m_relaxation = tempRelaxation;
	if (m_name != "Planetesimal")
		cout << m_name << " created " << m_ID << endl;

	m_isBound = true;
};

const int& Body::ID() const
{
	return m_ID;
}

const double Body::twoDimensionalDistanceToOtherBody(const Body& otherBody)
{
	double rx = m_xPosition - otherBody.xPosition();
	double ry = m_yPosition - otherBody.yPosition();

	return pow(pow(rx, 2) + pow(ry, 2), 0.5);
}

const double Body::threeDimensionalDistanceToOtherBody(const Body& otherBody)
{
	double rx = m_xPosition - otherBody.xPosition();
	double ry = m_yPosition - otherBody.yPosition();
	double rz = m_zPosition - otherBody.zPosition();

	return pow(pow(rx, 2) + pow(ry, 2) + pow(rz, 2), 0.5);
}

void Body::addToAccelerationDueToOneBody(const Body& otherBody)
{
	double rx = m_xPosition - otherBody.xPosition();
	double ry = m_yPosition - otherBody.yPosition();
	double rz = m_zPosition - otherBody.zPosition();
	double rCubed = pow(rx*rx + ry*ry + rz*rz + relaxation(), 1.5);

	double magAcc = -G*otherBody.mass() / rCubed;

	m_xAcceleration += magAcc*rx;
	m_yAcceleration += magAcc*ry;
	m_zAcceleration += magAcc*rz;

	return;
};

void Body::accelerationCalc(vector<Body>& binaryStars)
{
	m_xAcceleration = 0;
	m_yAcceleration = 0;
	m_zAcceleration = 0;

	for (int i = 0; i < binaryStars.size(); i++)
	{
		double rx = m_xPosition - binaryStars.at(i).xPosition();
		double ry = m_yPosition - binaryStars.at(i).yPosition();
		double rz = m_zPosition - binaryStars.at(i).zPosition();
		double rCubed = pow(rx*rx + ry*ry + rz*rz + relaxation(), 1.5);
		if (rCubed == 0)
			continue;
		double magAcc = -G*binaryStars.at(i).mass() / rCubed;

		m_xAcceleration += magAcc*rx;
		m_yAcceleration += magAcc*ry;
		m_zAcceleration += magAcc*rz;
	}
	return;
};

const double& Body::xPosition() const
{
	return m_xPosition;
};

const double& Body::yPosition() const
{
	return m_yPosition;
};

const double& Body::zPosition() const
{
	return m_zPosition;
};

const double& Body::xVelocity() const
{
	return m_xVelocity;
};

const double& Body::yVelocity() const
{
	return m_yVelocity;
};

const double& Body::zVelocity() const
{
	return m_zVelocity;
};

const string& Body::name() const
{
	return m_name;
};

const double& Body::mass() const
{
	return m_mass;
};

void Body::set_xPosition(const double& new_xPosition)
{
	m_xPosition = new_xPosition;
	return;
};

void Body::set_yPosition(const double& new_yPosition)
{
	m_yPosition = new_yPosition;
	return;
};

void Body::set_zPosition(const double& new_zPosition)
{
	m_zPosition = new_zPosition;
	return;
};

void Body::set_xVelocity(const double& new_xVelocity)
{
	m_xVelocity = new_xVelocity;
	return;
};

void Body::set_yVelocity(const double& new_yVelocity)
{
	m_yVelocity = new_yVelocity;
	return;
};

void Body::set_zVelocity(const double& new_zVelocity)
{
	m_zVelocity = new_zVelocity;
	return;
};

void Body::set_mass(const double& new_mass)
{
	m_mass = new_mass;
	return;
};

void Body::set_Radius(const double& new_Radius)
{
	m_radius = new_Radius;
	return;
};

const double& Body::radius() const
{
	return m_radius;
};

const bool& Body::isValid() const
{
	return m_isValid;
};

void Body::set_isValid(const bool& new_isValid)
{
	m_isValid = new_isValid;
	return;
}
void Body::set_acceleration(const double & x, const double & y, const double & z)
{
	m_xAcceleration = x;
	m_yAcceleration = y;
	m_zAcceleration = z;
}
;

const double Body::density() const
{
	return m_mass / ((4/3)*M_PI*pow(m_radius, 3));
};

const bool& Body::isTrackingTrajectory() const
{
	return m_logTrajectory;
};

void Body::addToTrajectory(ofstream* trajectory_file)
{
	if(isTrackingTrajectory() == true && trajectory_file != NULL)
	{
		*trajectory_file << m_ID << "\t" << m_xPosition/AU << "\t" << m_yPosition/AU << "\t" << m_zPosition/AU << endl;
	}
};

const double& Body::relaxation() const
{
	return m_relaxation;
};

void Body::update_position_and_velocity(const double& timestep)
{

	m_xPosition += m_xVelocity*timestep + 0.5*m_xAcceleration*timestep*timestep;//s = vt + 1/2at^2
	m_yPosition += m_yVelocity*timestep + 0.5*m_yAcceleration*timestep*timestep;
	m_zPosition += m_zVelocity*timestep + 0.5*m_zAcceleration*timestep*timestep;

	//cout << "vel1: " << m_xVelocity << " " << m_yVelocity << " " << m_zVelocity << endl;
	m_xVelocity += m_xAcceleration*timestep;
	m_yVelocity += m_yAcceleration*timestep;
	m_zVelocity += m_zAcceleration*timestep;
	//cout << "vel2: " << m_xVelocity << " " << m_yVelocity << " " << m_zVelocity << endl;


    //@@@IMPLEMENTING RUNGE KUTTA

 /*   double oldXPos(m_xPosition);
    double oldYPos(m_yPosition);
    double oldZPos(m_zPosition);

    double k1vi1x = m_xAcceleration;
	double k1vi1y = m_yAcceleration;
	double k1vi1z = m_zAcceleration;

	double k1ri1x = m_xVelocity;
	double k1ri1y = m_yVelocity;
	double k1ri1z = m_zVelocity;

	m_xPosition = m_xPosition + k1ri1x*timestep/2;
	m_yPosition = m_yPosition + k1ri1y*timestep/2;
	m_zPosition = m_zPosition + k1ri1z*timestep/2;

    accelerationCalc(Body_Vector);

	double k2vi1x = m_xAcceleration;
	double k2vi1y = m_yAcceleration;
	double k2vi1z = m_zAcceleration;

    double k2ri1x = k1ri1x + k2vi1x*timestep/2;
    double k2ri1y = k1ri1y + k2vi1y*timestep/2;
    double k2ri1z = k1ri1z + k2vi1z*timestep/2;

    m_xPosition = m_xPosition + k2ri1x*timestep/2;
    m_yPosition = m_yPosition + k2ri1y*timestep/2;
    m_zPosition = m_zPosition + k2ri1z*timestep/2;

    accelerationCalc(Body_Vector);

    double k3vi1x = m_xAcceleration;
    double k3vi1y = m_yAcceleration;
    double k3vi1z = m_zAcceleration;

    double k3ri1x = k2ri1x + k3vi1x*timestep/2;
    double k3ri1y = k2ri1y + k3vi1y*timestep/2;
    double k3ri1z = k2ri1z + k3vi1z*timestep/2;

    m_xPosition = m_xPosition + k3ri1x*timestep/2;
    m_yPosition = m_yPosition + k3ri1y*timestep/2;
    m_zPosition = m_zPosition + k3ri1z*timestep/2;

    accelerationCalc(Body_Vector);

    double k4vi1x = m_xAcceleration;
    double k4vi1y = m_yAcceleration;
    double k4vi1z = m_zAcceleration;

    double k4ri1x = k3ri1x + k4vi1x*timestep/2;
    double k4ri1y = k3ri1y + k4vi1y*timestep/2;
    double k4ri1z = k3ri1z + k4vi1z*timestep/2;

    m_xVelocity = m_xVelocity + timestep/6*(k1vi1x + 2*k2vi1x + 2*k3vi1x + k4vi1x);
    m_yVelocity = m_yVelocity + timestep/6*(k1vi1y + 2*k2vi1y + 2*k3vi1y + k4vi1y);
    m_zVelocity = m_zVelocity + timestep/6*(k1vi1z + 2*k2vi1z + 2*k3vi1z + k4vi1z);

    m_xPosition = oldXPos + timestep/6*(k1ri1x + 2*k2ri1x + 2*k3ri1x + k4ri1x);
    m_yPosition = oldYPos + timestep/6*(k1ri1y + 2*k2ri1y + 2*k3ri1y + k4ri1y);
    m_zPosition = oldZPos + timestep/6*(k1ri1z + 2*k2ri1z + 2*k3ri1z + k4ri1z);
    */

	return;
};

const double& Body::xAcceleration() const
{
	return m_xAcceleration;
};

const double& Body::yAcceleration() const
{
	return m_yAcceleration;
};

const double& Body::zAcceleration() const
{
	return m_zAcceleration;
};

const double Body::eccentricity()
{
    m_hx = m_yPosition*m_zVelocity - m_zPosition*m_yVelocity;
    m_hy = m_zPosition*m_xVelocity - m_xPosition*m_zVelocity;
    m_hz = m_xPosition*m_yVelocity - m_yPosition*m_xVelocity;

    double vxhx = (m_yVelocity*m_hz - m_zVelocity*m_hy)/(G*solar_mass);
    double vxhy = (m_zVelocity*m_hx- m_xVelocity*m_hz)/(G*solar_mass);
    double vxhz = (m_xVelocity*m_hy - m_yVelocity*m_hx)/(G*solar_mass);

    double ex = vxhx - m_xPosition/pow(pow(m_xPosition, 2) + pow(m_yPosition, 2) + pow(m_zPosition, 2), 0.5);
    double ey = vxhy - m_yPosition/pow(pow(m_xPosition, 2) + pow(m_yPosition, 2) + pow(m_zPosition, 2), 0.5);
    double ez = vxhz - m_zPosition/pow(pow(m_xPosition, 2) + pow(m_yPosition, 2) + pow(m_zPosition, 2), 0.5);

    return pow(pow(ex, 2) + pow(ey, 2) + pow(ez, 3), 0.5);
}

const double Body::ascendingNode()
{
    m_hx = m_yPosition*m_zVelocity - m_zPosition*m_yVelocity;
    m_hy = m_zPosition*m_xVelocity - m_xPosition*m_zVelocity;
    m_hz = m_xPosition*m_yVelocity - m_yPosition*m_xVelocity;

    double nx = -m_hy;
    double ny = m_hx;
    double ascendingNode(0);

    if (ny >= 0)
        ascendingNode = acos(nx/pow(pow(nx, 2) + pow(ny, 2), 0.5));
    else
        ascendingNode = 2*M_PI - acos(nx/pow(pow(nx, 2) + pow(ny, 2), 0.5));

    return ascendingNode;
}

const double Body::inclination()
{
    m_hx = m_yPosition*m_zVelocity - m_zPosition*m_yVelocity;
    m_hy = m_zPosition*m_xVelocity - m_xPosition*m_zVelocity;
    m_hz = m_xPosition*m_yVelocity - m_yPosition*m_xVelocity;

    return acos(m_hz/sqrt(pow(m_hx, 2) + pow(m_hy, 2) + pow(m_hz, 2)));
}

const bool Body::isBound()
{
	return m_isBound;
}

void Body::setIsBound(const bool status)
{
	m_isBound = status;
}
