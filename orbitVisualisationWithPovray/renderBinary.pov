#version 3.7
#include "/home/portera/projects/PovrayCommandLineMacV2/include/colors.inc"
#include "shapes3.inc"
#include "/home/portera/projects/PovrayCommandLineMacV2/include/stones.inc"
#include "stars.inc"
#include "rad_def.inc"

camera
{
	//location <2, -3.5, 0> //looks at plane of binary if inclination = 30 degs
	location <-1, 0,  -1>
	look_at <0, 0, 0>
}
 light_source {<0, 0, -1>, White }
//global_settings {
//   radiosity {
//      Rad_Settings(Radiosity_OutdoorHQ,on,on)
//   }
//}
//#default {finish{ambient 0}}
 
#declare star = union{sphere // transparent sphere containing media
 { 0,1 pigment { rgbt 1 } hollow
   interior
   { media
     { emission 60 
       density
       { spherical density_map
         { [0 rgb 0]
           [0.03 rgb <1,0,0>]
           [0.2 rgb <1,1,0>]
           [1 rgb 1]
         }
       }
     }
  }
 }
 light_source {<0.1,0.1,0.1>, Grey }
}

#declare circumbinary = sphere
{
	<0,0,0>, 0.01
	texture
	{
		pigment
		{
			color Blue
		}
	}
}

#macro drawOrbit (periastronDistance, Eccentricity, focusX, focusY, focusZ, inclination, argPeriapsis, longAscendingNode)

	object
	{
	torus
	{
		1, 0.002
		rotate -90*x
		texture
		{
			pigment
			{
				color Grey
			}
		}
	}
	
	//Scale ellipse so that is an ellipse, not a circle. x->ax, y->by
	scale x* periastronDistance/(1-Eccentricity) scale y* periastronDistance/(1-Eccentricity)*sqrt(1 - pow(Eccentricity, 2)) 
	
	//Using the periastron distance (position when argPeriapsis==0) and eccentricity, need to place the ellipse so that its focus is at the centre of mass
	//a - focal = periastronDistance
	//f = a*e
	//a = peri/(1-e)
	//f = e*peri/(1-e)
	
	//Translate ellipse so (0,0) is at CoM focus, not center of ellipse
	translate -x*Eccentricity* periastronDistance/(1-Eccentricity)
	
	//Rotations are always around (0,0), so need to do them while CoM focus is at (0,0)
	rotate z*argPeriapsis
	rotate x*inclination
	rotate z*longAscendingNode
	
	translate <focusX, focusY, focusZ>
	}
	
	
	object
	{
		cylinder
		{
			<1000, 0, 0>, <-1000, 0, 0>, 0.001
			texture
			{
				pigment
				{
					color Green
				}
			}
		}
		rotate z*argPeriapsis
		rotate x*inclination
		rotate z*longAscendingNode
		
	}
	object
	{ 
		Segment_of_Torus( 0.05, 0.005, argPeriapsis)
        	pigment
        	{ 
        		Green
        		}
        scale <1,1,1>
        rotate (90)*x
        rotate z*longAscendingNode
        rotate y*inclination
        translate <0,0,0>
      }
      
	object
	{
		cylinder
		{
			<1000, 0, 0>, <-1000, 0, 0>, 0.001
			texture
			{
				pigment
				{
					color Blue
				}
			}
		}
		//rotate z*argPeriapsis
		rotate y*inclination
		//rotate z*longAscendingNode
	}
	object
	{ 
		Segment_of_Torus( 0.05, 0.005, inclination)
        	pigment
        	{ 
        		Blue
        		}
        scale <1,1,1>
        //rotate (-90)*z
        translate <0,0,0>
        //rotate z*argPeriapsis
		//rotate x*inclination
//		rotate z*longAscendingNode
      }
	
		object
	{
		cylinder
		{
			<1000, 0, 0>, <-1000, 0, 0>, 0.001
			texture
			{
				pigment
				{
					color Red
				}
			}
		}
		//rotate z*argPeriapsis
		//rotate x*inclination
		rotate z*longAscendingNode
		
	}
	object
	{ 
		Segment_of_Torus( 0.07, 0.005, longAscendingNode)
        	pigment
        	{ 
        		Red
        		}
        scale <1,1,1>
        rotate (90)*x
        translate <0,0,0>
      }
      
#end
 
//Draw x and y axis as a green cylinder
cylinder
{
        <1000, 0, 0>, <-1000, 0, 0>, 0.001
        texture
	{
		pigment
		{
			color BlueViolet
		}
	}
	
}

cylinder
{
        <0, 1000, 0>, <0,-1000, 0>, 0.001
        texture
	{
		pigment
		{
			color BlueViolet
		}
	}
	
}


  
//object {star scale 0.02}//Pick out origin
#fopen myOrbitFile "orbits.txt" read
#while (defined(myOrbitFile))
  	#read (myOrbitFile, name, xVar, yVar, zVar, ecc, inc, argPeri, longNode)
  	#switch (0)
  		#case(strcmp(name, "primary"))
  		#case(strcmp(name, "circumbinary"))
  			drawOrbit(sqrt(pow(xVar, 2) + pow(yVar, 2) + pow(zVar, 2)), ecc, 0, 0, 0, inc, argPeri, longNode)
  			#break
  			
  		//secondary orbit needs to be defined from the "negative" side of the axis.
  		#case(strcmp(name, "secondary"))
  			drawOrbit(-sqrt(pow(xVar, 2) + pow(yVar, 2) + pow(zVar, 2)), ecc, 0, 0, 0, inc, argPeri, longNode)
  			#break
	#end
#end

#fopen myFile "binaryPosition.txt" read
#while (defined(myFile))
  	#read (myFile, name, xVar, yVar, zVar)
  		object {star scale 0.02 translate x*xVar translate y*yVar translate z*zVar }
#end

