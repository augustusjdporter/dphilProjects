#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <zconf.h>

#ifdef __linux__ 
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#endif

using namespace std;
const double G(6.67408*pow(10, -11));
const double AU(1.4960*pow(10, 11));
const double solarMass(1.989*pow(10, 30));

const vector <double> cartesianPosition3D(const double semiMajorAxis, const double ellipticity, const double trueAnomaly, const double inclination, const double argPeriapsis, const double longAscendingNode)
{
    double r = semiMajorAxis*(1 - pow(ellipticity, 2)) / (1 + ellipticity*cos(trueAnomaly));

    double xPos = r*(cos(longAscendingNode)*cos(trueAnomaly + argPeriapsis) - sin(longAscendingNode)*sin(trueAnomaly + argPeriapsis)*cos(inclination));

    double yPos = r*(sin(longAscendingNode)*cos(trueAnomaly + argPeriapsis) + cos(longAscendingNode)*sin(trueAnomaly + argPeriapsis)*cos(inclination));

    double zPos = r*(sin(trueAnomaly + argPeriapsis)*sin(inclination));

    vector <double> position;

    position.push_back(xPos);
    position.push_back(yPos);
    position.push_back(zPos);

    return position;
};

const double eccentricAnomalyNplus1(const double eccentricAnomalyN, const double eccentricity, const double meanAnomaly)
{
    return eccentricAnomalyN - (eccentricAnomalyN - eccentricity*sin(eccentricAnomalyN) - meanAnomaly)/(1 - eccentricity*cos(eccentricAnomalyN));
};

const double findTrueAnomaly(const double period, const double eccentricity, const double time)
{
    //https://en.wikipedia.org/wiki/Kepler%27s_laws_of_planetary_motion#Position_as_a_function_of_time
    double meanMotion(2*M_PI/period);
    double meanAnomaly(meanMotion*time);

    //need to iteratively solve Keplers equation M = E - e*sinE, where E is eccentric anomaly, and M is mean anomaly
    double eccentricAnomaly = eccentricAnomalyNplus1(meanAnomaly, eccentricity, meanAnomaly);
    while(true)
    {
        double tempEccentricAnomaly = eccentricAnomaly;
        eccentricAnomaly = eccentricAnomalyNplus1(eccentricAnomaly, eccentricity, meanAnomaly);

        if (abs(eccentricAnomaly - tempEccentricAnomaly) != 0 && tempEccentricAnomaly == 0)
            tempEccentricAnomaly = 0.0000000001;
        double percentageChange(abs(eccentricAnomaly - tempEccentricAnomaly)*100/tempEccentricAnomaly);

        cout << eccentricAnomaly << " " << percentageChange << endl;
        //When further iterations dont cause much change, carry on
        if(abs(eccentricAnomaly - tempEccentricAnomaly) == 0 || percentageChange < 0.1)
        {
            break;
        }
    };

    //return the true anomaly
    return 2*atan(sqrt((1 + eccentricity)/(1 - eccentricity))*tan(eccentricAnomaly/2));
};

int main()
{
    std::cout << "Program to print coordinates of heavenly bodies to file and call Povray to render them. " << std::endl;
    const double eccentricity(0.28);
    const double semiMajorAxisBinary(0.6*AU);
    const double semiMajorAxisOrbit(2.97*AU);
    const double massAtFocus(38*solarMass);
    const double massRatio(2.2);

    //M = m1 + m2
    //q = m1/m2
    //=m1/M-m1
    //qM - qm1 = m1
    //qM = (1+q)m1
    //m1 = qM/(1+q)
    const double massPrimary(massRatio*massAtFocus/(1+massRatio));
    const double massSecondary(massAtFocus - massPrimary);

    double a1(massSecondary*semiMajorAxisBinary/(massPrimary + massSecondary));
    double a2(-massPrimary*semiMajorAxisBinary/(massPrimary + massSecondary));//minus, so star starts in minus x axis, and velocity takes into account

    double a1oAU(a1/AU);
    double a2oAU(a2/AU);

    double trueAnomaly(0);
    double inclination(63.);
    double argPeriapsis(272.*M_PI/180.);
    double longAscendingNode(M_PI/2);

    vector<double> primaryStarInitialPosition(cartesianPosition3D(a1, eccentricity, trueAnomaly, inclination, argPeriapsis, longAscendingNode));
    vector<double> secondaryStarInitialPosition(cartesianPosition3D(a2, eccentricity, trueAnomaly, inclination, argPeriapsis, longAscendingNode));

    vector<double> testParticleInitialPosition(cartesianPosition3D(semiMajorAxisOrbit, 0/*ecc*/, trueAnomaly, inclination, argPeriapsis, longAscendingNode));

    ofstream orbitFile;
    orbitFile.open("orbits.txt");
    orbitFile << "\"primary\", " << primaryStarInitialPosition.at(0)/AU << ", " << primaryStarInitialPosition.at(1)/AU << ", " << primaryStarInitialPosition.at(2)/AU << ", " << eccentricity << ", " << inclination*180/M_PI << ", " << argPeriapsis*180/M_PI << ", " << longAscendingNode*180/M_PI << ", " << endl;
    orbitFile << "\"secondary\", " << secondaryStarInitialPosition.at(0)/AU << ", " << secondaryStarInitialPosition.at(1)/AU << ", " << secondaryStarInitialPosition.at(2)/AU << ", " << eccentricity << ", " << inclination*180/M_PI << ", " << argPeriapsis*180/M_PI << ", " << longAscendingNode*180/M_PI << ", " << endl;
    orbitFile << "\"circumbinary\", " << testParticleInitialPosition.at(0)/AU << ", " << testParticleInitialPosition.at(1)/AU << ", " << testParticleInitialPosition.at(2)/AU << ", " << 0 << ", " << inclination*180/M_PI << ", " << argPeriapsis*180/M_PI << ", " << longAscendingNode*180/M_PI << ", " << endl;
    orbitFile.close();


    const int imax(360);
    for (int i = 0; i < imax; i++)
    {
        cout << i << endl;
        trueAnomaly = 2*M_PI/imax * i;
        ofstream file;

        file.open("/Data/binaryPosition.txt");

        vector<double> primaryStarPosition(cartesianPosition3D(a1, eccentricity, trueAnomaly, inclination, argPeriapsis, longAscendingNode));
        vector<double> secondaryStarPosition(cartesianPosition3D(a2, eccentricity, trueAnomaly, inclination, argPeriapsis, longAscendingNode));
        //vector<double> testParticlePosition(cartesianPosition3D(semiMajorAxisOrbit, 0ecc, trueAnomaly, inclination, argPeriapsis, longAscendingNode));

        file << "\"primary\", " << primaryStarPosition.at(0)/AU << ", " << primaryStarPosition.at(1)/AU << ", " << primaryStarPosition.at(2)/AU << "," << endl;
        file << "\"secondary\", " << secondaryStarPosition.at(0)/AU << ", " << secondaryStarPosition.at(1)/AU << ", " << secondaryStarPosition.at(2)/AU << "," << endl;
        //file << "\"circumbinary\", " << testParticlePosition.at(0)/AU << ", " << testParticlePosition.at(1)/AU << ", " << testParticlePosition.at(2)/AU << "," << endl;
        file.close();

        stringstream plotNumber;
        plotNumber << i;
        string command("/home/portera/projects/PovrayCommandLineMacV2/Povray37UnofficialMacCmd -V +I/home/portera/privateWork/orbitVisualisationWithPovray/renderBinary.pov +L/home/portera/projects/PovrayCommandLineMacV2/include +J +Q11 +W1280 +H960 Output_File_Type=N +A +AM2 +R3 +THFS +o/Data/binary/binary" + plotNumber.str() + ".png");
        //string command("/home/portera/projects/PovrayCommandLineMacV2/Povray37UnofficialMacCmd  /home/portera/privateWork/orbitVisualisationWithPovray/renderBinary.ini +o/Data/binary/binary" + plotNumber.str() + ".png");
        system(command.c_str());
    }

//    double binaryPeriod(sqrt(4*pow(M_PI, 2)*pow(semiMajorAxisBinary, 3)/(G*massAtFocus)));
//    double circumbinaryPeriod(sqrt(4*pow(M_PI, 2)*pow(semiMajorAxisOrbit, 3)/(G*massAtFocus)));
//
//
//    double timestep = binaryPeriod/imax;
//
//    double binaryTrueAnomaly = findTrueAnomaly(binaryPeriod, eccentricity, trueAnomaly);
//    double circumbinaryTrueAnomaly = findTrueAnomaly(circumbinaryPeriod, 0, trueAnomaly);
//
//    ofstream file;
//
//    file.open("binaryPosition.txt");
//
//    vector<double> primaryStarPosition(cartesianPosition3D(a1, eccentricity, binaryTrueAnomaly, inclination, argPeriapsis, longAscendingNode));
//    vector<double> secondaryStarPosition(cartesianPosition3D(a2, eccentricity, binaryTrueAnomaly, inclination, argPeriapsis, longAscendingNode));
//    vector<double> testParticlePosition(cartesianPosition3D(semiMajorAxisOrbit, 0/*ecc*/, circumbinaryTrueAnomaly, inclination, argPeriapsis, longAscendingNode));
//
//    file << "\"primary\", " << primaryStarPosition.at(0)/AU << ", " << primaryStarPosition.at(1)/AU << ", " << primaryStarPosition.at(2)/AU << "," << endl;
//    file << "\"secondary\", " << secondaryStarPosition.at(0)/AU << ", " << secondaryStarPosition.at(1)/AU << ", " << secondaryStarPosition.at(2)/AU << "," << endl;
//    file << "\"circumbinary\", " << testParticlePosition.at(0)/AU << ", " << testParticlePosition.at(1)/AU << ", " << testParticlePosition.at(2)/AU << "," << endl;
//    file.close();
//
//    stringstream plotNumber;
//    //plotNumber << i;
//    #ifdef __APPLE__
//    //string command("/home/portera/projects/PovrayCommandLineMacV2/Povray37UnofficialMacCmd -V +I/home/portera/privateWork/orbitVisualisationWithPovray/renderBinary.pov +L/home/portera/projects/PovrayCommandLineMacV2/include +J +Q11 +W1280 +H960 Output_File_Type=N +A +AM2 +R3 +THFS +o/Data/binary/binary" + plotNumber.str() + ".png");
//    //string command("/home/portera/projects/PovrayCommandLineMacV2/Povray37UnofficialMacCmd  /home/portera/privateWork/orbitVisualisationWithPovray/renderBinary.ini +o/Data/binary/binary" + plotNumber.str() + ".png");
//    //system(command.c_str());
//
//    #elif __linux__
//    string command("povray  renderBinaryUbuntu.ini +o/home/augustus/Data/binary/binary" + plotNumber.str() + ".png");
//    system(command.c_str());
//    #endif



    return 0;
};