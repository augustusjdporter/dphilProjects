import matplotlib.pyplot as plt
import numpy as np


x_points = np.linspace(0., 2.*np.pi, 200)

sin_x = np.sin(x_points)
cos_x = np.cos(x_points)
tan_x = sin_x/cos_x
exp_x = np.exp(x_points)


f, axes_array = plt.subplots(2, 1, sharex=True)
axes_array[0].plot(x_points, sin_x, label = 'sin(x)')
axes_array[0].plot(x_points, cos_x, label = 'cos(x)')
axes_array[0].set_title('Sharing X axis')
axes_array[1].scatter(x_points, tan_x, label = 'tan(x)')

axes_array[0].legend(loc = 'upper right')
axes_array[1].legend(loc = 'upper right')
axes_array[1].set_ylim(-50., 50)

axes_array[1].set_xlabel("Phase")

plt.show()
