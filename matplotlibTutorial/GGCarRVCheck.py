import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit, least_squares, leastsq

import os

dir_path = os.path.dirname(os.path.realpath(__file__))

def eccentricAnomalyNPlus1(eccentricAnomalyN, eccentricity, meanAnomaly):
    return eccentricAnomalyN - (eccentricAnomalyN - eccentricity*np.sin(eccentricAnomalyN) - meanAnomaly)/(1 - eccentricity*np.cos(eccentricAnomalyN))


def getTrueAnomaly(phase, eccentricity):
    meanAnomaly = 2*np.pi*phase
    eccentricAnomaly = eccentricAnomalyNPlus1(meanAnomaly, eccentricity, meanAnomaly)
    while True:
        tempEccentricAnomaly = eccentricAnomaly
        eccentricAnomaly = eccentricAnomalyNPlus1(eccentricAnomaly, eccentricity, meanAnomaly)
        if abs(eccentricAnomaly - tempEccentricAnomaly) != 0 and tempEccentricAnomaly == 0: #stop division by zero at phase = 0
            tempEccentricAnomaly = 0.0000000001
        percentageChange = abs(eccentricAnomaly - tempEccentricAnomaly)*100./tempEccentricAnomaly
        if abs(eccentricAnomaly - tempEccentricAnomaly) == 0 or abs(percentageChange) < 0.1:
            break
    return 2*np.arctan(np.sqrt((1+eccentricity)/(1-eccentricity))*np.tan(eccentricAnomaly/2))


def velocityCurve(phase, amplitude, argPeriaps, eccentricity, systematicVel):
    trueAnomaly = []
    for ph in phase:
        truAnom = getTrueAnomaly(ph, eccentricity)
        trueAnomaly.append(truAnom)
    trueAnomaly = np.array(trueAnomaly)
    return amplitude*(np.cos(trueAnomaly + argPeriaps) + eccentricity*np.cos(argPeriaps)) + systematicVel


def errorFunc(params, *args):
    firstPhases = args[0]
    firstData = args[1]
    secondPhases = args[2]
    secondData = args[3]

    firstAmplitude = params[0]
    secondAmplitude = params[1]

    eccentricity = params[2]
    argPeriaps = params[3]
    systematicVel = params[4]

    firstVelocity = velocityCurve(firstPhases, firstAmplitude, argPeriaps, eccentricity, systematicVel)
    secondVelocity = velocityCurve(secondPhases, secondAmplitude, argPeriaps, eccentricity, systematicVel)

    error = np.array(abs(firstData - firstVelocity))
    error = np.concatenate((error, abs(secondData - secondVelocity)))

    return error

whitePhaseVals = np.loadtxt(dir_path + "/GGCarRVWhite.txt", usecols=range(0, 1))
whiteVelVals = np.loadtxt(dir_path + "/GGCarRVWhite.txt", usecols=range(1, 2))

blackPhaseVals = np.loadtxt(dir_path + "/GGCarRVBlack.txt", usecols=range(0, 1))
blackVelVals = np.loadtxt(dir_path + "/GGCarRVBlack.txt", usecols=range(1, 2))





firstAmplitude = 65.
secondAmplitude = -65.
eccentricity = 0.0
argPeriapsis = 0.*np.pi/180.
systematicVel = -150.


p0 = np.array([firstAmplitude, secondAmplitude, eccentricity, argPeriapsis, systematicVel])
minBounds = [-999999999999., -999999999999., 0., 0., -999999999999.]
maxBounds = [999999999999., 999999999999., 1., 2*np.pi, 99999999999.]

args = np.array([whitePhaseVals, whiteVelVals, blackPhaseVals, blackVelVals])
p_best, covar, info, errmsg, ier = leastsq(errorFunc, p0, args=(whitePhaseVals, whiteVelVals, blackPhaseVals, blackVelVals), full_output=True)

phaseVals = [-0.5 + 2.*i/100. for i in range (101)]
print (p_best)
print (covar)
result = p_best

firstAmplitude = result[0]
secondAmplitude = result[1]
eccentricity = result[2]
argPeriapsis = result[3]
systematicVel = result[4]

fitWhite = velocityCurve(np.array(phaseVals), firstAmplitude, argPeriapsis, eccentricity, systematicVel)
fitBlack = velocityCurve(np.array(phaseVals), secondAmplitude, argPeriapsis, eccentricity, systematicVel)

plt.scatter(whitePhaseVals, whiteVelVals)
plt.scatter(blackPhaseVals, blackVelVals)

plt.plot(phaseVals, fitWhite)
plt.plot(phaseVals, fitBlack)


print ('k1', abs(firstAmplitude), abs(firstAmplitude)*np.sqrt(covar[0][0]))
print ('k2', abs(secondAmplitude), abs(secondAmplitude)*np.sqrt(covar[1][1]))
print ('arg', argPeriapsis*180./np.pi, abs(argPeriapsis*180./np.pi)*np.sqrt(covar[3][3]))
print ('ecc', eccentricity, eccentricity*np.sqrt(covar[2][2]))
print ('vel', systematicVel, systematicVel*np.sqrt(covar[4][4]))

plt.show()
