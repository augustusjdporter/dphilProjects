import matplotlib.pyplot as plt
import numpy as np

x_points = np.linspace(0., 2. * np.pi, 200)

sin_x = np.sin(x_points)
cos_x = np.cos(x_points)
tan_x = sin_x / cos_x
exp_x = np.exp(x_points)

f, ax1 = plt.subplots()
ax2 = ax1.twinx()

ax1.plot(x_points, sin_x)
ax2.plot(x_points, exp_x, 'r')

ax1.set_xlabel("Phase")
ax1.set_ylabel("sin")
ax2.set_ylabel("exp")

plt.show()