import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, 100, 10)
plt.scatter(x, x**2)
x = np.linspace(0, 100, 1000)
plt.plot(x, x**2)

plt.title("y = x$^{2}$")
plt.xlabel("x")
plt.ylabel("y")
plt.show()