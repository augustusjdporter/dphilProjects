import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
from scipy.signal import medfilt2d
from matplotlib.colors import LogNorm
from astropy.visualization import (MinMaxInterval, SqrtStretch,
                                   ImageNormalize)

filePath = "/Data/GlobalJetWatch/Aquila/NGC3918PN-0001_1000s.fits"

SIGNAL_MASK_UPPER_BOUND = 60000
SIGNAL_MASK_LOWER_BOUND = 1300
NUMBER_OF_PEAKS_TO_FIND = 10

def choose_island_bounds(x, y, a):
    """
    @param x: x coordinate of peak of island-to-be.
    @param y: y coordinate of peak of island-to-be.
    @param a: <numpy.ndarray> - full image containing peak.
    @return: list containing coordinates defining an island.
    """
    if a[x, y] > 10000:
        bg = np.median(a)
        i = x
        threshold = bg + (a[x, y] - bg) / 5
        while a[i, y] >= threshold:
            i -= 1
            if i == 0:
                break
        x_lower = i
        i = x
        while a[i, y] >= threshold:
            i += 1
            if i == a.shape[0]:
                break
        x_upper = i
        i = y
        while a[x, i] >= threshold:
            i -= 1
            if i == 0:
                break
        y_lower = i
        i = y
        while a[x, i] >= threshold:
            i += 1
            if i == a.shape[1]:
                break
        y_upper = i
    else:
        # Check inputs.
        assert(isinstance(a, np.ndarray))
        radius = 10
        # First go left until pixels stop decreasing.
        i = x
        previous = a[x, y]

        while (SIGNAL_MASK_LOWER_BOUND < a[i, y] <= previous) or abs(i-x) < radius:
            previous = a[i, y]
            i -= 1
            if i == a.shape[1]:
                break
        x_lower = i
        # Go right until pixels stop decreasing.
        i = x
        previous = a[x, y]
        while (SIGNAL_MASK_LOWER_BOUND < a[i, y] <= previous) or abs(i-x) < radius:
            previous = a[i, y]
            i += 1
            if i == a.shape[0]:
                break
        x_upper = i
        # Go down until pixels stop decreasing.
        i = y
        previous = a[x, y]
        while (SIGNAL_MASK_LOWER_BOUND < a[x, i] <= previous) or abs(i-y) < radius:
            previous = a[x, i]
            i -= 1
            if i == a.shape[1]:
                break
        y_lower = i
        # Go up until pixels stop decreasing.
        i = y
        previous = a[x, y]
        while (SIGNAL_MASK_LOWER_BOUND < a[x, i] <= previous) or abs(i-y) < radius:
            previous = a[x, i]
            i += 1
            if i == a.shape[1]:
                break
        y_upper = i
    return [x_lower, x_upper, y_lower, y_upper]

def get_brightest_pixels(data):
    x_list = []
    y_list = []
    x_len_list = []
    y_len_list = []
    peaks = []
    blocked_data = data
    for i in range(NUMBER_OF_PEAKS_TO_FIND):
        (x, y) = np.unravel_index(blocked_data.argmax(), blocked_data.shape)
        x_list.append(x)
        y_list.append(y)
        peaks.append(blocked_data[x, y])

        [x_lower, x_upper, y_lower, y_upper] = choose_island_bounds(x, y, data)

        x_len = x_upper - x_lower
        y_len = y_upper - y_lower
        x_len_list.append(x_len)
        y_len_list.append(y_len)


        for x1 in range(x_len):
            if x_lower + x1 < blocked_data.shape[0] and x_lower + x1 >= 0:
                for y1 in range(y_len):
                    if y_lower + y1 < blocked_data.shape[1] and y_lower + y1 >= 0:
                        blocked_data[x_lower + x1, y_lower + y1] = 0

    return (x_list, y_list, x_len_list, y_len_list, peaks)

def radial_profile(data, center, x_len, y_len):
    y, x = np.indices((data.shape))
    r = np.sqrt((x - center[0])**2 + (y - center[1])**2)
    r = r.astype(np.int)
    r = r.ravel()
    data = data.ravel()
    data = data[r < np.sqrt(x_len**2 + y_len**2)]
    r = r[r < np.sqrt(x_len**2 + y_len**2)]


    tbin = np.bincount(r, data)
    nr = np.bincount(r)
    radialprofile = tbin / nr
    return radialprofile



hdulist = fits.open(filePath)

data = hdulist[0].data
flat_data = np.array(data, dtype=np.float32)
flat_data = medfilt2d(flat_data, kernel_size=[3, 3])
y_list, x_list, y_len_list, x_len_list, peaks = get_brightest_pixels(flat_data)
plt.imshow(data, cmap='gray', norm=LogNorm())
plt.gray()
# plot the fitted centers of the gaussian
plt.scatter(x_list, y_list)
plt.show()
for x, y, x_len, y_len in zip(x_list, y_list, x_len_list, y_len_list):
    radialProfile = radial_profile(data, [x,y], x_len, y_len)
    xVals = range(len(radialProfile))
    plt.title("Center: {}, {}".format(x,y))
    plt.xlabel("Pixels from center")
    plt.ylabel("Azimuthal average")
    plt.plot(xVals, radialProfile)
    plt.show()
hdulist.close()