from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize
import os
import time
import signal

# following class courtesy of http://stackoverflow.com/questions/2281850/timeout-function-if-it-takes-too-long-to-finish
class timeout:
    def __init__(self, seconds=1, error_message='Timeout'):
        self.seconds = seconds
        self.error_message = error_message
    def handle_timeout(self, signum, frame):
        raise TimeoutError(self.error_message)
    def __enter__(self):
        signal.signal(signal.SIGALRM, self.handle_timeout)
        signal.alarm(self.seconds)
    def __exit__(self, type, value, traceback):
        signal.alarm(0)

def removeBackground(data, A, B, C, i, j):
    newData = data - (A * j + B * i + C)
    return newData

def two_twoDimGaussian(p, x, y, data):
    xMean1 = p[0]
    xMean2 = p[1]
    yMean1 = p[2]
    yMean2 = p[3]
    amp1 = p[4]
    amp2 = p[5]
    sigma1 = p[6]
    sigma2 = p[7]
    base = p[8]

    xVar1 = (x - xMean1)**2 / (2*sigma1 ** 2)
    yVar1 = (y - yMean1)**2 / (2*sigma1 ** 2)

    xVar2 = (x - xMean2) ** 2 / (2 * sigma2 ** 2)
    yVar2 = (y - yMean2) ** 2 / (2 * sigma2 ** 2)

    component1 = amp1 * np.exp(-(xVar1 + yVar1))
    component2 = amp2 * np.exp(-(xVar2 + yVar2))

    answer = base + component1 + component2

    differences = answer - data
    return differences.flatten()

def findPixelInBrightestRegions(data):
    numberOfPixelsToSum = 2 # adds up a square 2N by 2N large
    pixelsToBlock = 40 # blocks out an area of this size around the first peak it finds, so that it does not return the same one
    brightnessMap = np.zeros(data.shape)
    for i in range(numberOfPixelsToSum, len(data) - numberOfPixelsToSum, numberOfPixelsToSum):
        for j in range(numberOfPixelsToSum, len(data[0]) - numberOfPixelsToSum, numberOfPixelsToSum):
            for k in range(-numberOfPixelsToSum, numberOfPixelsToSum):
                for l in range(-numberOfPixelsToSum, numberOfPixelsToSum):
                    brightnessMap[i][j] += data[i + k][j + l]

    firstMax = np.unravel_index(brightnessMap.argmax(), brightnessMap.shape)
    print ('first order (y, x):', firstMax)

    for k in range(-pixelsToBlock, pixelsToBlock):
        for l in range(-pixelsToBlock, pixelsToBlock):
            brightnessMap[firstMax[0] + k][firstMax[1] + l] = 0.
    secondMax = np.unravel_index(brightnessMap.argmax(), brightnessMap.shape)
    print('first order (y, x):', secondMax)
    return (firstMax, secondMax)


directory = '/Data/blobFitting'

for f in os.listdir(directory):
    filePath = directory + '/' + f
    if not os.path.isfile(filePath) or ((f.rsplit('.', 1)[1] != 'fit' and f.rsplit('.', 1)[1] != 'fits')):
        continue
    print(filePath)

    hdulist = fits.open(filePath)

    data = hdulist[0].data


    print ()

    start = time.clock()
    # get x values, y values and data into 1D arrays
    dataOneDim = []
    y = []
    x = []
    for i in range (data.shape[0]):
        for j in range (data.shape[1]):
            y.append(i)
            x.append(j)
            dataOneDim.append(data[i][j])
    x = np.array(x)
    y = np.array(y)
    dataOneDim = np.array(dataOneDim)
    average = np.sum(dataOneDim) / len(dataOneDim)

    firstMax, secondMax = findPixelInBrightestRegions(data)

    # average + 3 sigma
    acceptablePeakValue = average + 3. * np.sqrt(average)

    if data[firstMax[0]][firstMax[1]] < acceptablePeakValue and data[secondMax[0]][secondMax[1]] < acceptablePeakValue:
        print ("Signal to noise too low")
        hdulist.close()
        print()
        continue
    elif data[firstMax[0]][firstMax[1]] < acceptablePeakValue or data[secondMax[0]][secondMax[1]] < acceptablePeakValue:
        print("Could only find one peak with acceptable S/N")
        hdulist.close()
        print()
        continue

    initialGuess = np.array([firstMax[1], secondMax[1], firstMax[0], secondMax[0], data[firstMax[0]][firstMax[1]], data[secondMax[0]][secondMax[1]], 5., 5., average])
    minBounds = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
    maxBounds = np.array([data.shape[1], data.shape[1], data.shape[0], data.shape[0], np.inf, np.inf, data.shape[0]/4., data.shape[0]/4., np.inf])

    try:
        with timeout(seconds = 10):
            popt = scipy.optimize.least_squares(two_twoDimGaussian, initialGuess, method='lm', args = (x, y, dataOneDim))
        end = time.clock()
        elapsed = end - start

        print ("Time elapsed: ", elapsed)
        hdulist.close()

        # plot the original image in gray scale
        plt.imshow(data)
        plt.gray()
        # plot the fitted centers of the gaussian
        plt.scatter([popt.x[0], popt.x[1]], [popt.x[2], popt.x[3]])
        print('1st max center (x, y):', popt.x[0], popt.x[2])
        print('2nd max center (x, y):', popt.x[1], popt.x[3])
        print()
        plt.show()

    except TimeoutError:
        print ("Fitting took too long. Make sure there is high S/N and two distinct peaks.\n")
