from mpi4py import MPI
from binary import circumbinary_disc_run
from amuse.units import units

# GGCar
circumbinary_disc_run(    tend=401. | units.yr,         # simulation time
                          Ngas=10000,                 # number of gas particles
                          m1=26.  | units.MSun,     # primary mass
                          m2=12. | units.MSun,     # secondary mass
                          r1=32.  | units.RSun,     # primary radius
                          r2=32. | units.RSun,     # secondary radius
                          ecc=0.28,                 # binary orbit eccentricity
                          Pbinary=31.033 | units.day,   # binary orbit period
                          Rmin=0. | units.AU,                    # inner edge of initial disk
                          Rmax=9. | units.AU,                    # out edge of initial disk
                          q_out=12.,                   # outer disk Toomre Q parameter
                          discfraction=0.01,           # disk mass fraction
                          Raccretion=30.  | units.RSun,   # accretion radius for sink particle
                          dt_int=0.5 | units.day,      # timestep for gas - binary grav interaction (bridge timestep)
                          Pplanet=228.776 | units.day, # period of planet (makes the r-phi map rotate with this period)
                          densitypower=1.,             # surface density powerlaw
                          eosfreq=8,                   # times between EOS updates/sink particle checks
                          mapfreq=1,                   # time between maps ( in units of dt=eosfreq*dt_int)
                          Lmap=2. | units.AU,          # size of map
                          outputfreq=100,              # output snapshot frequency (every ... dt=eosfreq*dt_int)
                          outputdir='../output/GGCar_circumbinary',      # output directory
                          label='16',                  # label for run (only for terminal output)
                          alpha=0.5,                   # viscosity alpha
                          beta=1.,                     # viscosity beta
                          balsara=False,               # balsara viscosity switch
                          overwrite=True,
                          ejection_velocity=0. | units.kms,
                          restart=False,
                          binary_filename = 'bin-000800',
                          gas_filename = 'disc-000800')
