import numpy as np
G = (6.67408*pow(10, -11))
AU = (1.4960*pow(10, 11))
solarMass = (1.989*pow(10, 30))
M_PI = np.pi


def getVelocityFromOrbitInPolar(semiMajorAxis, eccentricity, massAtFocus, trueAnomaly):
    # orbitalPeriod = sqrt(4*pow(M_PI, 2)*pow(semiMajorAxis, 3)/(G*massAtFocus))
    orbitalPeriod = np.sqrt(4 * np.pi**2 * semiMajorAxis**3 / (G * massAtFocus))

    velocity = []

    # velocity.push_back(2*M_PI*semiMajorAxis*ellipticity*sin(trueAnomaly)/(orbitalPeriod*sqrt(1-pow(ellipticity, 2))));//r comp
    velocity.append(2*np.pi*semiMajorAxis*eccentricity*np.sin(trueAnomaly)/(orbitalPeriod*np.sqrt(1-eccentricity**2)))
    # velocity.push_back(2*M_PI*semiMajorAxis*(1+ellipticity*cos(trueAnomaly))/(orbitalPeriod*sqrt(1-pow(ellipticity, 2))));//phi comp
    velocity.append(2*np.pi*semiMajorAxis*(1+eccentricity*np.cos(trueAnomaly))/(orbitalPeriod*np.sqrt(1-eccentricity**2)))

    return velocity

def getBinaryVelocityFromOrbitInPolar(semiMajorAxis, ellipticity, massPrimary, massSecondary, trueAnomaly):

    primary_velocity = []
    secondary_velocity = []

    massAtFocus = massPrimary + massSecondary
    orbitalPeriod = np.sqrt(4 * np.pi ** 2 * semiMajorAxis ** 3 / (G * massAtFocus))

    # print("orbitalPeriod:",orbitalPeriod/24/3600)
    # //m1a1 = m2a2
    # //a = a1 + a2
    # //a1 = m2*a/M
    a1 = (massSecondary*semiMajorAxis/(massPrimary + massSecondary))
    a2 = (-massPrimary*semiMajorAxis/(massPrimary + massSecondary))#;//minus, so star starts in minus x axis, and velocity takes into account
    # primaryVelocity.push_back(2*M_PI*a1*ellipticity*sin(trueAnomaly)/(orbitalPeriod*sqrt(1-pow(ellipticity, 2))));      //v_r
    primary_velocity.append(2*M_PI*a1*ellipticity*np.sin(trueAnomaly)/(orbitalPeriod*np.sqrt(1-pow(ellipticity, 2))))
    # primaryVelocity.push_back(2*M_PI*a1*(1+ellipticity*cos(trueAnomaly))/(orbitalPeriod*sqrt(1-pow(ellipticity, 2))));  //v_phi
    primary_velocity.append(2*M_PI*a1*(1+ellipticity*np.cos(trueAnomaly))/(orbitalPeriod*np.sqrt(1-pow(ellipticity, 2))))

    # secondaryVelocity.push_back(2*M_PI*a2*ellipticity*sin(trueAnomaly)/(orbitalPeriod*sqrt(1-pow(ellipticity, 2))));
    secondary_velocity.append(2*M_PI*a2*ellipticity*np.sin(trueAnomaly)/(orbitalPeriod*np.sqrt(1-pow(ellipticity, 2))))
    # secondaryVelocity.push_back(2*M_PI*a2*(1+ellipticity*cos(trueAnomaly))/(orbitalPeriod*sqrt(1-pow(ellipticity, 2))));
    secondary_velocity.append(2*M_PI*a2*(1+ellipticity*np.cos(trueAnomaly))/(orbitalPeriod*np.sqrt(1-pow(ellipticity, 2))))

    return primary_velocity, secondary_velocity

def getCartesianVelocityFromPolar(polarVelocity, theta):
    # vector<double> velocity;
    # //cout << "phi: " << theta << endl;
    velocity = []
    velocity.append(polarVelocity[0]*np.cos(theta) - polarVelocity[1]*np.sin(theta))
    velocity.append(polarVelocity[0]*np.sin(theta) + polarVelocity[1]*np.cos(theta))

    return velocity

def magnitudeOfVectorInViewingDirection(cartVelocity, viewingAngle, inclination):
    # //Looking in x direction
    # if len(cartVelocity) == 2:
    return (cartVelocity[0]*np.cos(viewingAngle) + cartVelocity[1]*np.sin(viewingAngle))*np.sin(inclination)
    # else:
    #     return cartVelocity[2]

    # //Looking in z, x plane
    # //return cartVelocity.at(2)*cos(viewingAngle) + cartVelocity.at(0)*sin(viewingAngle);

def cartesianPosition3D(semiMajorAxis, ellipticity, trueAnomaly, inclination, argPeriapsis, longAscendingNode):
    r = semiMajorAxis*(1 - ellipticity**2) / (1 + ellipticity*np.cos(trueAnomaly))

    xPos = r*(np.cos(longAscendingNode)*np.cos(trueAnomaly + argPeriapsis) - np.sin(longAscendingNode)*np.sin(trueAnomaly + argPeriapsis)*np.cos(inclination))

    yPos = r*(np.sin(longAscendingNode)*np.cos(trueAnomaly + argPeriapsis) + np.cos(longAscendingNode)*np.sin(trueAnomaly + argPeriapsis)*np.cos(inclination))

    zPos = r*(np.sin(trueAnomaly + argPeriapsis)*np.sin(inclination))

    position = []

    position.append(xPos)
    position.append(yPos)
    position.append(zPos)

    return position


def cartesianVelocity3D(semiMajorAxis, ellipticity, massAtFocus, trueAnomaly, inclination, argPeriapsis, longAscendingNode):

    orbitalPeriod = np.sqrt(4 * pow(M_PI, 2)*pow(semiMajorAxis, 3) / (G*massAtFocus))

    velocityMagnitude = (2*M_PI*semiMajorAxis/(orbitalPeriod*np.sqrt(1-pow(ellipticity, 2))))
    velocity = []

    vx = ellipticity*(np.sin(longAscendingNode)*np.cos(argPeriapsis)*np.cos(inclination) + np.cos(longAscendingNode)*np.sin(argPeriapsis)) + np.sin(longAscendingNode)*np.cos(inclination)*np.cos(trueAnomaly + argPeriapsis) + np.cos(longAscendingNode)*np.sin(trueAnomaly + argPeriapsis)
    velocity.append(-velocityMagnitude*vx)

    vy = ellipticity*(np.cos(longAscendingNode)*np.cos(argPeriapsis)*np.cos(inclination) - np.sin(longAscendingNode)*np.sin(argPeriapsis)) + np.cos(longAscendingNode)*np.cos(inclination)*np.cos(trueAnomaly + argPeriapsis) - np.sin(longAscendingNode)*np.sin(trueAnomaly + argPeriapsis)
    velocity.append(velocityMagnitude*vy)

    vz = np.sin(inclination)*(np.cos(trueAnomaly + argPeriapsis) + ellipticity*np.cos(argPeriapsis))
    velocity.append(velocityMagnitude*vz)
    return velocity

def eccentricAnomalyNPlus1(eccentricAnomalyN, eccentricity, meanAnomaly):
    return eccentricAnomalyN - (eccentricAnomalyN - eccentricity*np.sin(eccentricAnomalyN) - meanAnomaly)/(1 - eccentricity*np.cos(eccentricAnomalyN))

def getTrueAnomaly(phase, eccentricity):
    meanAnomaly = 2*np.pi*phase
    eccentricAnomaly = eccentricAnomalyNPlus1(meanAnomaly, eccentricity, meanAnomaly)
    while True:
        tempEccentricAnomaly = eccentricAnomaly
        eccentricAnomaly = eccentricAnomalyNPlus1(eccentricAnomaly, eccentricity, meanAnomaly)
        if abs(eccentricAnomaly - tempEccentricAnomaly) != 0 and tempEccentricAnomaly == 0: #stop division by zero at phase = 0
            tempEccentricAnomaly = 0.0000000001
        percentageChange = abs(eccentricAnomaly - tempEccentricAnomaly)*100./tempEccentricAnomaly
        if abs(eccentricAnomaly - tempEccentricAnomaly) == 0 or abs(percentageChange) < 0.1:
            break
    return 2*np.arctan(np.sqrt((1+eccentricity)/(1-eccentricity))*np.tan(eccentricAnomaly/2))

def velocityCurve(phase, amplitude, argPeriaps, eccentricity, systematicVel):
    trueAnomaly = []
    for ph in phase:
        truAnom = getTrueAnomaly(ph, eccentricity)
        trueAnomaly.append(truAnom)
    trueAnomaly = np.array(trueAnomaly)
    return amplitude*(np.cos(trueAnomaly + argPeriaps) + eccentricity*np.cos(argPeriaps)) + systematicVel


def getPhaseFromEphemeris(julianTime, period, startingPoint, reduce=True):
    if period is None:
        return julianTime

    E = (julianTime - startingPoint) / period
    phase = E
    if reduce:
        phase = E % 1
    return phase

def turning_points(array):
    ''' turning_points(array) -> min_indices, max_indices
    Finds the turning points within an 1D array and returns the indices of the minimum and
    maximum turning points in two separate lists.
    '''
    idx_max = []
    idx_min = []
    if (len(array) < 3):
        return idx_min, idx_max

    NEUTRAL, RISING, FALLING = range(3)
    def get_state(a, b):
        if a < b: return RISING
        if a > b: return FALLING
        return NEUTRAL

    ps = get_state(array[0], array[1])
    begin = 1
    for i in range(2, len(array)):
        s = get_state(array[i - 1], array[i])
        if s != NEUTRAL:
            if ps != NEUTRAL and ps != s:
                if s == FALLING:
                    idx_max.append((begin + i - 1) // 2)
                else:
                    idx_min.append((begin + i - 1) // 2)
            begin = i
            ps = s
    return np.array(idx_min), np.array(idx_max)