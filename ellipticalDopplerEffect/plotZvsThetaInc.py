import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.cm as cm


from matplotlib import rc

colors = cm.rainbow(np.linspace(0, 1, 9))

for inc in range (0, 8):
    f, (ax1, ax2) = plt.subplots(1, 2, sharey=True, gridspec_kw=dict(width_ratios=[4, 1]))
    incNum = inc*0.125

    #incNum = 63
    stringIncNum = str(incNum)
    if stringIncNum.endswith('.0'):
        stringIncNum = stringIncNum[:-2]

    PrimaryZMaxList = []
    PrimaryZMinList = []
    SecondaryZMaxList = []
    SecondaryZMinList = []
    DiskZMaxList = []
    DiskZMinList = []
    AngleList = []

    ax1.set_xlim([0, 360])
    ax1.set_ylim([-0.0008, 0.0008])
    workfile = "/Data/ZvThetaI" + stringIncNum + "/ZvThetaCirc.txt"
    theta = np.loadtxt(workfile, usecols=range(0, 1))
    PrimaryZ = np.loadtxt(workfile, usecols=range(1, 2))
    SecondaryZ = np.loadtxt(workfile, usecols=range(2, 3))
    DiskZ = np.loadtxt(workfile, usecols=range(3, 4))

    ax1.plot(theta, DiskZ, label='circular orbit', color = colors[0])

    PrimaryZMaxList.append(max(PrimaryZ))
    PrimaryZMinList.append(min(PrimaryZ))
    SecondaryZMaxList.append(max(SecondaryZ))
    SecondaryZMinList.append(min(SecondaryZ))
    DiskZMaxList.append(max(DiskZ))
    DiskZMinList.append(min(DiskZ))
    AngleList.append(99)

    unit   = 0.5
    y_tick = np.arange(0, 2+unit, unit)

    y_label = [r"$0\pi$", r"$\pi/2$", r"$\pi$", r"$3\pi/2$",   r"$2\pi$"]
    #ax = ax1.subplot()
    #ax.set_xticks(y_tick*np.pi)
   # ax.set_xticklabels(y_label, fontsize=20)

    for num in range  (0,8):
        filenum = num*0.25


        stringfilenum = str(filenum)
        if stringfilenum.endswith('.0'):
            stringfilenum = stringfilenum[:-2]

        workfile = "/Data/ZvThetaI" + stringIncNum + "/ZvThetaView" + stringfilenum + "pi.txt"

        theta = np.loadtxt(workfile, usecols=range(0, 1))
        PrimaryZ = np.loadtxt(workfile, usecols=range(1, 2))
        SecondaryZ = np.loadtxt(workfile, usecols=range(2, 3))
        DiskZ = np.loadtxt(workfile, usecols=range(3, 4))

        if (filenum != 0.75 and filenum != 1 and filenum != 1.75):
            ax1.plot(theta, PrimaryZ, ls='dashed', color=colors[num + 1])
            ax1.plot(theta, SecondaryZ, ls='dotted', color=colors[num + 1])
            ax1.plot(theta, DiskZ, color = colors[num+1])

        PrimaryZMaxList.append(max(PrimaryZ))
        PrimaryZMinList.append(min(PrimaryZ))
        SecondaryZMaxList.append(max(SecondaryZ))
        SecondaryZMinList.append(min(SecondaryZ))
        DiskZMaxList.append(max(DiskZ))
        DiskZMinList.append(min(DiskZ))

        AngleList.append(filenum)


    ax1.set_title("i = " + stringIncNum + "$^\circ$", fontsize=50)
    ax1.set_ylabel('Redshift (z)', fontsize=50)
    ax1.set_xlabel('True anomaly', fontsize=50)

    ax3 = ax1.twinx()
    ax3.set_ylabel('Wavelength shift ($\AA$)', fontsize=50)
    HaWavelength = 6563
    ax3.set_ylim([-0.0007*HaWavelength, 0.001*HaWavelength])

    ax2.legend()
    #plt.savefig("redshift" + stringIncNum + "new.pdf", dpi=199)
    #plt.show()
    #plt.figure(figsize=(20, 20))
    #plt.close()


    #ax2.set_xlim([-5, 5])
    #ax2.set_ylim([0, 1])
    #plt.xticks(np.arange(6559, 6567, 1.0))
    x = [0, 1]

    ax2.plot(x, [0, 0])
    ax2.xaxis.set_visible(False)

    #zMaxList.reverse()
    #zMinList.reverse()
    #AngleList.reverse()

    for angle, PrimaryZMin, PrimaryZMax, SecondaryZMin, SecondaryZMax, DiskZMin, DiskZMax, c in zip(AngleList, PrimaryZMinList, PrimaryZMaxList, SecondaryZMinList, SecondaryZMaxList, DiskZMinList, DiskZMaxList, colors):

        PrimaryBlueshiftWavelength = (PrimaryZMin)
        PrimaryRedshiftWavelength  = (PrimaryZMax)
        SecondaryBlueshiftWavelength = (SecondaryZMin)
        SecondaryRedshiftWavelength = (SecondaryZMax)
        DiskBlueshiftWavelength = (DiskZMin)
        DiskRedshiftWavelength = (DiskZMax)

        #print(angle, DiskZMin, DiskZMax)
        #if angle == 1.75 or angle == 1.0 or angle == 0.75:
        if (angle != 0.75 and angle != 1 and angle != 1.75):
            if angle == 99:
                ax2.plot(x, [PrimaryBlueshiftWavelength, PrimaryBlueshiftWavelength], ls='dashed', color=c)
                ax2.plot(x, [SecondaryBlueshiftWavelength, SecondaryBlueshiftWavelength], ls='dotted', color=c)
                ax2.plot(x, [DiskBlueshiftWavelength, DiskBlueshiftWavelength], color=c, label='circular orbit')
            else:

                ax2.plot(x, [PrimaryBlueshiftWavelength, PrimaryBlueshiftWavelength], ls='dashed', color=c)
                ax2.plot(x, [SecondaryBlueshiftWavelength, SecondaryBlueshiftWavelength], ls='dotted', color=c)
                if angle == 0:
                    ax2.plot(x, [DiskBlueshiftWavelength, DiskBlueshiftWavelength], color=c, label='0$\pi$=$\pi$')
                elif angle == 0.25:
                    ax2.plot(x, [DiskBlueshiftWavelength, DiskBlueshiftWavelength], color=c, label='0.25$\pi$=0.75$\pi$')
                elif angle == 1.25:
                    ax2.plot(x, [DiskBlueshiftWavelength, DiskBlueshiftWavelength], color=c, label='1.25$\pi$=1.75$\pi$')
                else:
                    ax2.plot(x, [DiskBlueshiftWavelength, DiskBlueshiftWavelength], color=c, label =str(angle)+'$\pi$')

            ax2.plot(x, [PrimaryRedshiftWavelength, PrimaryRedshiftWavelength], ls='dashed', color=c)
            ax2.plot(x, [SecondaryRedshiftWavelength, SecondaryRedshiftWavelength], ls='dotted', color=c)
            ax2.plot(x, [DiskRedshiftWavelength, DiskRedshiftWavelength], color=c)

    ax2.set_title("i = " + stringIncNum + "$^\circ$")
    ax2.set_xlabel('Wavelength (' + '$\AA$' + ')')
    ax2.legend()
    #plt.savefig("peaks" + stringIncNum + "new.pdf")
    plt.show()
    plt.close()


