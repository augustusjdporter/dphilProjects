import matplotlib
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
import numpy as np
from functions import cartesianPosition3D, getTrueAnomaly
import astropy.units as u
import random


def overlap_area(r1, r2, d):
    # r are radii
    # d is distance between centers
    #     http://mathworld.wolfram.com/Circle-CircleIntersection.html

    min_r = min(r1, r2)
    max_r = max(r1, r2)
    answer = np.zeros_like(d.value) * u.AU * u.AU

    # overlap when one not enveloped
    valid_indices = np.logical_and(d < r1 + r2, d > max_r - min_r)

    first_term = (r1**2) * np.arccos((d[valid_indices]**2 + r1**2 - r2**2)/(2*d[valid_indices]*r1)).value
    second_term = (r2 ** 2) * np.arccos((d[valid_indices] ** 2 + r2 ** 2 - r1 ** 2) / (2 * d[valid_indices] * r2)).value
    third_term = -0.5*np.sqrt((-d[valid_indices] + r1 + r2)*(d[valid_indices] + r1 - r2)*(d[valid_indices] - r1 + r2)*(d[valid_indices] + r1 + r2))

    answer[valid_indices] = first_term + second_term + third_term

    # when enveloped
    valid_indices = d <= max_r - min_r
    answer[valid_indices] = np.pi * min_r**2

    return answer


def get_positions_of_components(phases, a1, a2, eccentricity, inclination, arg_periapsis):
    x_s_p = np.zeros_like(phases) * u.AU
    y_s_p = np.zeros_like(phases) * u.AU
    z_s_p = np.zeros_like(phases) * u.AU
    x_s_s = np.zeros_like(phases) * u.AU
    y_s_s = np.zeros_like(phases) * u.AU
    z_s_s = np.zeros_like(phases) * u.AU

    for i, phase in enumerate(phases):
        true_anomaly = getTrueAnomaly(phase, eccentricity)

        primary_position = cartesianPosition3D(a1, eccentricity, true_anomaly, inclination, arg_periapsis, 0)
        secondary_position = cartesianPosition3D(a2, eccentricity, true_anomaly, inclination, arg_periapsis, 0)

        x_s_p[i] = primary_position[0]
        y_s_p[i] = primary_position[1]
        z_s_p[i] = primary_position[2]

        x_s_s[i] = secondary_position[0]
        y_s_s[i] = secondary_position[1]
        z_s_s[i] = secondary_position[2]

    return x_s_p, y_s_p, z_s_p, x_s_s, y_s_s, z_s_s

def points_in_circles(x, y, xs, ys, rs):
    points = np.zeros_like(x.value.astype(int))
    for X, Y, R in zip(xs, ys, rs):
        d = np.sqrt((x-X)**2 + (y-Y)**2)
        indices = d < R
        points[indices] += 1

    # if point in all 3, its count will be 3
    indices = points == 3
    n_points = len(points[indices])
    return n_points


if __name__ == "__main__":
    print("start")

    # parameters of the "outer" binary
    eccentricity = 0.28
    arg_periapsis = 272 * np.pi / 180
    inclination = 90 * (np.pi / 180)
    # Mass at focus and mass ratio used to calculate semi-major axes of the components in the plots
    semiMajorAxis = 0.65 * u.AU
    massAtFocus = 4*38 * u.M_sun
    massRatio = 2.2

    # //M = m1 + m2
    # //q = m1/m2
    # //=m1/M-m1
    # //qM - qm1 = m1
    # //qM = (1+q)m1
    # //m1 = qM/(1+q)
    massPrimary = (massRatio * massAtFocus / (1 + massRatio))
    massSecondary = (massAtFocus - massPrimary)

    a1 = (massSecondary * semiMajorAxis / (massPrimary + massSecondary))
    a2 = (-massPrimary * semiMajorAxis / (massPrimary + massSecondary))

    period = 31.033*2
    timeline = np.linspace(0, period, 1000)

    phases = (timeline % period) / period

    x_s_b, y_s_b, z_s_b, x_s_s, y_s_s, z_s_s = get_positions_of_components(phases, a1, a2, eccentricity, inclination, arg_periapsis)


    # parameters of the closer binary
    a2 = 0.0754 * u.AU
    a1 = -0.00334 * u.AU
    a2 = semiMajorAxis / 4
    a1 = -a2
    eccentricity = 0.
    inclination = 88 * np.pi / 180
    arg_periapsis = 0

    period = 1.583 * 2
    phases = (timeline % period) / period
    x_s_b1, y_s_b1, z_s_b1, x_s_b2, y_s_b2, z_s_b2 = get_positions_of_components(phases, a1, a2, eccentricity, inclination,
                                                                           arg_periapsis)

    x_s_b1 = x_s_b1 + x_s_b
    y_s_b1 = y_s_b1 + y_s_b
    z_s_b1 = z_s_b1 + z_s_b

    x_s_b2 = x_s_b2 + x_s_b
    y_s_b2 = y_s_b2 + y_s_b
    z_s_b2 = z_s_b2 + z_s_b

    # radii of the stars
    r_1 = semiMajorAxis / 4#36*u.R_sun
    r_b1 = (np.abs(a1) + np.abs(a2))/2.2 #1.0001 * u.R_sun
    r_b2= (np.abs(a1) + np.abs(a2))/2.2 #1 *u.R_sun

    # surface brightnesses relative to each other
    sb_1  = .6
    sb_b1 = 1
    sb_b2 = 1


    # area_1_b1 = overlap_area(r_1, r_b1, np.sqrt((x_s_s - x_s_b1)**2 + (y_s_s - y_s_b1)**2))
    # area_b1_b2 = overlap_area(r_b1, r_b2, np.sqrt((x_s_b1 - x_s_b2)**2 + (y_s_b1 - y_s_b2)**2))
    # area_1_b2 = overlap_area(r_1, r_b2, np.sqrt((x_s_s - x_s_b2)**2 + (y_s_s - y_s_b2)**2))


    samples = 1000000
    areas = []
    Rs = np.array([r_1.to(u.AU).value, r_b1.to(u.AU).value, r_b2.to(u.AU).value]) * u.AU
    Sbs = np.array([sb_1, sb_b1, sb_b2])

    for i in range(len(phases)):
        zs = np.array([z_s_s[i].to(u.AU).value, z_s_b1[i].to(u.AU).value, z_s_b2[i].to(u.AU).value])*u.AU
        xs = np.array([x_s_s[i].to(u.AU).value, x_s_b1[i].to(u.AU).value, x_s_b2[i].to(u.AU).value])*u.AU
        ys = np.array([y_s_s[i].to(u.AU).value, y_s_b1[i].to(u.AU).value, y_s_b2[i].to(u.AU).value])*u.AU

        indices = np.argsort(zs)
        zs = zs[indices]
        xs = xs[indices]
        ys = ys[indices]
        rs = Rs[indices]
        sbs = Sbs[indices]


        area_closest_to_middle = overlap_area(rs[0], rs[1], np.sqrt((xs[0] - xs[1])**2 + (ys[0] - ys[1])**2))

        area_closest_to_furthest = overlap_area(rs[0], rs[2], np.sqrt((xs[0] - xs[2])**2 + (ys[0] - ys[2])**2))

        area_middle_to_furthest = overlap_area(rs[1], rs[2], np.sqrt((xs[1] - xs[2]) ** 2 + (ys[1] - ys[2]) ** 2))

        count = 0
        area_total = 0

        if area_closest_to_middle > 0 and area_closest_to_furthest > 0 and area_middle_to_furthest > 0:
            # make box around the littlest one!
            index = np.argmin(rs)
            min_r = rs[index]
            min_x = xs[index] - min_r
            max_x = xs[index] + min_r

            min_y = ys[index] - min_r
            max_y = ys[index] + min_r

            area_total = (max_x - min_x) * (max_y - min_y)

            x = np.random.uniform(min_x.value, max_x.value, samples) * u.AU
            y = np.random.uniform(min_y.value, max_y.value, samples) * u.AU

            count = points_in_circles(x, y, xs, ys, rs)

            print("{}\t{}".format(i, count))
        area = area_closest_to_middle*sbs[1] + (area_closest_to_furthest + area_middle_to_furthest - area_total * count / samples)*sbs[2]
        # if area != 0 * area:
        #     print(sbs)
        areas.append(area.value)

    plt.plot(timeline, areas)
    plt.plot(timeline + max(timeline) + (timeline[1]-timeline[0]), areas)
    plt.gca().invert_yaxis()
    plt.show()


