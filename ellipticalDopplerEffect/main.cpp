#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include <math.h>
#include <vector>
#include <sstream>
#include <fstream>
#include <algorithm>

#ifdef _WIN32
#include <Windows.h>

int mkdir(const char* directory, const int code)
{
	return CreateDirectory(directory, NULL);
};

const std::string path("C:/Data");

#elif __linux__
const std::string path("/home/augustus/Data");
#else
const std::string path("/Data");
#include <sys/stat.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <utility>
#include <string.h>
#include <thread>

#include <sys/types.h>
#include <sys/stat.h>

#ifdef __linux__
#include <unistd.h>
#endif


using namespace std;
const double G(6.67408*pow(10, -11));
const double AU(1.4960*pow(10, 11));
const double solarMass(1.989*pow(10, 30));


const vector<double> getVelocityFromOrbitInPolar(const double semiMajorAxis, const double ellipticity, const double massAtFocus, const double trueAnomaly)
{
    double orbitalPeriod(0);

    orbitalPeriod = sqrt(4*pow(M_PI, 2)*pow(semiMajorAxis, 3)/(G*massAtFocus));

    vector<double> velocity;

    velocity.push_back(2*M_PI*semiMajorAxis*ellipticity*sin(trueAnomaly)/(orbitalPeriod*sqrt(1-pow(ellipticity, 2))));//r comp

    velocity.push_back(2*M_PI*semiMajorAxis*(1+ellipticity*cos(trueAnomaly))/(orbitalPeriod*sqrt(1-pow(ellipticity, 2))));//phi comp

    return velocity;
};

void getBinaryVelocityFromOrbitInPolar(const double semiMajorAxis, const double ellipticity, const double massPrimary, const double massSecondary, const double trueAnomaly, vector<double>& primaryVelocity, vector<double>& secondaryVelocity)
{
    double orbitalPeriod(0);

    orbitalPeriod = sqrt(4*pow(M_PI, 2)*pow(semiMajorAxis, 3)/(G*(massPrimary + massSecondary)));

    cout << "orbitalPeriod: " << orbitalPeriod/24/3600 << endl;
    //m1a1 = m2a2
    //a = a1 + a2
    //a1 = m2*a/M
    double a1(massSecondary*semiMajorAxis/(massPrimary + massSecondary));
    double a2(-massPrimary*semiMajorAxis/(massPrimary + massSecondary));//minus, so star starts in minus x axis, and velocity takes into account
    primaryVelocity.push_back(2*M_PI*a1*ellipticity*sin(trueAnomaly)/(orbitalPeriod*sqrt(1-pow(ellipticity, 2))));      //v_r
    primaryVelocity.push_back(2*M_PI*a1*(1+ellipticity*cos(trueAnomaly))/(orbitalPeriod*sqrt(1-pow(ellipticity, 2))));  //v_phi

    secondaryVelocity.push_back(2*M_PI*a2*ellipticity*sin(trueAnomaly)/(orbitalPeriod*sqrt(1-pow(ellipticity, 2))));
    secondaryVelocity.push_back(2*M_PI*a2*(1+ellipticity*cos(trueAnomaly))/(orbitalPeriod*sqrt(1-pow(ellipticity, 2))));
};

const vector<double> getCartesianVelocityFromPolar(const vector<double> polarVelocity, const double theta)
{
    vector<double> velocity;
    //cout << "phi: " << theta << endl;
    velocity.push_back(polarVelocity.at(0)*cos(theta) - polarVelocity.at(1)*sin(theta));
    velocity.push_back(polarVelocity.at(0)*sin(theta) + polarVelocity.at(1)*cos(theta));

    return velocity;
};

const double magnitudeOfVectorInViewingDirection(const vector<double> cartVelocity, const double viewingAngle, const double inclination)
{
    //Looking in x direction
	if (cartVelocity.size() == 2)
		return (cartVelocity.at(0)*cos(viewingAngle) + cartVelocity.at(1)*sin(viewingAngle))*sin(inclination);
	else
		return cartVelocity.at(2);

	//Looking in z, x plane
	//return cartVelocity.at(2)*cos(viewingAngle) + cartVelocity.at(0)*sin(viewingAngle);
};

const vector <double> cartesianPosition3D(const double semiMajorAxis, const double ellipticity, const double trueAnomaly, const double inclination, const double argPeriapsis, const double longAscendingNode)
{
	double r = semiMajorAxis*(1 - pow(ellipticity, 2)) / (1 + ellipticity*cos(trueAnomaly));

	double xPos = r*(cos(longAscendingNode)*cos(trueAnomaly + argPeriapsis) - sin(longAscendingNode)*sin(trueAnomaly + argPeriapsis)*cos(inclination));

	double yPos = r*(sin(longAscendingNode)*cos(trueAnomaly + argPeriapsis) + cos(longAscendingNode)*sin(trueAnomaly + argPeriapsis)*cos(inclination));

	double zPos = r*(sin(trueAnomaly + argPeriapsis)*sin(inclination));

	vector <double> position;

	position.push_back(xPos);
	position.push_back(yPos);
	position.push_back(zPos);

	return position;
};

const vector <double> cartesianVelocity3D(const double semiMajorAxis, const double ellipticity, const double massAtFocus, const double trueAnomaly, const double inclination, const double argPeriapsis, const double longAscendingNode)
{
	double orbitalPeriod(0);

	orbitalPeriod = sqrt(4 * pow(M_PI, 2)*pow(semiMajorAxis, 3) / (G*massAtFocus));

	double velocityMagnitude(2*M_PI*semiMajorAxis/(orbitalPeriod*sqrt(1-pow(ellipticity, 2))));
	vector<double> velocity;

	double vx = ellipticity*(sin(longAscendingNode)*cos(argPeriapsis)*cos(inclination) + cos(longAscendingNode)*sin(argPeriapsis)) + sin(longAscendingNode)*cos(inclination)*cos(trueAnomaly + argPeriapsis) + cos(longAscendingNode)*sin(trueAnomaly + argPeriapsis);
	velocity.push_back(-velocityMagnitude*vx);

	double vy = ellipticity*(cos(longAscendingNode)*cos(argPeriapsis)*cos(inclination) - sin(longAscendingNode)*sin(argPeriapsis)) + cos(longAscendingNode)*cos(inclination)*cos(trueAnomaly + argPeriapsis) - sin(longAscendingNode)*sin(trueAnomaly + argPeriapsis);
	velocity.push_back(velocityMagnitude*vy);

	double vz = sin(inclination)*(cos(trueAnomaly + argPeriapsis) + ellipticity*cos(argPeriapsis));
	velocity.push_back(velocityMagnitude*vz);
	return velocity;
};

int main()
{
    std::cout << "Elliptical Doppler effect." << std::endl;
    std::cout << "To calculate the spread of doppler shifting of a body in elliptical orbit as a function of viewing angle." << std::endl;

	int result = mkdir(path.c_str(), 0700);
    if (result == -1)
    {
        cout << "error making directory" << endl;
    }

    //Calculate real velocity of the orbiting stuff around the entire ellipse
    //GG Car's CO ring properties
    double eccentricity(0.28);
    const double semiMajorAxis(2.97*AU);
    const double massAtFocus(38*solarMass);
    const double massRatio(2.2);

    //M = m1 + m2
    //q = m1/m2
    //=m1/M-m1
    //qM - qm1 = m1
    //qM = (1+q)m1
    //m1 = qM/(1+q)
    const double massPrimary(massRatio*massAtFocus/(1+massRatio));
    const double massSecondary(massAtFocus - massPrimary);

    //changing inclination
	/*for (double inclCount = 0; inclCount <= 0.5; inclCount = inclCount + 0.125)
	{
        //double inclination = 63/180;
		//double inclination(M_PI*63/180/**inclCount);
        double inclination(M_PI*inclCount);

		stringstream inclinationSS;
		inclinationSS << inclCount;
		string subfolder(path + "/ZvThetaI" + inclinationSS.str());

		mkdir(subfolder.c_str(), 0700);

		//Get redshift for a circular orbit, but same other params.
		{
            string filename(subfolder + "/ZvThetaCirc.txt");
			cout << filename << endl;
			ofstream file;
			file.open(filename.c_str());

			for (int i = 0; i < 360; i++)
			{
				double theta(i * M_PI / 180);

				vector<double> velocityDisk2D(getVelocityFromOrbitInPolar(semiMajorAxis, 0, massAtFocus, theta));
				vector<double> velocityDiskCart2D(getCartesianVelocityFromPolar(velocityDisk2D, theta));

                vector<double> primaryPolarVelocity;
                vector<double> secondaryPolarVelocity;

                getBinaryVelocityFromOrbitInPolar(0.6*AU, 0, massPrimary, massSecondary, theta, primaryPolarVelocity, secondaryPolarVelocity);
                vector<double> velocityPrimaryCart2D(getCartesianVelocityFromPolar(primaryPolarVelocity, theta));
                vector<double> velocitySecondaryCart2D(getCartesianVelocityFromPolar(secondaryPolarVelocity, theta));

                double redshiftDisk(magnitudeOfVectorInViewingDirection(velocityDiskCart2D, 0, inclination) / (3 * pow(10, 8)));
                double redshiftPrimary(magnitudeOfVectorInViewingDirection(velocityPrimaryCart2D, 0, inclination) / (3 * pow(10, 8)));
                double redshiftSecondary(magnitudeOfVectorInViewingDirection(velocitySecondaryCart2D, 0, inclination) / (3 * pow(10, 8)));

                file << i << "\t" << redshiftPrimary << "\t" << redshiftSecondary << "\t" << redshiftDisk << endl;
			};

			file.close();
		}


		for (double j = 0; j < 2; j = j + 0.25)
		{
			double viewingAngle(j*M_PI);

			string filename(subfolder + "/ZvThetaView");
			stringstream number;
			number << j;
			filename = filename + number.str() + "pi.txt";

            vector <double> primaryZs, secondaryZs, diskZs;
			for (int i = 0; i < 360; i++)
			{
				double theta(i * M_PI / 180);

				vector<double> velocityDisk2D(getVelocityFromOrbitInPolar(semiMajorAxis, eccentricity, massAtFocus, theta));
				vector<double> velocityDiskCart2D(getCartesianVelocityFromPolar(velocityDisk2D, theta));

                vector<double> primaryPolarVelocity;
                vector<double> secondaryPolarVelocity;
                getBinaryVelocityFromOrbitInPolar(0.6*AU, eccentricity, massPrimary, massSecondary, theta, primaryPolarVelocity, secondaryPolarVelocity);
                vector<double> velocityPrimaryCart2D(getCartesianVelocityFromPolar(primaryPolarVelocity, theta));
                vector<double> velocitySecondaryCart2D(getCartesianVelocityFromPolar(secondaryPolarVelocity, theta));

                double redshiftDisk(magnitudeOfVectorInViewingDirection(velocityDiskCart2D, viewingAngle, inclination) / (3 * pow(10, 8)));
                double redshiftPrimary(magnitudeOfVectorInViewingDirection(velocityPrimaryCart2D, viewingAngle, inclination) / (3 * pow(10, 8)));
                double redshiftSecondary(magnitudeOfVectorInViewingDirection(velocitySecondaryCart2D, viewingAngle, inclination) / (3 * pow(10, 8)));

				//file << i << "\t" << redshiftPrimary << "\t" << redshiftSecondary << "\t" << redshiftDisk << endl;

                diskZs.push_back(redshiftDisk);
                primaryZs.push_back(redshiftPrimary);
                secondaryZs.push_back((redshiftSecondary));

			};

            for (int i = 0; i < diskZs.size() -1; i++)
            {
                if (diskZs.at(i) == 0 || (diskZs.at(i) < 0 && diskZs.at(i + 1) > 0) /*|| (diskZs.at(i) > 0 && diskZs.at(i + 1) < 0))
                {
                    //put first "i" of vector at the end
                    auto it = diskZs.begin();
                    std::rotate(it, it + i, diskZs.end());
                }

                if (primaryZs.at(i) == 0 || (primaryZs.at(i) < 0 && primaryZs.at(i + 1) > 0) /*|| (primaryZs.at(i) > 0 && primaryZs.at(i + 1) < 0))
                {
                    //put first "i" of vector at the end
                    auto it = primaryZs.begin();
                    std::rotate(it, it + i, primaryZs.end());
                }

                if (secondaryZs.at(i) == 0 || (secondaryZs.at(i) > 0 && secondaryZs.at(i + 1) < 0) /*|| (secondaryZs.at(i) > 0 && secondaryZs.at(i + 1) < 0))
                {
                    //put first "i" of vector at the end
                    auto it = secondaryZs.begin();
                    std::rotate(it, it + i, secondaryZs.end());
                }
            }
            ofstream file;
            file.open(filename.c_str());
            for (int i = 0; i < diskZs.size(); i++)
            {
                file << i << "\t" << primaryZs.at(i) << "\t" << secondaryZs.at(i) << "\t" << diskZs.at(i) << endl;
            }
			file.close();
		};

	};*/

    //changing eccentricity
    for (double eccCount = 0; eccCount < 0.31; eccCount = eccCount + 0.01)
    {
        //double inclination = 63/180;
        //double inclination(M_PI*63/180/**inclCount*/);
        double inclination(M_PI*0.5);

        eccentricity = eccCount;

        stringstream eccSS;
        eccSS << eccCount;
        string subfolder(path + "/ZvThetaEcc" + eccSS.str());

        mkdir(subfolder.c_str(), 0700);


        for (double j = 0; j < 2; j = j + 0.25)
        {
            double viewingAngle(j*M_PI);

            string filename(subfolder + "/ZvThetaView");
            stringstream number;
            number << j;
            filename = filename + number.str() + "pi.txt";

            vector <double> primaryZs, secondaryZs, diskZs;
            for (int i = 0; i < 360; i++)
            {
                double theta(i * M_PI / 180);

                vector<double> velocityDisk2D(getVelocityFromOrbitInPolar(semiMajorAxis, eccentricity, massAtFocus, theta));
                vector<double> velocityDiskCart2D(getCartesianVelocityFromPolar(velocityDisk2D, theta));

                vector<double> primaryPolarVelocity;
                vector<double> secondaryPolarVelocity;
                getBinaryVelocityFromOrbitInPolar(0.6*AU, eccentricity, massPrimary, massSecondary, theta, primaryPolarVelocity, secondaryPolarVelocity);
                vector<double> velocityPrimaryCart2D(getCartesianVelocityFromPolar(primaryPolarVelocity, theta));
                vector<double> velocitySecondaryCart2D(getCartesianVelocityFromPolar(secondaryPolarVelocity, theta));

                double redshiftDisk(magnitudeOfVectorInViewingDirection(velocityDiskCart2D, viewingAngle, inclination) / (3 * pow(10, 8)));
                double redshiftPrimary(magnitudeOfVectorInViewingDirection(velocityPrimaryCart2D, viewingAngle, inclination) / (3 * pow(10, 8)));
                double redshiftSecondary(magnitudeOfVectorInViewingDirection(velocitySecondaryCart2D, viewingAngle, inclination) / (3 * pow(10, 8)));

                //file << i << "\t" << redshiftPrimary << "\t" << redshiftSecondary << "\t" << redshiftDisk << endl;

                diskZs.push_back(redshiftDisk);
                primaryZs.push_back(redshiftPrimary);
                secondaryZs.push_back((redshiftSecondary));

            };

            for (int i = 0; i < diskZs.size() -1; i++)
            {
                if (diskZs.at(i) == 0 || (diskZs.at(i) < 0 && diskZs.at(i + 1) > 0) /*|| (diskZs.at(i) > 0 && diskZs.at(i + 1) < 0*/)
                {
                    //put first "i" of vector at the end
                    auto it = diskZs.begin();
                    std::rotate(it, it + i, diskZs.end());
                }

                if (primaryZs.at(i) == 0 || (primaryZs.at(i) < 0 && primaryZs.at(i + 1) > 0) /*|| (primaryZs.at(i) > 0 && primaryZs.at(i + 1) < 0)*/)
                {
                    //put first "i" of vector at the end
                    auto it = primaryZs.begin();
                    std::rotate(it, it + i, primaryZs.end());
                }

                if (secondaryZs.at(i) == 0 || (secondaryZs.at(i) > 0 && secondaryZs.at(i + 1) < 0) /*|| (secondaryZs.at(i) > 0 && secondaryZs.at(i + 1) < 0)*/)
                {
                    //put first "i" of vector at the end
                    auto it = secondaryZs.begin();
                    std::rotate(it, it + i, secondaryZs.end());
                }
            }
            ofstream file;
            file.open(filename.c_str());
            for (int i = 0; i < diskZs.size(); i++)
            {
                file << i << "\t" << primaryZs.at(i) << "\t" << secondaryZs.at(i) << "\t" << diskZs.at(i) << endl;
            }
			file.close();
		};

	};


    return 0;
}