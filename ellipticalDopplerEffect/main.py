from elliptical_doppler_effect_functions import *
import matplotlib
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt

if __name__ == "__main__":
    # //Calculate real velocity and position of orbiting binaries
    eccentricity = 0.28
    arg_periapsis = 272 * np.pi / 180

    # eccentricity = 0.54683
    # arg_periapsis = 320.11 * np.pi / 180
    inclination = 65 * (M_PI / 180)

    # Mass at focus and mass ratio used to calculate semi-major axes of the components in the plots
    semiMajorAxis = 0.65 * AU
    massAtFocus = 38 * solarMass
    massRatio = 1

    # velocity amplitudes, used in the RV curve
    amplitude_pr = 65.8
    amplitude_sec = 143.3

    systemic_vel = -162.2
    systemic_vel = 0

    # //M = m1 + m2
    # //q = m1/m2
    # //=m1/M-m1
    # //qM - qm1 = m1
    # //qM = (1+q)m1
    # //m1 = qM/(1+q)
    massPrimary = (massRatio*massAtFocus/(1+massRatio))
    massSecondary = (massAtFocus - massPrimary)

    a1 = (massSecondary * semiMajorAxis / (massPrimary + massSecondary))/AU
    a2 = (-massPrimary * semiMajorAxis / (massPrimary + massSecondary))/AU

    x_s_p = []
    y_s_p = []
    z_s_p = []

    x_s_s = []
    y_s_s = []
    z_s_s = []
    phases = np.arange(0, 1, 0.00001)
    for phase in phases:
        true_anomaly = getTrueAnomaly(phase, eccentricity)

        primary_position = cartesianPosition3D(a1, eccentricity, true_anomaly, inclination, arg_periapsis, 0)
        secondary_position = cartesianPosition3D(a2, eccentricity, true_anomaly, inclination, arg_periapsis, 0)

        x_s_p.append(primary_position[0])
        y_s_p.append(primary_position[1])
        z_s_p.append(primary_position[2])

        x_s_s.append(secondary_position[0])
        y_s_s.append(secondary_position[1])
        z_s_s.append(secondary_position[2])

    phases = np.array(phases)
    x_s_p = np.array(x_s_p)
    y_s_p = np.array(y_s_p)
    z_s_p = np.array(z_s_p)

    x_s_s = np.array(x_s_s)
    y_s_s = np.array(y_s_s)
    z_s_s = np.array(z_s_s)

    plt.plot(phases, velocityCurve(phases, amplitude_pr, arg_periapsis, eccentricity, systemic_vel))
    plt.plot(phases, velocityCurve(phases, -amplitude_sec, arg_periapsis, eccentricity, systemic_vel))
    plt.xlabel("Phase")
    plt.ylabel("Velocity (km s$^{-1}$)")
    plt.title("Radial velocity curve")
    plt.show()

    ds = np.sqrt((x_s_p - x_s_s)**2 + (y_s_p - y_s_s)**2)
    plt.plot(phases, ds)
    index = np.argmin(ds)
    plt.axvline(x=phases[index], c="orange")

    idx_min, idx_max = turning_points(ds)
    plt.title("Distance between stars in x-y plane (min at p {}. zp{:.3f}, zs{:.3f})".format(phases[idx_min], z_s_p[index], z_s_s[index]))

    plt.show()
    phases_display = np.arange(0, 1, 0.01)
    x_last = 0
    print(np.min(y_s_p), np.max(y_s_p))
    for phase in phases_display:
        plt.gca().set_aspect('equal', adjustable='box')
        plt.plot(x_s_p, y_s_p)
        plt.plot(x_s_s, y_s_s)
        index = np.argmin(np.abs(phases - phase))
        x = x_s_p[index]

        if (x_last < 0 and x > 0) or (x_last > 0 and x < 0):
            print(phase)
        x_last = x
        plt.scatter(x_s_p[index], y_s_p[index])
        plt.scatter(x_s_s[index], y_s_s[index])

        plt.scatter(0,0)
        plt.xlabel("x (AU)")
        plt.ylabel("y (AU)")
        plt.title("x - y at phase {:.2f} (viewing along z-axis)".format(phase))
        plt.show()


    phases_display = np.arange(0, 1, 0.1)
    for phase in phases_display:
        plt.gca().set_aspect('equal', adjustable='box')
        plt.plot(x_s_p, z_s_p)
        plt.plot(x_s_s, z_s_s)

        index = np.argmin(np.abs(phases - phase))
        plt.scatter(x_s_p[index], z_s_p[index])
        plt.scatter(x_s_s[index], z_s_s[index])

        plt.scatter(0, 0)
        plt.xlabel("x (AU)")
        plt.ylabel("z (AU)")
        plt.title("x - z at phase {:.2f} (viewing along y-axis)".format(phase))
        plt.show()

    dist = np.sqrt((x_s_p - x_s_s)**2 + (y_s_p - y_s_s)**2 + (z_s_p - z_s_s)**2)
    plt.ylabel("dist")
    plt.plot(phases, dist)
    plt.xlabel("Phase")
    plt.ylabel("Real distance between components (AU)")
    plt.show()