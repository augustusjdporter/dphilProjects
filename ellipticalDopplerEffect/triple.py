from functions import *
import matplotlib
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt

if __name__ == "__main__":
    # //Calculate real velocity and position of orbiting binaries
    eccentricity = 0.28
    arg_periapsis = 272 * np.pi / 180

    # velocity amplitudes, used in the RV curve
    amplitude_pr = 65.8
    amplitude_sec = 143.3
    p_1 = 31.033
    systemic_vel = -162.2

    eccentricity_2 = 0
    arg_periapsis_2 = 0
    amplitude_pr_2 = 30
    p_2 = 1.583

    times = np.linspace(0, 100, 4000)
    phases_1 = getPhaseFromEphemeris(times, p_1, 0)
    vel_1 = velocityCurve(phases_1, amplitude_pr, arg_periapsis, eccentricity, systemic_vel)
    phases_2 = getPhaseFromEphemeris(times, p_2, 0)
    vel_2 = velocityCurve(phases_2, amplitude_pr_2, arg_periapsis_2, eccentricity_2, systemic_vel)

    plt.plot(phases_1, vel_1 + vel_2)
    plt.show()
    plt.plot(phases_2, vel_1 + vel_2)
    # plt.plot(phases_1, velocityCurve(phases_1, -amplitude_sec, arg_periapsis, eccentricity, systemic_vel))
    plt.xlabel("Phase")
    plt.ylabel("Velocity (km s$^{-1}$)")
    plt.title("Radial velocity curve")
    plt.show()