from functions import *
import matplotlib
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import simps
import os

eccentricity = 0.28
arg_periapsis = 272 * np.pi / 180
inclination = 63 * (M_PI / 180)

# Mass at focus and mass ratio used to calculate semi-major axes of the components in the plots
semiMajorAxis = 0.65 * AU
massAtFocus = 38 * solarMass
massRatio = 2.2

# velocity amplitudes, used in the RV curve
amplitude_pr = 65.8
amplitude_sec = 143.3

systemic_vel = 0

def normalise(y):
    y_ret = y #- min(y)
    return y_ret / 1# max(y)


wind_speeds = np.linspace(50, 400, 10)
directory = "conv"
if not os.path.isdir(directory):
    os.makedirs(directory)

if __name__ == "__main__":
    for wind_speed in wind_speeds:
        t = (semiMajorAxis - 0.1488*AU) / (wind_speed * 1000) / (24 * 3600) #time to get to A from primary surface

        print("t = {} days".format(t))

        phases = np.linspace(0, 1, 500)
        vel_pr = velocityCurve(phases, amplitude_pr, arg_periapsis, eccentricity, systemic_vel)
        vel_se = velocityCurve(phases, -amplitude_sec, arg_periapsis, eccentricity, systemic_vel)
        t_1 = 0*(t / 2) / 31.033
        t_2 = t / 31.033

        convolved_vel = np.zeros_like(phases)
        convolved_vel_se = np.zeros_like(phases)
        vel_pr = np.append(vel_pr, vel_pr)
        vel_se = np.append(vel_se, vel_se)

        plt.plot(np.append(phases, phases + 1), normalise(vel_pr), c="black")
        plt.plot(np.append(phases, phases + 1), normalise(vel_se), c="black", linestyle="--")

        for i, phase in enumerate(phases):
            phase_1 = phase - t_1
            phase_2 = phase - t_2

            new_vel_grid = np.linspace(phase_2, phase_1, 300)
            new_vel = velocityCurve(new_vel_grid, amplitude_pr, arg_periapsis, eccentricity, systemic_vel)
            I = np.mean(new_vel)#simps(new_vel, new_vel_grid)

            convolved_vel[i] = I
            # plt.plot(phases, (vel_pr))
            # plt.plot(new_vel_grid, (new_vel))
            # plt.plot(phases[convolved_vel != 0], convolved_vel[convolved_vel!=0])
            # plt.axvline(phase)
            # plt.axhline(I)
            #
            # plt.savefig("conv/{}.png".format(str(i).zfill(5)))
            # plt.clf()

            new_vel = velocityCurve(new_vel_grid, -amplitude_sec, arg_periapsis, eccentricity, systemic_vel)
            I = np.mean(new_vel) #simps(new_vel, new_vel_grid)

            convolved_vel_se[i] = I

        phases = np.append(phases, phases + 1)

        convolved_vel = np.append(convolved_vel, convolved_vel)

        convolved_vel_se = np.append(convolved_vel_se, convolved_vel_se)


        l = plt.plot(phases, normalise(convolved_vel), label="{}".format(wind_speed))
        plt.plot(phases, normalise(convolved_vel_se), linestyle="--", color = l[0].get_color())

    plt.legend()
    plt.show()