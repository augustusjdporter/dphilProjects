//
// Created by augustus on 15/11/16.
//

#include "star.h"

using namespace std;

star::star() : m_xVel(0), m_yVel(0)
{
    m_name = "invalid";
    m_mass = 0;
    m_integratorChoice = invalid;

    m_xPos = 0;
    m_yPos = 0;

    m_newXPos = 0;
    m_newYPos = 0;

    m_xVel = 0;
    m_yVel = 0;

    m_xAcc = 0;
    m_yAcc = 0;

    m_time = 0;

    m_companion = NULL;
};

star::star(const string name, const double mass, const integrator integratorChoice) : m_xVel(0), m_yVel(0)
{
    m_name = name;
    m_mass = mass;
    m_integratorChoice = integratorChoice;

    m_xPos = 0;
    m_yPos = 0;

    m_newXPos = 0;
    m_newYPos = 0;

    m_xVel = 0;
    m_yVel = 0;

    m_xAcc = 0;
    m_yAcc = 0;

    m_time = 0;

    m_companion = NULL;
};

star::~star()
{
    m_companion = NULL;
    //cout << m_name << " destroyed." << endl;
};

const string star::name()
{
    return m_name;
};

const double star::xPos()
{
    return m_xPos;
};

const double star::yPos()
{
    return m_yPos;
};

void star::setXPos(const double newXPos)
{
    m_xPos = newXPos;
};

void star::setYPos(const double newYPos)
{
    m_yPos = newYPos;
};

const double star::xVel()
{
    return m_xVel;
};

const double star::yVel()
{
    return m_yVel;
};

void star::setXVel(const double newXVel)
{
    m_xVel = newXVel;
};

void star::setYVel(const double newYVel)
{
    m_yVel = newYVel;
};

void star::setIntegrator(const integrator newIntegrator)
{
    m_integratorChoice = newIntegrator;
};

void star::setCompanion(star* companion)
{
    m_companion = companion;
};

void star::stepUpInTime(const double timestep)
{

    if (m_integratorChoice == integrator::fixed)
    {
        m_time = m_time + timestep;
    }
    else if (m_integratorChoice == integrator::simple)
    {
        simpleIntegration(timestep);
        m_time = m_time + timestep;
    }
    else if (m_integratorChoice == integrator::rungeKutta)
    {
        RK4_step(timestep);
    }


};

void star::simpleIntegration(const double timestep)
{
    calculateAcceleration();

    m_newXPos = m_xPos + m_xVel*timestep + 0.5*m_xAcc*timestep*timestep;//s = vt + 1/2at^2
    m_newYPos = m_yPos + m_yVel*timestep + 0.5*m_xAcc*timestep*timestep;

    //cout << "vel1: " << m_xVelocity << " " << m_yVelocity << " " << m_zVelocity << endl;
    m_xVel += m_xAcc*timestep;
    m_yVel += m_yAcc*timestep;
    //cout << "vel2: " << m_xVelocity << " " << m_yVelocity << " " << m_zVelocity << endl;
    return;
}

void star::calculateAcceleration()
{
    m_xAcc = 0;
    m_yAcc = 0;

    double rx = m_xPos - m_companion->xPos();
    double ry = m_yPos - m_companion->yPos();

    double rCubed = pow(rx*rx + ry*ry, 1.5);

    //double magAcc = -G*m_companion->mass()/rCubed; //SI units
    double magAcc = -4*M_PI*M_PI * m_companion->mass() / rCubed; //Solar units

    m_xAcc = magAcc*rx;
    m_yAcc = magAcc*ry;
};

const double star::mass()
{
    return m_mass;
};

void star::setNewPosition()
{
    m_xPos = m_newXPos;
    m_yPos = m_newYPos;
};


void star::RK4_step(          // replaces x(t) by x(t + dt)
//        vector<double>& x,  // solution vector. Contains time, initial position and initial velocity
        double dt          // fixed time step
)   // derivative vector function, gets acceleration
{
    vector<double> x;
    x.push_back(m_time);
    x.push_back(m_xPos);
    x.push_back(m_yPos);
    x.push_back(m_xVel);
    x.push_back(m_yVel);

    int n = x.size();
    vector<double> f(n), k1(n), k2(n), k3(n), k4(n), x_temp(n);
    f = equations(x); //returns 1, then velocity components, then acceleration components
    for (int i = 0; i < n; i++)
    {
        k1[i] = dt * f[i];
        x_temp[i] = x[i] + k1[i] / 2;
    }
    f = equations(x_temp);
    for (int i = 0; i < n; i++)
    {
        k2[i] = dt * f[i];
        x_temp[i] = x[i] + k2[i] / 2;
    }
    f = equations(x_temp);
    for (int i = 0; i < n; i++)
    {
        k3[i] = dt * f[i];
        x_temp[i] = x[i] + k3[i];
    }
    f = equations(x_temp);
    for (int i = 0; i < n; i++)
    {
        k4[i] = dt * f[i];
    }
    for (int i = 0; i < n; i++)
    {
        x[i] += (k1[i] + 2 * k2[i] + 2 * k3[i] + k4[i]) / 6;
    }

    m_time = x.at(0);
    m_newXPos = x.at(1);
    m_newYPos = x.at(2);
    m_xVel = x.at(3);
    m_yVel = x.at(4);
};
//  Derivative vector for Newton's law of gravitation
vector<double> star::equations(vector<double>& trv)
{

    //get new acc and vel
    calculateAcceleration();
    vector<double> flow(5);
    flow[0] = 1;
    flow[1] = trv.at(3);//m_xVel;
    flow[2] = trv.at(4);//m_yVel;
    flow[3] = m_xAcc;
    flow[4] = m_yAcc;
    return flow;
}
