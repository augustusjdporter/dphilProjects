#include <iostream>
#include "functions.h"
#include "star.h"
#include <fstream>
#include <string>
#include <sstream>
#include <functional>
#include <thread>

unsigned concurentThreadsSupported = std::thread::hardware_concurrency();

int main(int argc, char *argv[])
{
    std::cout << "A small program to test orbit integration accuracy of binary stars." << std::endl;
    const time_t ctt = time(0);
    cout << asctime(localtime(&ctt)) << endl;//output time
    if (argc == 1)
    {
        cout << "Error: No command line arguments." << endl << "Enter each integration timestep (in hours) as a command line argument" << endl;
        return 0;
    }

    vector<double> timesteps;
    cout << "Chosen timesteps are:" << endl;
    for (int i = 1; i < argc; i++)
    {
        timesteps.push_back(atof(argv[i])/24/365.25); //timestep in years
        cout << "\t" << timesteps.at(i - 1) << " seconds." << endl;
    };

    //const double eccentricityBinary(0.62);
    //const double semiMajorAxisBinary(1/**AU*/);
    //const double massAtFocus(1/*solarMass*/);
    //const double massRatio(2.2);
    
    //GG Car
    const double eccentricityBinary(0.28);
    const double semiMajorAxisBinary((0.18 + 0.39)/0.884/**AU*/);
    const double massAtFocus((18+8)/0.69/*solarMass*/);
    const double massRatio(18/8);

    //M = m1 + m2
    //q = m1/m2
    //=m1/M-m1
    //qM - qm1 = m1
    //qM = (1+q)m1
    //m1 = qM/(1+q)
    const double massPrimary(massRatio*massAtFocus/(1+massRatio));
    const double massSecondary(massAtFocus - massPrimary);

    double a1(massSecondary*semiMajorAxisBinary/(massPrimary + massSecondary));
    double a2(-massPrimary*semiMajorAxisBinary/(massPrimary + massSecondary));//minus, so star starts in minus x axis, and velocity takes into account

    double a1oAU(a1/AU);
    double a2oAU(a2/AU);

    double inclinationBinary(0);
    double longAscendingNodeBinary(0);
    double argPeriapsisBinary(0);

    //double binaryPeriod(sqrt(4*pow(M_PI, 2)*pow(semiMajorAxisBinary, 3)/(G*massAtFocus))); //SI units
    double binaryPeriod = sqrt(pow(semiMajorAxisBinary, 3) / massAtFocus);

    const vector<double> initialVelPrimary(cartesianVelocity3D(semiMajorAxisBinary, a1, eccentricityBinary, massAtFocus, 0, inclinationBinary, argPeriapsisBinary, longAscendingNodeBinary));
    const vector<double> initialVelSecondary(cartesianVelocity3D(semiMajorAxisBinary, a2, eccentricityBinary, massAtFocus, 0, inclinationBinary, argPeriapsisBinary, longAscendingNodeBinary));

    vector<double> primaryStarPositionInitial(cartesianPosition3D(a1, eccentricityBinary, 0, inclinationBinary, argPeriapsisBinary, longAscendingNodeBinary));
    vector<double> secondaryStarPositionInitial(cartesianPosition3D(a2, eccentricityBinary, 0, inclinationBinary, argPeriapsisBinary, longAscendingNodeBinary));

    std::function<double (double)> run_orbits_with_timestep = [&](const double timestep)
    {
        cout << "Integrating with timestep: " << timestep << endl;

        {
            //create fixed orbit
            star fixedPrimary("fixedPrimary", 0, integrator::fixed);
            star fixedSecondary("fixedSecondary", 0, integrator::fixed);
            fixedPrimary.setCompanion(&fixedSecondary);
            fixedSecondary.setCompanion(&fixedPrimary);

            //create simple integrator
            star simplePrimary("simplePrimary", massPrimary, integrator::simple);
            star simpleSecondary("simpleSecondary", massSecondary, integrator::simple);
            simplePrimary.setCompanion(&simpleSecondary);
            simpleSecondary.setCompanion(&simplePrimary);
            simplePrimary.setXPos(primaryStarPositionInitial.at(0));
            simplePrimary.setYPos(primaryStarPositionInitial.at(1));
            simpleSecondary.setXPos(secondaryStarPositionInitial.at(0));
            simpleSecondary.setYPos(secondaryStarPositionInitial.at(1));
            simplePrimary.setXVel(initialVelPrimary.at(0));
            simplePrimary.setYVel(initialVelPrimary.at(1));
            simpleSecondary.setXVel(initialVelSecondary.at(0));
            simpleSecondary.setYVel(initialVelSecondary.at(1));

            //create runge kutta
            star rkPrimary("rkPrimary", massPrimary, integrator::rungeKutta);
            star rkSecondary("rkSecondary", massSecondary, integrator::rungeKutta);
            rkPrimary.setCompanion(&rkSecondary);
            rkSecondary.setCompanion(&rkPrimary);
            rkPrimary.setXPos(primaryStarPositionInitial.at(0));
            rkPrimary.setYPos(primaryStarPositionInitial.at(1));
            rkSecondary.setXPos(secondaryStarPositionInitial.at(0));
            rkSecondary.setYPos(secondaryStarPositionInitial.at(1));
            rkPrimary.setXVel(initialVelPrimary.at(0));
            rkPrimary.setYVel(initialVelPrimary.at(1));
            rkSecondary.setXVel(initialVelSecondary.at(0));
            rkSecondary.setYVel(initialVelSecondary.at(1));


            stringstream timestepSS;
            timestepSS << timestep;
            ofstream trackingFile;
            trackingFile.open("/Data/orbitIntegrationTesting/naturalUnitsGGCar/starTracks" + timestepSS.str() + ".txt");
            if (!trackingFile.is_open())
            {
                cout << "tracking file not open" << endl;
                //return 0;
            }

            double time(0);

            int numberOfSteps = 1000 * binaryPeriod / timestep; //1000 binary periods
            //cout << "Number of steps: " << numberOfSteps << endl;

            int refreshNumber = 0.02 * binaryPeriod / timestep; //get 50 points per binary period
            int refresh(0);
            int start_s = clock();
            for (int i = 0; i < numberOfSteps; i++)
            {
                double binaryTrueAnomaly = findTrueAnomaly(binaryPeriod, eccentricityBinary, time);

                vector<double> primaryStarPosition(
                        cartesianPosition3D(a1, eccentricityBinary, binaryTrueAnomaly, inclinationBinary,
                                            argPeriapsisBinary, longAscendingNodeBinary));
                vector<double> secondaryStarPosition(
                        cartesianPosition3D(a2, eccentricityBinary, binaryTrueAnomaly, inclinationBinary,
                                            argPeriapsisBinary, longAscendingNodeBinary));
                fixedPrimary.setXPos(primaryStarPosition.at(0));
                fixedPrimary.setYPos(primaryStarPosition.at(1));
                fixedSecondary.setXPos(secondaryStarPosition.at(0));
                fixedSecondary.setYPos(secondaryStarPosition.at(1));

                simplePrimary.stepUpInTime(timestep);
                simpleSecondary.stepUpInTime(timestep);

                simplePrimary.setNewPosition();
                simpleSecondary.setNewPosition();

                rkPrimary.stepUpInTime(timestep);
                rkSecondary.stepUpInTime(timestep);

                rkPrimary.setNewPosition();
                rkSecondary.setNewPosition();


                if (refresh == refreshNumber)
                {
                    trackingFile << 0 << "\t" << fixedPrimary.xPos() / AU << "\t" << fixedPrimary.yPos() / AU << endl;
                    trackingFile << 1 << "\t" << fixedSecondary.xPos() / AU << "\t" << fixedSecondary.yPos() / AU << endl;

                    trackingFile << 2 << "\t" << simplePrimary.xPos() / AU << "\t" << simplePrimary.yPos() / AU << endl;
                    trackingFile << 3 << "\t" << simpleSecondary.xPos() / AU << "\t" << simpleSecondary.yPos() / AU << endl;

                    trackingFile << 4 << "\t" << rkPrimary.xPos() / AU << "\t" << rkPrimary.yPos() / AU << endl;
                    trackingFile << 5 << "\t" << rkSecondary.xPos() / AU << "\t" << rkSecondary.yPos() / AU << endl;

                    refresh = 0;
                    //cout << i << endl;
                }

                time += timestep;
                refresh++;
            }

            trackingFile.close();

            ofstream coordinateFile;
            coordinateFile.open("/Data/orbitIntegrationTesting/naturalUnitsGGCar/starCoordinates" + timestepSS.str() + ".txt");

            if (!coordinateFile.is_open())
            {
                cout << "could not open coordinate file" << endl;
                //return 0;
            }
            coordinateFile << 0 << "\t" << fixedPrimary.xPos() / AU << "\t" << fixedPrimary.yPos() / AU << endl;
            coordinateFile << 1 << "\t" << fixedSecondary.xPos() / AU << "\t" << fixedSecondary.yPos() / AU << endl;

            coordinateFile << 2 << "\t" << simplePrimary.xPos() / AU << "\t" << simplePrimary.yPos() / AU << endl;
            coordinateFile << 3 << "\t" << simpleSecondary.xPos() / AU << "\t" << simpleSecondary.yPos() / AU << endl;

            coordinateFile << 4 << "\t" << rkPrimary.xPos() / AU << "\t" << rkPrimary.yPos() / AU << endl;
            coordinateFile << 5 << "\t" << rkSecondary.xPos() / AU << "\t" << rkSecondary.yPos() / AU << endl;

            coordinateFile.close();

            int stop_s = clock();

            cout << "Finished integrating timestep " << timestep << " in time: " << (stop_s - start_s) / double(CLOCKS_PER_SEC) << " seconds." << endl;
        }
        //stars should be deleted
        if (timesteps.size() > 0)
        {
            cout << "Still have " << timesteps.size() << " timesteps to go." << endl;
            //cout << "Calling " << timesteps.back() << endl;

            double newTimestep = timesteps.back();
            timesteps.pop_back();
            run_orbits_with_timestep(newTimestep);
        };
        //no more timesteps => finish lamda
        return 1;
    };

    //cout << "starting" << endl;

    vector<thread> threads;
    for (int i = 0; i < concurentThreadsSupported; i++)
    {
        if (timesteps.size() <= 0)
	    {
	        cout << "breaking out of initial loop" << endl;
            break;
	    }

        double theTimestep = timesteps.back();
        timesteps.pop_back();
        threads.push_back(thread(run_orbits_with_timestep, theTimestep));
        //run_orbits_with_timestep(theTimestep);
    };

    cout << "here" << endl;
    for (auto& th : threads)
        th.join();

    threads.clear();

    cout << "All done!" << endl;


    //system("python /home/portera/privateWork/orbitIntegrationTesting/plot.py");

    return 0;
}
