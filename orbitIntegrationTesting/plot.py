import numpy as np
import matplotlib.pyplot as plt
import math

from pylab import figure, axes, pie, title, show
import pylab as p

coordWorkfileName = "/Data/orbitIntegrationTesting/naturalUnitsGGCar/starCoordinates5.70386e-07.txt"
trackWorkfileName = "/Data/orbitIntegrationTesting/naturalUnitsGGCar/starTracks5.70386e-07.txt"

names = np.loadtxt(trackWorkfileName, usecols=range(0, 1))
xtracks = np.loadtxt(trackWorkfileName, usecols=range(1, 2))
ytracks = np.loadtxt(trackWorkfileName, usecols=range(2, 3))

namesco = np.loadtxt(coordWorkfileName, usecols=range(0, 1))
xcoords = np.loadtxt(coordWorkfileName, usecols=range(1, 2))
ycoords = np.loadtxt(coordWorkfileName, usecols=range(2, 3))

plot_lim = 5
plt.ylim([-plot_lim, plot_lim])
plt.xlim([-plot_lim, plot_lim])


plt.plot(xtracks[names == 0], ytracks[names == 0], color='black')
plt.plot(xtracks[names == 1], ytracks[names == 1], color='black')

plt.plot(xtracks[names == 2], ytracks[names == 2], color='red')
plt.plot(xtracks[names == 3], ytracks[names == 3], color='red')

plt.plot(xtracks[names == 4], ytracks[names == 4], color='green')
plt.plot(xtracks[names == 5], ytracks[names == 5], color='green')

pointSize = 10
plt.scatter(xcoords[namesco == 0], ycoords[namesco == 0], s = pointSize, color='black')
plt.scatter(xcoords[namesco == 1], ycoords[namesco == 1], s = pointSize, color='black')

plt.scatter(xcoords[namesco == 2], ycoords[namesco == 2], s = pointSize, color='red')
plt.scatter(xcoords[namesco == 3], ycoords[namesco == 3], s = pointSize, color='red')

plt.scatter(xcoords[namesco == 4], ycoords[namesco == 4], s = pointSize, color='green')
plt.scatter(xcoords[namesco == 5], ycoords[namesco == 5], s = pointSize, color='green')
plt.show()
