//
// Created by augustus on 15/11/16.
//

#ifndef ORBITINTEGRATIONTESTING_STAR_H
#define ORBITINTEGRATIONTESTING_STAR_H

enum integrator
{
    invalid,
    fixed,
    simple,
    rungeKutta
};

#include <string>
#include <iostream>
#include <cmath>
#include <vector>

/*const double G(6.67408*pow(10, -11));
const double AU(1.4960*pow(10, 11));
const double secondsInYear(3.154*pow(10, 7));
const double solarMass(1.989*pow(10, 30));*/

const double G(1);
const double AU(1);
const double secondsInYear(1);
const double solarMass(1);

using namespace std;

class star
{
public:
    star();

    star(const string name, const double mass, const integrator integratorChoice);

    ~star();

    const string name();

    const double xPos();

    const double yPos();

    void setXPos(const double newXPos);

    void setYPos(const double newYPos);

    const double xVel();

    const double yVel();

    void setXVel(const double newXVel);

    void setYVel(const double newYVel);

    void setIntegrator(const integrator newIntegrator);

    void setCompanion(star* companion);

    void stepUpInTime(const double timestep);

    const double mass();

    void setNewPosition();
private:
    string m_name;
    double m_xPos;
    double m_yPos;

    double m_xVel;
    double m_yVel;

    double m_xAcc;
    double m_yAcc;

    double m_mass;

    double m_time;

    double m_newXPos;
    double m_newYPos;

    integrator m_integratorChoice;

    star*  m_companion;

    void simpleIntegration(const double timestep);

    void calculateAcceleration();

    vector<double> equations(vector<double>& trv);

    void RK4_step(          // replaces x(t) by x(t + dt)
//            vector<double>& x,  // solution vector. Contains time, initial position and initial velocity
            double dt          // fixed time step
            //vector<double> flow(vector<double>&
            );
};


#endif //ORBITINTEGRATIONTESTING_STAR_H
