//
// Created by augustus on 15/11/16.
//

#ifndef ORBITINTEGRATIONTESTING_FUNCTIONS_H
#define ORBITINTEGRATIONTESTING_FUNCTIONS_H

#include <vector>
#include <cmath>
#include <iostream>
#include "star.h"
using namespace std;

const vector <double> cartesianPosition3D(const double semiMajorAxis, const double ellipticity, const double trueAnomaly, const double inclination, const double argPeriapsis, const double longAscendingNode)
{
    double r = semiMajorAxis*(1 - pow(ellipticity, 2)) / (1 + ellipticity*cos(trueAnomaly));

    double xPos = r*(cos(longAscendingNode)*cos(trueAnomaly + argPeriapsis) - sin(longAscendingNode)*sin(trueAnomaly + argPeriapsis)*cos(inclination));

    double yPos = r*(sin(longAscendingNode)*cos(trueAnomaly + argPeriapsis) + cos(longAscendingNode)*sin(trueAnomaly + argPeriapsis)*cos(inclination));

    double zPos = r*(sin(trueAnomaly + argPeriapsis)*sin(inclination));

    vector <double> position;

    position.push_back(xPos);
    position.push_back(yPos);
    position.push_back(zPos);

    return position;
};

const double eccentricAnomalyNplus1(const double eccentricAnomalyN, const double eccentricity, const double meanAnomaly)
{
    return eccentricAnomalyN - (eccentricAnomalyN - eccentricity*sin(eccentricAnomalyN) - meanAnomaly)/(1 - eccentricity*cos(eccentricAnomalyN));
};

const double findTrueAnomaly(const double period, const double eccentricity, const double time)
{
    //https://en.wikipedia.org/wiki/Kepler%27s_laws_of_planetary_motion#Position_as_a_function_of_time
    double meanMotion(2*M_PI/period);
    double meanAnomaly(meanMotion*time);

    //need to iteratively solve Keplers equation M = E - e*sinE, where E is eccentric anomaly, and M is mean anomaly
    double eccentricAnomaly = eccentricAnomalyNplus1(meanAnomaly, eccentricity, meanAnomaly);
    while(true)
    {
        double tempEccentricAnomaly = eccentricAnomaly;
        eccentricAnomaly = eccentricAnomalyNplus1(eccentricAnomaly, eccentricity, meanAnomaly);

        if (abs(eccentricAnomaly - tempEccentricAnomaly) != 0 && tempEccentricAnomaly == 0)
            tempEccentricAnomaly = 0.0000000001;
        double percentageChange(abs(eccentricAnomaly - tempEccentricAnomaly)*100/tempEccentricAnomaly);

        //cout << eccentricAnomaly << " " << percentageChange << endl;
        //When further iterations dont cause much change, carry on
        if(abs(eccentricAnomaly - tempEccentricAnomaly) == 0 || percentageChange < 0.1)
        {
            break;
        }
    };

    //return the true anomaly
    return 2*atan(sqrt((1 + eccentricity)/(1 - eccentricity))*tan(eccentricAnomaly/2));
};

const vector <double> cartesianVelocity3D(const double semiMajorAxis,const double singleSemiMajorAxis, const double ellipticity, const double massAtFocus, const double trueAnomaly, const double inclination, const double argPeriapsis, const double longAscendingNode)
{
    double orbitalPeriod(0);

    //orbitalPeriod = sqrt(4 * pow(M_PI, 2)*pow(semiMajorAxis, 3) / (G*massAtFocus));
    orbitalPeriod = sqrt(pow(semiMajorAxis, 3) / massAtFocus);

    double velocityMagnitude(2*M_PI*singleSemiMajorAxis/(orbitalPeriod*sqrt(1-pow(ellipticity, 2)))); //AU/yr
    vector<double> velocity;

    double vx = ellipticity*(sin(longAscendingNode)*cos(argPeriapsis)*cos(inclination) + cos(longAscendingNode)*sin(argPeriapsis)) + sin(longAscendingNode)*cos(inclination)*cos(trueAnomaly + argPeriapsis) + cos(longAscendingNode)*sin(trueAnomaly + argPeriapsis);
    velocity.push_back(-velocityMagnitude*vx);

    double vy = ellipticity*(cos(longAscendingNode)*cos(argPeriapsis)*cos(inclination) - sin(longAscendingNode)*sin(argPeriapsis)) + cos(longAscendingNode)*cos(inclination)*cos(trueAnomaly + argPeriapsis) - sin(longAscendingNode)*sin(trueAnomaly + argPeriapsis);
    velocity.push_back(velocityMagnitude*vy);

    double vz = sin(inclination)*(cos(trueAnomaly + argPeriapsis) + ellipticity*cos(argPeriapsis));
    velocity.push_back(velocityMagnitude*vz);
    return velocity;
};

#endif //ORBITINTEGRATIONTESTING_FUNCTIONS_H
