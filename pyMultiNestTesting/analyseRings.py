from numpy import log
import pymultinest
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from spectrumLoader import *
from scipy.ndimage.filters import gaussian_filter


Min = 6510
Max = 6520
filename = "GGCar-2457817.2989004632-Summed-20170304-GJW-WA_3000.0_bin0.5.txt"
withNegative = False
noise = 0.05
n_gausses = [6, 7, 8, 9, 10]
wavelength, intensity = loadSpectrum(filename)
intensity = removeBackground(wavelength, intensity)
intensity = normaliseSpectrum(wavelength, intensity)
intensity = intensity[np.logical_and(wavelength > Min, wavelength < Max)]
wavelength = wavelength[np.logical_and(wavelength > Min, wavelength < Max)]


n_gauss = 1
directory = "model_n_ring{}_noise{}_min{}_max{}".format(n_gauss,noise, Min, Max)



parameters = ["base"]

for i in range(n_gauss):
    parameters.append("keplerVel{}".format(i))
    parameters.append("internalVel{}".format(i))
    parameters.append("resolution{}".format(i))
    parameters.append("maxIntensity{}".format(i))
    parameters.append("center{}".format(i))
n_params = len(parameters)


def line_profile_thin_ring(losVel, center, keplerVelocity, internalVelocity, resolution, maxIntensity, base):
    def intensity_by_projected_velocity(projectedVel, actualVel, center):
        if abs(projectedVel) == actualVel:
            return 0.
        return 1./(2.*np.pi*np.sqrt(1.-(abs(projectedVel - center)/actualVel)**2.))
    intensities = []

    for i in losVel:
        if abs(i - center) < 0.999*keplerVelocity:
            intensities.append(intensity_by_projected_velocity(i, keplerVelocity, center))
        else:
            intensities.append(0.)
    intensities = np.array(intensities)
    result = gaussian_filter(intensities, internalVelocity+resolution)
    result = result/max(result)*maxIntensity + base
    return result


def line_profile_thin_ring_N(x, cube):
    y = np.zeros_like(x)
    base = cube[0]
    for i in range(1, n_params, 5):
        keplerVel = cube[i]
        internalVel = cube[i + 1]
        resolution = cube[i + 2]
        maxIntensity = cube[i + 3]
        center = cube[i + 4]
        y = y + line_profile_thin_ring(x, center, keplerVel, internalVel, resolution, maxIntensity, 0.)
    y = y + base

    return y

datafile = os.path.join(directory, "output")

# plot the distribution of a posteriori possible models
figure, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
ax1.plot(wavelength, intensity, '+ ', color='red', label='data')
a = pymultinest.Analyzer(outputfiles_basename=datafile, n_params = n_params)
x = np.linspace(Min, Max, 1000)

for popt in a.get_equal_weighted_posterior()[::100,:-1]:
    colors = cm.rainbow(np.linspace(0, 1, n_gauss))
    ax1.plot(x, line_profile_thin_ring_N(x, popt), '-', color='blue', alpha=0.3, label='data')
    for i in range(int((len(popt) - 1) / 3)):
        mean = popt[3 * i + 1]
        A = popt[3 * i + 2]
        SD = popt[3 * i + 3]

        # gaussFit = line_profile_thin_ring(x, popt[0], A, mean, SD)
        # ax1.plot(x, gaussFit, color=colors[i], alpha=0.3)
ax1.set_title("{}, {}".format(n_gauss, noise))

a_lnZ = a.get_stats()['global evidence']
a_log_global_evid = a.get_stats()['nested sampling global log-evidence']
get_stats = a.get_stats()
print ()
print ('************************')
print ('MAIN RESULT: Evidence Z ')
print ('************************')
print ('  log Z for model with 1 line = %.1f' % (a_lnZ / log(10)))
print (a_log_global_evid)

ax2.plot(wavelength, intensity, '+ ', color='red', label='data')
popt = a.get_best_fit()
ax2.plot(x, line_profile_thin_ring_N(x, popt["parameters"]))
colors = cm.rainbow(np.linspace(0, 1, n_gauss))
for i in range(int((len(popt["parameters"]) - 1) / 3)):
    mean = popt["parameters"][3 * i + 1]
    A = popt["parameters"][3 * i + 2]
    SD = popt["parameters"][3 * i + 3]

    # gaussFit = line_profile_thin_ring_N(x, popt["parameters"][0], A, mean, SD)
    # ax2.plot(x, gaussFit, color=colors[i], alpha=0.3)
plt.gcf().set_size_inches(7, 14)
plt.show()