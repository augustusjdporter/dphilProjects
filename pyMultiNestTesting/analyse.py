from numpy import log
import pymultinest
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from spectrumLoader import *
Min = 6540
Max = 6580
filename = "GGCar-2457811.2917824076-Summed-20170226-GJW-WA_100.0_bin0.5.txt"
withNegative = False
noise = 0.05
n_gausses = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
wavelength, intensity, error = loadSpectrum(filename)
intensity = removeBackground(wavelength, intensity)
intensity = normaliseSpectrum(wavelength, intensity)
intensity = intensity[np.logical_and(wavelength > Min, wavelength < Max)]
wavelength = wavelength[np.logical_and(wavelength > Min, wavelength < Max)]

for n_gauss in n_gausses:
    plt.close()
    parameters = ["base"]
    for i in range(n_gauss):
        parameters.append("mean{}".format(i))
        parameters.append("amp{}".format(i))
        parameters.append("sigma{}".format(i))
    n_params = len(parameters)



    directory = "model_n_gauss{}_noise{}".format(n_gauss,noise)
    if withNegative:
        directory += "_withNegative"
    if not os.path.exists(directory):
        directory = "model_n_gauss{}_noise{}_min{}_max{}".format(n_gauss, noise, Min, Max)
        if withNegative:
            directory += "_withNegative"
        if not os.path.exists(directory):
            print ("No directory {}".format(directory))
            continue

    def gauss(x, base, A, mu, sigma):
        return base + A*np.exp(-(x-mu)**2/(2.*sigma**2))

    def gaussN(x, cube):
        y = np.zeros_like(x)
        base = cube[0]
        for i in range(n_gauss):
            mean = cube[3*i + 1]
            A = cube[3*i+2]
            SD = cube[3*i+3]

            y = y + gauss(x, base/n_gauss, A, mean, SD)
        return y

    datafile = os.path.join(directory, "output")

    # plot the distribution of a posteriori possible models
    figure, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
    ax1.plot(wavelength, intensity, '+ ', color='red', label='data')
    a = pymultinest.Analyzer(outputfiles_basename=datafile, n_params = n_params)
    x = np.linspace(Min, Max, 1000)

    for popt in a.get_equal_weighted_posterior()[::100,:-1]:
        colors = cm.rainbow(np.linspace(0, 1, n_gauss))
        ax1.plot(x, gaussN(x, popt), '-', color='blue', alpha=0.3, label='data')
        for i in range(int((len(popt) - 1) / 3)):
            mean = popt[3 * i + 1]
            A = popt[3 * i + 2]
            SD = popt[3 * i + 3]

            gaussFit = gauss(x, popt[0], A, mean, SD)
            ax1.plot(x, gaussFit, color=colors[i], alpha=0.3)
    ax1.set_title("{}, {}".format(n_gauss, noise))

    a_lnZ = a.get_stats()['global evidence']
    a_log_global_evid = a.get_stats()['nested sampling global log-evidence']
    get_stats = a.get_stats()
    print ()
    print ('************************')
    print ('MAIN RESULT: Evidence Z ')
    print ('************************')
    print ('  log Z for model with 1 line = %.1f' % (a_lnZ / log(10)))
    print (a_log_global_evid)

    ax2.plot(wavelength, intensity, '+ ', color='red', label='data')
    popt = a.get_best_fit()
    ax2.plot(x, gaussN(x, popt["parameters"]))
    colors = cm.rainbow(np.linspace(0, 1, n_gauss))
    for i in range(int((len(popt["parameters"]) - 1) / 3)):
        mean = popt["parameters"][3 * i + 1]
        A = popt["parameters"][3 * i + 2]
        SD = popt["parameters"][3 * i + 3]

        gaussFit = gauss(x, popt["parameters"][0], A, mean, SD)
        ax2.plot(x, gaussFit, color=colors[i], alpha=0.3)
    plt.gcf().set_size_inches(7, 14)
    # plt.show()
    if not os.path.isdir("modelPlots{}".format(noise)):
        os.mkdir("modelPlots{}".format(noise))
    plt.savefig("modelPlots{}/n_gauss{}.png".format(noise, n_gauss))
