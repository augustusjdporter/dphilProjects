import numpy as np
from scipy.signal import savgol_filter
import os

def loadSpectrum(filePath):
    if os.path.isfile(filePath) == False:
        return [], []
    wavelength = np.loadtxt(filePath, usecols=range(0, 1))
    intensity = np.loadtxt(filePath, usecols=range(1, 2))
    error = np.loadtxt(filePath, usecols=range(2, 3))
    if len(wavelength) == 0 or len(intensity) == 0 or len(wavelength) != len(intensity):
        return ([], [])
    # following lines gets rid of those annoying parts of the spectrum at around 0 intensity
    where_are_NaNs = np.isnan(intensity)
    intensity[where_are_NaNs] = 0
    if len(intensity) > 0:
        error = error[intensity > max(intensity) / 10000]
        wavelength = wavelength[intensity > max(intensity) / 10000]
        intensity = intensity[intensity > max(intensity) / 10000]

    else:
        print("Warning, file {} is empty.".format(filePath))
    return (wavelength, intensity, error)


def savgolFilter(xVals, yVals):
    spectrumNoHA = []
    last = 0
    if len(xVals) == 0 or len(yVals) == 0:
        return (np.array([]), np.array([]))
    for x, y in zip(xVals, yVals):
        # don't count the Halpha line, lest it gets fitted by the savgol
        if x < 6540 or x > 6580:
            spectrumNoHA.append(y)
            last = y
        else:
            spectrumNoHA.append(last)
    num = len(spectrumNoHA)
    if (num % 2 == 0):  # even
        num = num - 1
    savgol = savgol_filter(spectrumNoHA, num, 5)
    return savgol


def removeBackground(xVals, yVals):
    savgol = savgolFilter(xVals, yVals)
    yVals = yVals - savgol
    return yVals


def normalizeByContinuum(xVals, yVals):
    savgol = savgolFilter(xVals, yVals)
    yVals = yVals / savgol - 1.
    return yVals


def normaliseSpectrum(xValues, yValues, method = "H alpha peak"):
    if len(yValues) == 0:
        return np.array([])
    def findClosestArrayIndex(array, value):
        oldDiff = np.inf
        for i, a in zip(range(len(array)), array):
            diff = abs(value - a)
            if diff > oldDiff:
                return i
            else:
                oldDiff = diff
        # failed
        return 0

    wavelength = 6563.
    if method == 'H alpha peak':
        yValues = yValues / max(yValues)
        return yValues

    if method == 'Continuum':
        yValues = normalizeByContinuum(xValues, yValues)
        return yValues

    elif method == 'H alpha left':
        wavelength = 6525.

    elif method == 'H alpha right':
        wavelength = 6620.

    elif method == 'Atmospheric absorption line':
        wavelength = 7585.

    index = findClosestArrayIndex(xValues, wavelength)
    savgol = savgolFilter(xValues, yValues)
    newYValues = yValues / savgol[index]
    return newYValues
