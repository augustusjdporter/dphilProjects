"""
Compare analysed models using the stats.json files (which contain the evidence,
but also marginal summaries)
"""
import json
import sys
import numpy
from math import log, log10
import os
import pymultinest

Min = 6540
Max = 6580
filename = "GGCar-2457811.2917824076-Summed-20170226-GJW-WA_100.0_bin0.5.txt"
withNegative = False
noise = 0.01
n_gausses = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
prefixes = []
for n_gauss in n_gausses:
    directory = "model_n_gauss{}_noise{}_min{}_max{}".format(n_gauss,noise, Min, Max)
    if os.path.isfile(os.path.join(directory, 'output_params.json')):
        print (directory)
        prefixes.append(os.path.join(directory, "output"))

models = dict()
for f in prefixes:
    params = numpy.loadtxt(f + '_params.json', delimiter="\", \"", dtype=str)
    a = pymultinest.Analyzer(outputfiles_basename=f, n_params=len(params))
    s = a.get_stats()
    models[f] = s['global evidence']

best = max(models, key=models.__getitem__)
Zbest = models[best]


for m in models:
    models[m] -= Zbest
Ztotal = log(sum(numpy.exp([Z for Z in models.values()])))
limit = 30 # for example, Jeffreys scale for the Bayes factor

fout = open('modelcompare{}.rst'.format(noise), 'w')
fout.write('Model comparison\n')
fout.write('****************\n')

for m in sorted(models, key=models.__getitem__):
	Zrel = models[m]
	fout.write ('model %-25s: log10(Z) = %6.1f %s' % (m.replace('stats.json',''), Zrel/log(10),
		' XXX ruled out\n' if Zrel < Ztotal - log(limit) else '   <-- GOOD\n' ))

print ()
fout.write('The last, most likely model was used as normalization.\n')
fout.write('Uniform model priors are assumed, with a cut of log10(%s) to rule out models.\n' % limit)
print ()
fout.close()
try:
	AICs = dict([(f, 2*len(json.load(open(f + '_params.json'))) + numpy.loadtxt(f + '.txt')[:,1].min()) for f in prefixes])

	fout = open('modelcompare{}.rst'.format(noise), 'a')
	fout.write('+-%-40s-+-%-15s-+-%-15s-+\n' % ('-'*40, '-'*15, '-'*15))
	fout.write('| %-40s | %+15s | %+15s |\n' % ('model', 'ln Z', 'AIC'))
	#print '+-%-40s-+-%-15s-+-%-15s-+ ' % ('-'*40, '-'*15, '-'*15)
	fout.write('+=%-40s=+=%-15s=+=%-15s=+\n' % ('='*40, '='*15, '='*15))
	for m in sorted(models, key=models.__getitem__):
		mname = m.replace('stats.json','').rstrip('_')
		Zrel = models[m]
		fout.write('| %-40s | %+15s | %+15s |\n' % (mname,'%.1f' % Zrel, '%.1f' % AICs[m]))
		fout.write('+-%-40s-+-%-15s-+-%-15s-+\n' % ('-'*40, '-'*15, '-'*15))
	fout.close()
except IOError as e:
	print ('Warning:', e)