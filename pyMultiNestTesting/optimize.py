import json
import pymultinest
from spectrumLoader import *
import os
import numpy as np
from mpi4py import MPI
import socket
Min = 6540
Max = 6580
filename = "GGCar-2457811.2917824076-Summed-20170226-GJW-WA_100.0_bin0.5.txt"

noise = 0.01
n_gausses = [6, 7, 8, 9, 10, 11]

wavelength, intensity, error = loadSpectrum(filename)
error = error/max(intensity)
intensity = removeBackground(wavelength, intensity)
intensity = normaliseSpectrum(wavelength, intensity)
intensity = intensity[np.logical_and(wavelength > Min, wavelength < Max)]
error = error[np.logical_and(wavelength > Min, wavelength < Max)]
wavelength = wavelength[np.logical_and(wavelength > Min, wavelength < Max)]

for n_gauss in n_gausses:
    directory = "model_n_gauss{}_noise{}_min{}_max{}_doubleCore".format(n_gauss,noise, Min, Max)
    if not os.path.exists(directory):
        try:
            os.makedirs(directory)
        except:
            print("exception, folder exists")
    print (directory)


    parameters = ["base"]
    for i in range(n_gauss):
        parameters.append("mean{}".format(i))
        parameters.append("amp{}".format(i))
        parameters.append("sigma{}".format(i))
    n_params = len(parameters)


    def gauss(x, base, A, mu, sigma):
        return base + A*np.exp(-(x-mu)**2/(2.*sigma**2))

    def gaussN(x, cube):
        y = np.zeros_like(x)
        base = cube[0]
        for i in range(n_gauss):
            mean = cube[3*i + 1]
            A = cube[3*i+2]
            SD = cube[3*i+3]

            y = y + gauss(x, base/n_gauss, A, mean, SD)
        return y

    # a more elaborate prior
    # parameters are pos1, width, height1, [height2]
    def prior(cube, ndim, nparams):
        cube[0] = cube[0] * 0.1  # base
        for i in range(1, n_params, 3):
            cube[i] = Min + (Max - Min) * cube[i]
            # print(i, cube[i])
            cube[i + 1] = cube[i + 1]  # Amplitude
            cube[i + 2] = cube[i + 2] * 20.  # Sigma


    def loglike(cube, ndim, nparams):
        ymodel = gaussN(wavelength, cube)
        loglikelihood = (-0.5 * ((ymodel - intensity)/noise)**2).sum()
        return loglikelihood


    datafile = os.path.join(directory, "output")

    # run MultiNest
    print('running multinest...', socket.gethostname())
    pymultinest.run(loglike, prior, n_params, outputfiles_basename=datafile, resume = True, verbose = False, sampling_efficiency="model")
    json.dump(parameters, open(datafile + '_params.json', 'w')) # save parameter names
