import json
import pymultinest
from spectrumLoader import *
import os
from scipy.ndimage.filters import gaussian_filter
from mpi4py import MPI
Min = 6510
Max = 6520
filename = "GGCar-2457817.2989004632-Summed-20170304-GJW-WA_3000.0_bin0.5.txt"

noise = 0.05

wavelength, intensity = loadSpectrum(filename)
intensity = removeBackground(wavelength, intensity)
intensity = normaliseSpectrum(wavelength, intensity)
intensity = intensity[np.logical_and(wavelength > Min, wavelength < Max)]
wavelength = wavelength[np.logical_and(wavelength > Min, wavelength < Max)]

n_gauss = 1
directory = "model_n_ring{}_noise{}_min{}_max{}".format(n_gauss,noise, Min, Max)
if not os.path.exists(directory):
    os.makedirs(directory)
print (directory)


parameters = ["base"]

for i in range(n_gauss):
    parameters.append("keplerVel{}".format(i))
    parameters.append("internalVel{}".format(i))
    parameters.append("resolution{}".format(i))
    parameters.append("maxIntensity{}".format(i))
    parameters.append("center{}".format(i))
n_params = len(parameters)


def line_profile_thin_ring(losVel, center, keplerVelocity, internalVelocity, resolution, maxIntensity, base):
    def intensity_by_projected_velocity(projectedVel, actualVel, center):
        if abs(projectedVel) == actualVel:
            return 0.
        return 1./(2.*np.pi*np.sqrt(1.-(abs(projectedVel - center)/actualVel)**2.))
    intensities = []

    for i in losVel:
        if abs(i - center) < 0.999*keplerVelocity:
            intensities.append(intensity_by_projected_velocity(i, keplerVelocity, center))
        else:
            intensities.append(0.)
    intensities = np.array(intensities)
    result = gaussian_filter(intensities, internalVelocity+resolution)
    result = result/max(result)*maxIntensity + base
    return result


def line_profile_thin_ring_N(x, cube):
    y = np.zeros_like(x)
    base = cube[0]
    for i in range(1, n_params, 5):
        keplerVel = cube[i]
        internalVel = cube[i + 1]
        resolution = cube[i + 2]
        maxIntensity = cube[i + 3]
        center = cube[i + 4]
        y = y + line_profile_thin_ring(x, center, keplerVel, internalVel, resolution, maxIntensity, 0.)
    y = y + base

    return y


# a more elaborate prior
# parameters are pos1, width, height1, [height2]
def prior(cube, ndim, nparams):
    cube[0] = cube[0] * 0.1  # base
    for i in range(1, n_params, 5):
        cube[i] = cube[i]*20. # keplerVel
        cube[i + 1] = cube[i + 1] * 50. #
        cube[i + 2] = cube[i + 2] * 50.  # Sigma
        cube[i + 3] = cube[i + 3]
        cube[i + 4] = Min + (Max - Min) * cube[i+4] # center



def loglike(cube, ndim, nparams):
    ymodel = line_profile_thin_ring_N(wavelength, cube)
    loglikelihood = (-0.5 * ((ymodel - intensity)/noise)**2).sum()
    return loglikelihood


datafile = os.path.join(directory, "output")

# run MultiNest
pymultinest.run(loglike, prior, n_params, outputfiles_basename=datafile, resume = False, verbose = False, sampling_efficiency="model")
json.dump(parameters, open(datafile + '_params.json', 'w')) # save parameter names