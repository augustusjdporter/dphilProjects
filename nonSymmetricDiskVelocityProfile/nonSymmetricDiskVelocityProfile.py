import matplotlib.pyplot as plt
import numpy as np
import pylab as pl
from diskProfiles import diskDensityHorseShoe, diskDensitySpiral, diskDensityAxisymmetric, binaryBrightnessAtVelocity, keplerianVelocity, ejectedVelocity
import cppBinding

def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return idx-1

def convertToPolarCoords(x, y):
    r = np.sqrt(x**2 + y**2)
    theta = np.arctan(y / x)
    if x >= 0 and y >= 0:
        theta = theta
    elif x < 0 and y > 0:
        theta = theta + np.pi
    elif x < 0 and y <= 0:
        theta = theta + np.pi
    elif x > 0 and y < 0:
        theta = theta + 2 * np.pi
    return r, theta

def convertToCartesian(r, theta):
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    return x, y

circumbinaryInclination = np.radians(45.)
periodCircumbinaryOverPeriodBinary = 10.

numberOfThetaBins = 360
numberOfRBins = 100
numberOfVelocityBins = 400
thetaBins = pl.linspace(0., 2 * np.pi, numberOfThetaBins)  # 0 to 360 in steps of 360/N.
rBins = pl.linspace(0.1, 1., numberOfRBins)
velocityBins = pl.linspace(-1., 1., numberOfVelocityBins)




librationFileNames = ["librationData/LowerLibration.txt", "librationData/UpperLibration.txt", "librationData/InnerLibration.txt", "librationData/OuterLibration.txt"]
typeLibration = ["Lower", "Upper", "Inner", "Outer"]

for librationFileName, typeLib in zip(librationFileNames, typeLibration):
    if typeLib != "Outer":
        continue
    calculator = cppBinding.dopplerTomogramCalculator()
    calculatorBinPhase = cppBinding.dopplerTomogramCalculator()
    iCosWoPi = np.loadtxt(librationFileName, usecols=[0])
    iSinWoPi = np.loadtxt(librationFileName, usecols=[1])
    inclinationsRaw = np.pi * np.sqrt(iCosWoPi**2 + iSinWoPi**2)

    inclinations = []
    for i in inclinationsRaw:
        if i > np.pi/2.:
            i = np.pi - i
        inclinations.append(i)
    inclinations = np.array(inclinations)

    numberOfFrames = len(inclinations)
    numberOfBinaryOrbitsPerCBOrbit = 11.

    thetaMeanVals = pl.linspace(0., 2.*np.pi - (2*np.pi / numberOfFrames), numberOfFrames)

    A, B = np.meshgrid(thetaBins, rBins)
    densityArray = np.zeros(A.shape)

    phaseBins = pl.linspace(0., numberOfFrames, numberOfFrames)
    A, B = np.meshgrid(velocityBins, phaseBins)
    trailedSpectraBrightness = np.zeros(A.shape)

    for k, circumbinaryInclination in zip(range(len(inclinations)), inclinations):
        print(k)
        plt.clf()
        thetaArray = []
        rArray = []
        velocityArray = []
        brightnessMappedToVelocity = []

        thetaMean = thetaMeanVals[k % len(thetaMeanVals)]
        rMean = 0.5
        rSD = 0.1
        rMin = min(rBins)
        # get brightness of disk as function of velocity
        for i, theta in zip(range(len(thetaBins)), thetaBins):
            for j, r in zip(range(len(rBins)), rBins):
                # assume brightness of disk at a point is it's density
                density = diskDensityHorseShoe(r, theta, thetaMean, np.radians(45.))*r
                # get the inclination view in the top down
                x, y = convertToCartesian(r, theta)
                x = x * np.sin(circumbinaryInclination)
                newR, newTheta = convertToPolarCoords(x, y)
                thetaIndex = find_nearest(thetaBins, newTheta)
                rIndex = find_nearest(rBins, newR)
                densityArray[rIndex, thetaIndex] = density
                brightnessMappedToVelocity.append(density)
                velocityArray.append(keplerianVelocity(r, theta, rMin, circumbinaryInclination))

        # get brightness of binary as function of velocity
        binaryPhase = (k / numberOfFrames) * numberOfBinaryOrbitsPerCBOrbit
        circumbinaryPhase = k / numberOfFrames

        # uncomment following lines to include binary lines
        # for vel in velocityBins:
        #     velocityArray.append(vel)
        #     brightnessMappedToVelocity.append(binaryBrightnessAtVelocity(vel, binaryPhase))

        brightnessMappedToVelocity = np.array(brightnessMappedToVelocity)
        velocityArray = np.array(velocityArray)

        ax = plt.subplot(211, polar=True)
        ax.set_title("Inclination: {}".format(np.degrees(circumbinaryInclination)))
        ax.pcolormesh(thetaBins, rBins, densityArray)
        plt.subplot(212)

        n, bins, patches = plt.hist(velocityArray, numberOfVelocityBins, normed=1, range=[-1, 1], alpha=0.75, weights=brightnessMappedToVelocity)
        for i, histVal in zip(range(len(n)), n):
            trailedSpectraBrightness[k, i] = histVal
        print (k / numberOfFrames)

        calculator.loadDataFrom1DArray(trailedSpectraBrightness[k], circumbinaryPhase, velocityBins)
        calculatorBinPhase.loadDataFrom1DArray(trailedSpectraBrightness[k], binaryPhase, velocityBins)
        plt.gcf().set_size_inches(12., 12.)
        # plt.show()
        plt.savefig('/Data/nonSymmetricDiskVelocityProfile/precessing' + typeLib + '/lineProfile' + str(k) + '.png')


    # # plot DT with binary phase
    # plt.clf()
    # numberOfRBins = 200
    # numberOfThetaBins = 720
    # calculatorBinPhase.computeRadialTomogram(numberOfRBins, numberOfThetaBins)
    # aBins = pl.linspace(0, 2 * pl.pi, numberOfThetaBins)  # 0 to 360 in steps of 360/N.
    # rBins = pl.linspace(0, 1, numberOfRBins)
    #
    # figure = plt.figure()
    # ax = figure.add_axes([0.,0.1,0.3,0.8],polar=True)
    # ax2 = figure.add_axes([0.35,0.1,0.3,0.8],polar=True)
    # ax3 = figure.add_axes([0.7,0.1,0.3,0.8],polar=False)
    #
    # intensityArray = np.array(calculatorBinPhase.intensityValuesRadial())
    # rVelArray = np.array(calculatorBinPhase.radialVelValues())
    # thetaVelArray = np.array(calculatorBinPhase.thetaValues())
    #
    # intensityArray = intensityArray[rVelArray != 0]
    # thetaVelArray = thetaVelArray[rVelArray != 0]
    # rVelArray = rVelArray[rVelArray != 0]
    #
    # theta, r = pl.meshgrid(aBins, rBins)
    #
    # H, xedges, yedges = np.histogram2d(thetaVelArray[thetaVelArray != 0], rVelArray[thetaVelArray != 0], bins=(aBins, rBins), weights=intensityArray[thetaVelArray != 0])
    #
    # H = H.transpose()
    # ax.pcolormesh(theta, r, H)
    # ax.grid(True)
    #
    # rVelArray = np.array([1 - R for R in rVelArray])
    #
    # H, xedges, yedges = np.histogram2d(thetaVelArray[thetaVelArray != 0], rVelArray[thetaVelArray != 0], bins=(aBins, rBins), weights=intensityArray[thetaVelArray != 0])
    #
    # H = H.transpose()
    # ax.set_ylim(0, 1)
    # ax2.pcolormesh(theta, r, H)
    # ax2.grid(True)
    #
    # ax2.set_ylim(0, 1)
    #
    # ax3.pcolormesh(velocityBins, phaseBins, trailedSpectraBrightness)
    # plt.show()


    # plot DT of the circumbinary phase
    plt.clf()
    numberOfRBins = 200
    numberOfThetaBins = 720
    calculator.computeRadialTomogram(numberOfRBins, numberOfThetaBins)
    aBins = pl.linspace(0, 2 * pl.pi, numberOfThetaBins)  # 0 to 360 in steps of 360/N.
    rBins = pl.linspace(0, 1, numberOfRBins)

    figure = plt.figure()
    figure.suptitle("{}".format(typeLib))
    ax = figure.add_axes([0.,0.1,0.3,0.8],polar=True)
    ax2 = figure.add_axes([0.35,0.1,0.3,0.8],polar=True)
    ax3 = figure.add_axes([0.7,0.1,0.3,0.8],polar=False)

    intensityArray = np.array(calculator.intensityValuesRadial())
    rVelArray = np.array(calculator.radialVelValues())
    thetaVelArray = np.array(calculator.thetaValues())

    intensityArray = intensityArray[rVelArray != 0]
    thetaVelArray = thetaVelArray[rVelArray != 0]
    rVelArray = rVelArray[rVelArray != 0]

    theta, r = pl.meshgrid(aBins, rBins)

    H, xedges, yedges = np.histogram2d(thetaVelArray[thetaVelArray != 0], rVelArray[thetaVelArray != 0], bins=(aBins, rBins), weights=intensityArray[thetaVelArray != 0])

    H = H.transpose()
    ax.pcolormesh(theta, r, H)
    ax.grid(True)

    rVelArray = np.array([1 - R for R in rVelArray])

    H, xedges, yedges = np.histogram2d(thetaVelArray[thetaVelArray != 0], rVelArray[thetaVelArray != 0], bins=(aBins, rBins), weights=intensityArray[thetaVelArray != 0])

    H = H.transpose()
    ax.set_ylim(0, 1)
    ax2.pcolormesh(theta, r, H)
    ax2.grid(True)

    ax2.set_ylim(0, 1)

    ax3.pcolormesh(velocityBins, phaseBins, trailedSpectraBrightness)
    plt.show()