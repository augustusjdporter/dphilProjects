import matplotlib.pyplot as plt
import numpy as np
import pylab as pl
from scipy.integrate import simps
from scipy.optimize import curve_fit
from diskProfiles import diskDensityHorseShoe, diskDensitySpiral, diskDensityAxisymmetric

def gauss(x, base, A, mu, sigma):
    return base + A*np.exp(-(x-mu)**2/(2.*sigma**2))

def opticalDepth(opacity, density):
    return 1/(opacity * density)

def intensityAfterExtinction(initialIntensity, densityValuesLOS, rValsLOS, opacity):
    if len(densityValuesLOS) == 0:
        print("no disk")
        return initialIntensity

    integral = simps(densityValuesLOS, rValsLOS)
    return initialIntensity*np.exp(-opacity * integral)

numberOfFrames = 50
rBins = pl.linspace(0.1, 1., 500)
zBins = pl.linspace(0., .5, 500) # because the light will only ever go through half the disk, only iterate through the positive z

thetaSD = 0.

scaleHeight = 0.1

librationFileNames = ["librationData/LowerLibration.txt", "librationData/UpperLibration.txt", "librationData/InnerLibration.txt", "librationData/OuterLibration.txt"]
for librationFileName in librationFileNames:
    iCosWoPi = np.loadtxt(librationFileName, usecols=[0])
    iSinWoPi = np.loadtxt(librationFileName, usecols=[1])
    plt.plot(iCosWoPi, iSinWoPi)
plt.xlim([-1.,1.])
plt.ylim([-1.,1.])
plt.xlabel("i*cos(W)/pi")
plt.ylabel("i*sin(W)/pi")
plt.show()
plt.clf()
for librationFileName in librationFileNames:
    iCosWoPi = np.loadtxt(librationFileName, usecols=[0])
    iSinWoPi = np.loadtxt(librationFileName, usecols=[1])

    inclinationsRaw = np.pi * np.sqrt(iCosWoPi**2 + iSinWoPi**2)
    lightCurve = []
    inclinations = []
    thetaVals = []
    for i in inclinationsRaw:
        if i > np.pi/2.:
            i = np.pi - i
        inclinations.append(i)
    inclinations = np.array(inclinations)
    for inclination, i in zip(inclinations, range(len(inclinations))):
        thetaVals.append(i/len(inclinations))
        print("inc", np.degrees(inclination))
        if inclination > np.pi/2.:
            inclination = np.pi/2 - (inclination - np.pi/2.)
            print("\t", np.degrees(inclination))

        densityArray = []
        rArray = []
        velocityArray = []
        thetaMean = 0.
        theta = 0.0 # line of sight
        for r in rBins:
            for z in zBins:
                # print(np.tan(inclination), r/z, abs(np.tan(inclination) - r/z))
                if z != 0. and abs(z*np.sin(inclination) - r*np.cos(inclination)) < 0.02:
                    densityArray.append(diskDensityAxisymmetric(r, theta, thetaMean, np.radians(thetaSD), z=z, scaleHeight=scaleHeight))
                    rArray.append(np.sqrt(r**2 + z**2))
                elif z==0. and inclination == np.radians(90.*(numberOfFrames-1.)/numberOfFrames):
                    densityArray.append(diskDensityAxisymmetric(r, theta, thetaMean, np.radians(thetaSD)))
                    rArray.append(r)


        densityArray = np.array(densityArray)
        # for d in densityArray:
        #     print(d)
        rArray = np.array(rArray)

        intenstityFromBinary = 1.
        intensityFromBinaryNoOccultation = 1.
        lightCurve.append(intensityAfterExtinction(intenstityFromBinary, densityArray, rArray, 1.))

    lightCurve = np.array(lightCurve)

    fig, ax1 = plt.subplots()
    ax1.set_title(librationFileName)
    ax1.plot(thetaVals, lightCurve, 'b')
    ax1.set_xlabel("Phase")
    ax1.set_ylabel("Intensity (arbitrary)", color='b')
    ax2 = ax1.twinx()
    ax2.plot(thetaVals, np.degrees(inclinations), 'r')
    ax2.set_ylabel('Inclination (degrees)', color='r')

    fig.tight_layout()
    plt.show()

