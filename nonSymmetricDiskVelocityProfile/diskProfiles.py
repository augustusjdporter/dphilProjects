import numpy as np

def diskDensityAxisymmetric(r, rMean, rSD, theta, thetaMean, thetaSD, z = None, scaleHeight = None):
    amplitude = 0.01
    rVar = ((r - rMean) ** 2) / (2 * rSD ** 2)
    density = amplitude * np.exp(-(rVar))
    if z != None:
        zVar = z**2 / (2.*scaleHeight**2)
        density = density * np.exp(-zVar)
    return density

# model disk density as a 2D gaussian in R and theta
def diskDensityHorseShoe(r, theta, thetaMean, thetaSD):
    amplitude = 1
    rMean = 0.5
    rSD = 0.1
    # thetaMean = 5.*np.pi/180.
    angularDistanceFromMean = abs(thetaMean - theta)
    thetaToUse = angularDistanceFromMean
    if angularDistanceFromMean > np.pi:
        thetaToUse = 360.*np.pi/180. - max(thetaMean, theta) + min(thetaMean, theta)

    rVar = ((r - rMean)**2)/(2*rSD**2)
    thetaVar = ((thetaToUse) ** 2) / (2 * thetaSD ** 2)
    return amplitude * np.exp(-(rVar + thetaVar))


def diskDensitySpiral(r, theta, thetaMean, thetaSD):
    amplitude = 1
    # rMean = thetaMean + 0.1*theta
    if theta < thetaMean:
        theta = theta + 2*np.pi
    fractionAround = (theta - thetaMean)/(2*np.pi)
    rMean1 = 0.5 - (0.5 - 0.1) * fractionAround
    rSD = 0.05
    rVar1 = ((r - rMean1)**2)/(2*rSD**2)

    rMean2 = 0.9 - (0.9 - 0.5) * fractionAround
    rVar2 = ((r - rMean2)**2)/(2*rSD**2)
    return amplitude * (np.exp(-rVar1) + np.exp(-rVar2))


def binaryBrightnessAtVelocity(velocity, phase):
    amplitudeSinusoid = 0.2
    amplitudeGaussian = 50.
    widthGaussian = 0.05

    center1 = amplitudeSinusoid*np.sin(2 * np.pi * phase)
    center2 = -amplitudeSinusoid*np.sin(2 * np.pi * phase)

    var1 = ((velocity - center1)**2)/(2*widthGaussian**2)
    var2 = ((velocity - center2) ** 2) / (2 * widthGaussian ** 2)

    # print (amplitudeGaussian*(np.exp(-var1) + np.exp(-var2)))
    return amplitudeGaussian*(np.exp(-var1) + np.exp(-var2))

def keplerianVelocity(r, theta, rMin, inclination):
    velocity = ((r/rMin)**-0.5)*np.cos(np.pi/2. - theta)*np.sin(inclination)
    return velocity

def ejectedVelocity(r, theta, rMin, inclination):
    velocity = (rMin/r) * np.cos(theta) * np.sin(inclination)
    return velocity