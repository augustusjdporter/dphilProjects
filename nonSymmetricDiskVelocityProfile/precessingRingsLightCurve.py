import matplotlib.pyplot as plt
import numpy as np
import pylab as pl
from scipy.integrate import simps
from scipy.optimize import curve_fit
from diskProfiles import diskDensityHorseShoe, diskDensitySpiral, diskDensityAxisymmetric

def gauss(x, base, A, mu, sigma):
    return base + A*np.exp(-(x-mu)**2/(2.*sigma**2))

def opticalDepth(opacity, density):
    return 1/(opacity * density)

def intensityAfterExtinction(initialIntensity, densityValuesLOS, rValsLOS, opacity):
    if len(densityValuesLOS) == 0:
        print("no disk")
        return initialIntensity
    # for r, density in zip(rValsLOS, densityValuesLOS):
    #     print (r,density)
    integral = simps(densityValuesLOS, rValsLOS)
    if integral < 0:
        integral = -integral
    # for r, density in zip(rValsLOS, densityValuesLOS):
    #     if density < 0:
    #         print (r,density)
        # print (integral)
    return initialIntensity*np.exp(-opacity * integral)

numberOfFrames = 50
rBins = pl.linspace(0.1, 1., 500)
zBins = pl.linspace(0., .5, 500) # because the light will only ever go through half the disk, only iterate through the positive z

thetaSD = 0.

scaleHeight = 0.1

librationFileNames = ["librationData/LowerLibration.txt", "librationData/UpperLibration.txt", "librationData/InnerLibration.txt", "librationData/OuterLibration.txt"]
for librationFileName in librationFileNames:
    iCosWoPi = np.loadtxt(librationFileName, usecols=[0])
    iSinWoPi = np.loadtxt(librationFileName, usecols=[1])
    plt.plot(iCosWoPi, iSinWoPi)
plt.xlim([-1.,1.])
plt.ylim([-1.,1.])
plt.xlabel("i*cos(W)/pi")
plt.ylabel("i*sin(W)/pi")
plt.show()
plt.clf()

ringRadii = [.5, .75]
innerInclinations = []
outerInclinations = []
for librationFileName in librationFileNames:
    iCosWoPi = np.loadtxt(librationFileName, usecols=[0])
    iSinWoPi = np.loadtxt(librationFileName, usecols=[1])

    inclinationsRaw = np.pi * np.sqrt(iCosWoPi**2 + iSinWoPi**2)
    lightCurve = []
    inclinations = []
    thetaVals = []
    for i in inclinationsRaw:
        if i > np.pi/2.:
            i = np.pi - i
        inclinations.append(i)
    inclinations = np.array(inclinations)
    numberOfFrames = 4*len(inclinations)
    for k in range(numberOfFrames):
        circumbinaryInclinationOuterRing = inclinations[int(k / 4)]
        circumbinaryInclinationInnerRing = inclinations[k % len(inclinations)]
        innerInclinations.append(circumbinaryInclinationInnerRing)
        outerInclinations.append(circumbinaryInclinationOuterRing)
        print(k, np.degrees(circumbinaryInclinationInnerRing), np.degrees(circumbinaryInclinationOuterRing))
        thetaVals.append(k/numberOfFrames)

        if circumbinaryInclinationOuterRing > np.pi/2.:
            circumbinaryInclinationOuterRing = np.pi/2 - (circumbinaryInclinationOuterRing - np.pi/2.)
        if circumbinaryInclinationInnerRing > np.pi/2.:
            circumbinaryInclinationInnerRing = np.pi/2 - (circumbinaryInclinationInnerRing - np.pi/2.)

        densityArray = []
        rArray = []
        velocityArray = []
        thetaMean = 0.
        theta = 0.0 # line of sight
        rSD = 0.025
        thetaSD = 0.
        for r in rBins:
            for z in zBins:
                # print(np.tan(inclination), r/z, abs(np.tan(inclination) - r/z))
                if abs(z*np.sin(circumbinaryInclinationInnerRing) - r*np.cos(circumbinaryInclinationInnerRing)) < 0.005:
                    densityArray.append(diskDensityAxisymmetric(r, ringRadii[0], rSD, theta, thetaMean, np.radians(thetaSD), z=z, scaleHeight=scaleHeight))
                    rArray.append(np.sqrt(r ** 2 + z ** 2))
                elif z==0. and circumbinaryInclinationInnerRing == np.radians(90.*(numberOfFrames-1.)/numberOfFrames):
                    densityArray.append(diskDensityAxisymmetric(r, ringRadii[0], rSD, theta, thetaMean, np.radians(thetaSD)))
                    rArray.append(r)

                if abs(z * np.sin(circumbinaryInclinationOuterRing) - r * np.cos(circumbinaryInclinationOuterRing)) < 0.005:
                    densityArray.append(diskDensityAxisymmetric(r, ringRadii[1], rSD, theta, thetaMean, np.radians(thetaSD), z=z,scaleHeight=scaleHeight))
                    rArray.append(np.sqrt(r ** 2 + z ** 2))
                elif z==0. and circumbinaryInclinationOuterRing == np.radians(90.*(numberOfFrames-1.)/numberOfFrames):
                    densityArray.append(diskDensityAxisymmetric(r, ringRadii[1], rSD, theta, thetaMean, np.radians(thetaSD)))
                    rArray.append(r)

        newRArray = []
        newDensityArray = []
        for i in range(len(rArray)):
            if rArray[i] not in newRArray:
                newRArray.append(rArray[i])
                newDensityArray.append(densityArray[i])
            else:
                index = newRArray.index(rArray[i])
                newDensityArray[index] += densityArray[i]

        densityArray = np.array(newDensityArray)
        # for d in densityArray:
        #     print(d)
        rArray = np.array(newRArray)

        intenstityFromBinary = 1.
        intensityFromBinaryNoOccultation = 1.
        # densityArray = np.array([x for (y, x) in sorted(zip(rArray, densityArray))])
        # rArray.sort()
        lightCurve.append(intensityAfterExtinction(intenstityFromBinary, densityArray, rArray, 1.))

    lightCurve = np.array(lightCurve)
    lightCurve[lightCurve > 1] = np.nan
    for theta, light in zip(thetaVals,lightCurve):
        print(theta, light)
    fig, ax1 = plt.subplots()
    ax1.set_title(librationFileName)
    ax1.plot(thetaVals, lightCurve, 'b')
    ax1.set_xlabel("Phase")
    ax1.set_ylabel("Intensity (arbitrary)", color='b')
    ax2 = ax1.twinx()
    ax2.plot(thetaVals, np.degrees(outerInclinations), 'r')
    ax2.plot(thetaVals, np.degrees(innerInclinations), 'r')
    ax2.set_ylabel('Inclination (degrees)', color='r')

    fig.tight_layout()
    plt.show()