import matplotlib.pyplot as plt
import numpy as np
import pylab as pl
from scipy.integrate import simps
from scipy.optimize import curve_fit
from diskProfiles import diskDensityHorseShoe, diskDensitySpiral

def gauss(x, base, A, mu, sigma):
    return base + A*np.exp(-(x-mu)**2/(2.*sigma**2))

def binaryIntensity(binaryPhase, inclination):
    intensityPrimary = 0.8
    intensitySecondary = 1. - intensityPrimary

    radiusPrimary = 1.
    radiusSecondary = radiusPrimary/3.

    semiMajorAxis = 5.*radiusPrimary

    impactParameter = semiMajorAxis * np.cos(inclination) / radiusPrimary
    while binaryPhase > 2. * np.pi:
        binaryPhase = binaryPhase - 2*np.pi

    while binaryPhase < 0.:
        binaryPhase = binaryPhase + 2. * np.pi

    if impactParameter * radiusPrimary > radiusPrimary + radiusSecondary:
        # no occultation
        return intensityPrimary + intensitySecondary
    else:
        phaseDurationTransit = 2 * np.arcsin(np.sqrt((radiusPrimary + radiusSecondary)**2 - (impactParameter * radiusPrimary)**2)/semiMajorAxis)
        ingressBeginPhase = phaseDurationTransit / 2.
        ingressEndPhase = np.arcsin(np.sqrt((radiusPrimary - radiusSecondary)**2 - (impactParameter * radiusPrimary)**2)/semiMajorAxis)
        # print (phaseDurationTransit / 2)
        # print (ingressBeginPhase, ingressEndPhase)

        if (binaryPhase >= 0. - ingressEndPhase and binaryPhase <= 0. + ingressEndPhase) or (binaryPhase >= 2*np.pi - ingressEndPhase and binaryPhase <= 2*np.pi + ingressEndPhase): # primary is completely eclipsing the secondary
            # print ('primary is completely eclipsing the secondary')
            return intensityPrimary
        elif ((binaryPhase > 0. - ingressBeginPhase and binaryPhase < 0 - ingressEndPhase) or (binaryPhase > 2.*np.pi - ingressBeginPhase and binaryPhase < 2*np.pi - ingressEndPhase) or (binaryPhase < 0. + ingressBeginPhase and binaryPhase > 0 + ingressEndPhase) or (binaryPhase < 2.*np.pi + ingressBeginPhase and binaryPhase > 2.*np.pi + ingressEndPhase)):
            # print ('primary ingressing/egressing in front of secondary')
            depth = intensitySecondary

            # next if statement, if binary approaching 2pi, change binaryPhase so it's on the mirror side of zero
            if binaryPhase > np.pi:
                binaryPhase = binaryPhase + 2*(2*np.pi - binaryPhase) - 2*np.pi
            distanceFromStartOfIngress = abs(binaryPhase - (ingressBeginPhase))
            return intensityPrimary + intensitySecondary - (depth * distanceFromStartOfIngress / (ingressBeginPhase - ingressEndPhase))

        elif binaryPhase >= np.pi - ingressEndPhase and binaryPhase <= np.pi + ingressEndPhase: # secondary is completely eclipsing primary
            # print ('secondary is completely eclipsing primary')
            if radiusPrimary - impactParameter > radiusSecondary:
                # if entirety of secondary occults the primary
                return intensitySecondary + intensityPrimary * (1. - (radiusSecondary/radiusPrimary)**2)
            else:
                # if only partial occultation
                return intensitySecondary + intensityPrimary * (1. - (radiusSecondary - (impactParameter - radiusPrimary)/2 * radiusPrimary)**2)
        elif (binaryPhase > np.pi - ingressBeginPhase and binaryPhase < np.pi - ingressEndPhase) or (binaryPhase < np.pi + ingressBeginPhase and binaryPhase > np.pi + ingressEndPhase):
            # print('secondary ingressing/egressing in front of primary')
            distanceFromStartOfIngress  = abs(binaryPhase - (np.pi - ingressBeginPhase))
            depth = 0
            minIntensity = 0
            if radiusPrimary - impactParameter > radiusSecondary:
                # if entirety of secondary occults the primary
                minIntensity = (intensitySecondary + intensityPrimary * (1. - (radiusSecondary/radiusPrimary)**2))
            else:
                # if only partial occultation
                minIntensity = (intensitySecondary + intensityPrimary * (1. - (radiusSecondary - (impactParameter - radiusPrimary)/2 * radiusPrimary)**2))

            depth = intensityPrimary + intensitySecondary - minIntensity
            intensity = intensityPrimary + intensitySecondary - depth * distanceFromStartOfIngress/(ingressBeginPhase - ingressEndPhase)
            if intensity < minIntensity:
                distanceFromStartOfIngress = abs(binaryPhase - (np.pi + ingressBeginPhase))
                intensity = intensityPrimary + intensitySecondary - depth * distanceFromStartOfIngress/(ingressBeginPhase - ingressEndPhase)
            return intensity

    return intensityPrimary + intensitySecondary

def opticalDepth(opacity, density):
    return 1/(opacity * density)

def intensityAfterExtinction(initialIntensity, densityValuesLOS, rValsLOS, opacity):
    integral = simps(densityValuesLOS, rValsLOS)
    return initialIntensity*np.exp(-opacity * integral)

binaryInclination = np.radians(90.)
circumbinaryInclination = np.radians(90.)
periodCircumbinaryOverPeriodBinary = 10.

rBins = pl.linspace(0.1, 1., 200)

numberOfFrames = 2000

thetaVals = pl.linspace(-np.pi, np.pi, numberOfFrames)
thetaSDs = pl.linspace(22.5, 270., 20)
widths = []
for thetaSD in thetaSDs:
    lightCurve = []
    lightCurveNoEclipsing = []
    for i in range(numberOfFrames):
        densityArray = []
        rArray = []
        velocityArray = []
        thetaMean = thetaVals[i]
        theta = 0.0 # line of sight
        for r in rBins:
            densityArray.append(diskDensityHorseShoe(r, theta, thetaMean, np.radians(thetaSD)) * r)
            rArray.append(r)

        densityArray = np.array(densityArray)
        rArray = np.array(rArray)

        intenstityFromBinary = binaryIntensity(thetaMean * periodCircumbinaryOverPeriodBinary, binaryInclination)
        intensityFromBinaryNoOccultation = 1.
        lightCurve.append(intensityAfterExtinction(intenstityFromBinary, densityArray, rArray, 1.))
        lightCurveNoEclipsing.append(intensityAfterExtinction(intensityFromBinaryNoOccultation, densityArray, rArray, 1.))

    lightCurve = np.array(lightCurve)
    lightCurveNoEclipsing = np.array(lightCurveNoEclipsing)
    plt.plot(thetaVals, lightCurve)
    guess = []
    guess += [1]
    guess += [np.pi, -0.4, 1.]

    popt, pcov = curve_fit(gauss, thetaVals, lightCurveNoEclipsing, p0=guess)
    print (popt)

    fit = gauss(thetaVals, popt[0], popt[1], popt[2], popt[3])
    plt.plot(thetaVals, fit, label=str(thetaSD) + ' ' + str(popt[3]*180./np.pi))
    widths.append(popt[3]*180./np.pi)

plt.xlabel("Circumbinary phase")
plt.ylabel("Intensity")
plt.legend()
plt.show()

plt.clf()
plt.plot(thetaSDs, widths)
plt.show()